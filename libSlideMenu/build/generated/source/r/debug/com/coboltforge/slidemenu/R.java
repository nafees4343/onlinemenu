/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.coboltforge.slidemenu;

public final class R {
    public static final class drawable {
        public static int ic_launcher = 0x7f090001;
        public static int logo = 0x7f090002;
        public static int rounded_corner = 0x7f090003;
    }
    public static final class id {
        public static int footer = 0x7f0c0001;
        public static int footerText = 0x7f0c0002;
        public static int footer_icon = 0x7f0c0003;
        public static int menu_label = 0x7f0c0004;
        public static int menu_listview = 0x7f0c0005;
        public static int overlay = 0x7f0c0006;
    }
    public static final class layout {
        public static int footer = 0x7f0f0001;
        public static int slidemenu = 0x7f0f0002;
        public static int slidemenu_listitem = 0x7f0f0003;
    }
    public static final class mipmap {
        public static int ic_launcher = 0x7f110001;
        public static int logo = 0x7f110002;
    }
    public static final class string {
        public static int action_settings = 0x7f150001;
        public static int add_to_cart = 0x7f150002;
        public static int app_name = 0x7f150003;
        public static int footer_text = 0x7f150004;
        public static int intro_head = 0x7f150005;
        public static int intro_text = 0x7f150006;
        public static int item_one = 0x7f150007;
        public static int item_three = 0x7f150008;
        public static int item_two = 0x7f150009;
        public static int menubutton = 0x7f15000a;
    }
}
