package com.android.tonyvu.sc.model;

import android.util.Log;

import com.android.tonyvu.sc.exception.ProductNotFoundException;
import com.android.tonyvu.sc.exception.QuantityOutOfRangeException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A representation of shopping cart.
 * <p/>
 * A shopping cart has a map of {@link Saleable} products to their corresponding quantities.
 */
//ye poori cart ki file hai
public class Cart implements Serializable {

    private static final long serialVersionUID = 42L;

    private Map<Saleable, Integer> cartItemMap = new HashMap<Saleable, Integer>();

    private HashMap<Saleable, List<ExtraProuct>> productExtraList = new HashMap<>();

    private HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> productExtraListMap  = new HashMap<>();

    private ArrayList<Integer> productQuantities = new ArrayList<>();

    //for quantity
    private Map<Integer, Integer> quantityMap = new HashMap<Integer, Integer>();


    private BigDecimal totalPrice = BigDecimal.ZERO;
    private BigDecimal itemTotalPrice = BigDecimal.ZERO;
    private  BigDecimal totalExtraPrice = BigDecimal.ZERO;
    private int quantity;
    private int totalQuantity = 0;
    private static final AtomicInteger cartItemId = new AtomicInteger(0);
    private int itemid;
    private HashMap<Integer,HashMap<Saleable, List<ExtraProuct>>> productExtraListid  = new HashMap<>();
    //trying to add on id
    private HashMap<Integer, List<ExtraProuct>> idProductExtraList = new HashMap<>();

    private ArrayList<Saleable> productHeader = new ArrayList<>();
    private ArrayList<Integer> productHeaderId = new ArrayList<>();

    private Map<Integer, BigDecimal> extrasTotal = new HashMap<Integer, BigDecimal>();

    public void addTotal(int position,BigDecimal totalofextras){
        extrasTotal.put(position,totalofextras);
    }

    public BigDecimal getExtrasTotal(int key){
        if (extrasTotal.get(key)!=null)
            return extrasTotal.get(key);

        return BigDecimal.ZERO;
    }

    public void removeExtrasTotal(int key){
        extrasTotal.remove(key);
    }



    public void addQuantity(int pid,int qty){
        quantityMap.put(pid,qty);
    }

    public int getProductQuantity(int key){
        Log.d("kkk",""+key);
        if (quantityMap.get(key)!=null)
        return quantityMap.get(key);

        return 0;
    }

    public void removeProductQuantity(int key){
         quantityMap.remove(key);
    }

    public void removeAllProductQuantities(){
        quantityMap.clear();
    }



    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addHeader(final Saleable sellable){
        productHeader.add(sellable);
        totalPrice = totalPrice.add(sellable.getPrice());
    }
    public void addHeaderid(final Saleable sellable){
        productHeaderId.add(sellable.getitemId());
        //totalPrice = totalPrice.add(sellable.getPrice());
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }


    public ArrayList<Integer> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(ArrayList<Integer> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public void add(final Saleable sellable, List<ExtraProuct> extraItems, int quantity) {
//        if (productExtraList.containsKey(sellable) ){
//            productExtraList.put(sellable, extraItems);
//            productExtraListMap.put(productExtraList, quantity++);
//            productQuantities.add(quantity++);
//
//
//        } else {
//            productHeader.add(sellable);
        /////yaha per
            productExtraList.put(sellable, extraItems);
            productExtraListMap.put(productExtraList, quantity);
            productQuantities.add(quantity);
//        }




        totalPrice = totalPrice.add(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
        totalQuantity += quantity;
    }



    public void additemWithId(final Saleable sellable, List<ExtraProuct> extraItems, int quantity) {

        int itemId = cartItemId.incrementAndGet();
//        for (Saleable sell: productExtraList.keySet()){
//            if (sell.getIde() == itemId){
//
//            }
//        }
        idProductExtraList.put(sellable.getitemId(),extraItems);
        productExtraList.put(sellable, extraItems);
        productExtraListid.put(itemId,productExtraList);
        productExtraListMap.put(productExtraList, quantity);
        productQuantities.add(quantity);

        quantityMap.put(sellable.getproductId(),quantity);

        Log.d("qtyyyyyy","pid: "+sellable.getproductId()+"\naty: "+quantity);
        cartItemMap.put(sellable, itemId);

        double totalPriceWithExtra = 0;
        for (int i = 0 ; i < extraItems.size(); i++){

            ExtraProuct currentCHild = extraItems.get(i);
            totalPriceWithExtra += currentCHild.getExtraPrice();
        }

        BigDecimal currentitem = sellable.getPrice();
        currentitem = currentitem.add(BigDecimal.valueOf(totalPriceWithExtra));
        currentitem = currentitem.multiply(BigDecimal.valueOf(quantity));

//        BigDecimal tax = getTaxValue(itemTotalPrice);
//        itemTotalPrice = itemTotalPrice.subtract(tax);
        itemTotalPrice = itemTotalPrice.add(currentitem);
        //itemTotalPrice = itemTotalPrice.add(getTaxValue(itemTotalPrice));

//        totalPrice = totalPrice.add(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity += quantity;
    }
//    public void add(final Saleable sellable, List<ExtraProuct> extraItems, int quantity) {
//
//        productExtraList.put(sellable, extraItems);
//        productExtraListMap.put(productExtraList, quantity);
//        productQuantities.add(quantity);
//
//
//
//        totalPrice = totalPrice.add(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity += quantity;
//    }

//TODO: modify remove item from cart
    public void remove(int groupPosition,
                       HashMap<Saleable, List<ExtraProuct>> mproductExtraList,
                       HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> mproductExtraListMap
                       ) {

        mproductExtraListMap.remove(mproductExtraList.get(groupPosition));
        mproductExtraList.remove(mproductExtraList.get(groupPosition));



        //        totalPrice = totalPrice.subtract(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity -= quantity;

    }

    public void remove(BigDecimal price){
        itemTotalPrice = itemTotalPrice.subtract(price);
    }

    public void updateItemPrice(BigDecimal price){
        itemTotalPrice = price;
    }
//    /**
//     * Remove a certain quantity of a {@link Saleable} product from this shopping cart
//     *
//     * @param sellable the product which will be removed
//     * @param quantity the quantity of product which will be removed
//     * @throws ProductNotFoundException    if the product is not found in this shopping cart
//     * @throws QuantityOutOfRangeException if the quantity is negative or more than the existing quantity of the product in this shopping cart
//     */
//    public void remove(final Saleable sellable, int quantity) throws ProductNotFoundException, QuantityOutOfRangeException {
//        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();
//
//        int productQuantity = cartItemMap.get(sellable);
//
//        if (quantity < 0 || quantity > productQuantity)
//            throw new QuantityOutOfRangeException(quantity + " is not a valid quantity. It must be non-negative and less than the current quantity of the product in the shopping cart.");
//
//        if (productQuantity == quantity) {
//            cartItemMap.remove(sellable);
//        } else {
//            cartItemMap.put(sellable, productQuantity - quantity);
//        }
//
//        totalPrice = totalPrice.subtract(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity -= quantity;
//    }

    public HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> getProductExtraListMap() {
        return productExtraListMap;
    }

    public void setProductExtraListMap(HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> productExtraListMap) {
        this.productExtraListMap = productExtraListMap;
    }

    public HashMap<Saleable, List<ExtraProuct>> getProductExtraList() {
        return productExtraList;
    }

    public BigDecimal getProductTotalPrice(List<Integer> header, HashMap<Integer, List<ExtraProuct>> extralist, int groupPosition,Integer itemQuantity){
        double _extraPrice = 0;
        List<ExtraProuct> currentExtraList = extralist.get(header.get(groupPosition));
        for (int i = 0 ; i < currentExtraList.size(); i++){

            ExtraProuct currentCHild = currentExtraList.get(i);
            _extraPrice += currentCHild.getExtraPrice();
        }
        BigDecimal result = BigDecimal.ZERO;
        result = productHeader.get(groupPosition).getPrice().add(BigDecimal.valueOf(_extraPrice)).multiply(BigDecimal.valueOf(itemQuantity));
        return result;
    }
    //    /**
//     * Add a quantity of a certain {@link Saleable} product to this shopping cart
//     *
//     * @param sellable the product will be added to this shopping cart
//     * @param quantity the amount to be added
//     */
//    public void add(final Saleable sellable, int quantity) {
//        if (cartItemMap.containsKey(sellable)) {
//            cartItemMap.put(sellable, cartItemMap.get(sellable) + quantity);
//        } else {
//            cartItemMap.put(sellable, quantity);
//        }
//
//        totalPrice = totalPrice.add(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
//        totalQuantity += quantity;



//    }

    /**
     * Set new quantity for a {@link Saleable} product in this shopping cart
     *
     * @param sellable the product which quantity will be updated
     * @param quantity the new quantity will be assigned for the product
     * @throws ProductNotFoundException    if the product is not found in this shopping cart.
     * @throws QuantityOutOfRangeException if the quantity is negative
     */

    public void update(final Saleable sellable, int quantity) throws ProductNotFoundException, QuantityOutOfRangeException {
        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();
        if (quantity < 0)
            throw new QuantityOutOfRangeException(quantity + " is not a valid quantity. It must be non-negative.");

        int productQuantity = cartItemMap.get(sellable);

        Log.d("PQ","productQuantity: "+productQuantity);

        BigDecimal productPrice = sellable.getPrice().multiply(BigDecimal.valueOf(productQuantity));

        cartItemMap.put(sellable, quantity);

        quantityMap.put(sellable.getproductId(),quantity);


        totalQuantity = totalQuantity - productQuantity + quantity;
        totalPrice = totalPrice.subtract(productPrice).add(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
    }

    /**
     * Remove a {@link Saleable} product from this shopping cart totally
     *
     * @param sellable the product to be removed
     * @throws ProductNotFoundException if the product is not found in this shopping cart
     */
    public void remove(final Saleable sellable) throws ProductNotFoundException {
        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();

        //int quantity = cartItemMap.get(sellable);
        cartItemMap.remove(sellable);
    /*    totalPrice = totalPrice.subtract(sellable.getPrice().multiply(BigDecimal.valueOf(quantity)));
        totalQuantity -= quantity;*/
    }

    /**
     * Remove all products from this shopping cart
     */
    public void clear() {
        cartItemMap.clear();
        totalPrice = BigDecimal.ZERO;
        itemTotalPrice = BigDecimal.ZERO;
        totalQuantity = 0;
        productHeader.clear();
    }

    /**
     * Get quantity of a {@link Saleable} product in this shopping cart
     *
     * @param sellable the product of interest which this method will return the quantity
     * @return The product quantity in this shopping cart
     * @throws ProductNotFoundException if the product is not found in this shopping cart
     */
    public int getQuantity(final Saleable sellable) throws ProductNotFoundException {
        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();
        return cartItemMap.get(sellable);
    }

    /**
     * Get extrasTotal cost of a {@link Saleable} product in this shopping cart
     *
     * @param sellable the product of interest which this method will return the extrasTotal cost
     * @return Total cost of the product
     * @throws ProductNotFoundException if the product is not found in this shopping cart
     */
    public BigDecimal getCost(final Saleable sellable) throws ProductNotFoundException {
        if (!cartItemMap.containsKey(sellable)) throw new ProductNotFoundException();
        return sellable.getPrice().multiply(BigDecimal.valueOf(cartItemMap.get(sellable)));
    }

    /**
     * Get extrasTotal price of all products in this shopping cart
     *
     * @return Total price of all products in this shopping cart
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }


    public BigDecimal getTotalPriceWithExtra() {
        return itemTotalPrice;
    }

    /**GrandTotal Implementation , with adding to tax to the extrasTotal
    * @param totalPrice is the extrasTotal price of all items added to the cart
     */

    public BigDecimal getGrandTotalPrice(BigDecimal totalPrice, BigDecimal tax){

        return totalPrice.add(tax);
    }

    /**
     * getTax method is to add the totalPrice * 0.13 to get extrasTotal tax value
     *
     */

    public BigDecimal getTaxValue(BigDecimal totalPrice) {
        final float TAX = 0.13f;
        BigDecimal totalTax = totalPrice.multiply(BigDecimal.valueOf(TAX));
        BigDecimal roundedTaxValue = totalTax.setScale(2,BigDecimal.ROUND_CEILING);
        return  roundedTaxValue;
    }


    /**
     * Get extrasTotal quantity of all products in this shopping cart
     *
     * @return Total quantity of all products in this shopping cart
     */
    public int getTotalQuantity() {
        return totalQuantity;
    }

    public HashMap<Saleable, List<ExtraProuct>> getExtraProduct(){
        return productExtraList;
    }

    public HashMap<Integer, List<ExtraProuct>> getIdProductExtraList() {
        return idProductExtraList;
    }

    public HashMap<Integer, HashMap<Saleable, List<ExtraProuct>>> getProductsWithExtraId(){
        return productExtraListid;
    }


    public List<Saleable> getProductsHeader(){return productHeader;}

    public ArrayList<Integer> getProductHeaderId() {
        return productHeaderId;
    }

    /**
     * Get set of products in this shopping cart
     *
     * @return Set of {@link Saleable} products in this shopping cart
     */

    public Set<Saleable> getProducts() {
        return productExtraList.keySet();
    }

    /**
     * Get a map of products to their quantities in the shopping cart
     *
     * @return A map from product to its quantity in this shopping cart
     */
    public Map<Saleable, Integer> getItemWithQuantity() {
        Map<Saleable, Integer> cartItemMap = new HashMap<Saleable, Integer>();
        cartItemMap.putAll(this.cartItemMap);
        return cartItemMap;
    }

    public double getExtraItemPrice(HashMap<Saleable, List<ExtraProuct>> items, int row){
        double result = 0;
        List<ExtraProuct> currentList = items.get(row);
        for (int j = 0; j < currentList.size(); j++){
            double itemPrice = currentList.get(j).extraPrice;
            result += itemPrice;
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        for (Entry<Saleable, Integer> entry : cartItemMap.entrySet()) {
            strBuilder.append(String.format("Product: %s, Unit Price: %f, Quantity: %d%n", entry.getKey().getPname(), entry.getKey().getPrice(), entry.getValue()));
        }
        strBuilder.append(String.format("Total Quantity: %d, Total Price: %f", totalQuantity, totalPrice));

        return strBuilder.toString();
    }
}
