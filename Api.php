<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
        $this->load->model('menu_model');
		$this->load->model('cart_model');
		$this->load->model('order_model');
        $this->load->model('admin/admin_model');
		$this->load->model('admin/employee_model');
    }
	
	public function login(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		//$username = "sandeep@trsemail.ca";
		//$password = "12345";
		if ($this->user_model->resolve_user_login($username, $password)) {
			$user_id = $this->user_model->get_user_id_from_username($username);
			$user    = $this->user_model->get_user($user_id);
						
			$data['success'] = true;
			$data['user_id'] = $user->iduser;
			$data['first_name'] = $user->first_name;
			$data['last_name'] = $user->last_name;
			$data['email'] = $user->email;
			$data['phone'] = $user->phone;
			$data['address'] = $user->address;
			$data['city'] = $user->city;
			$data['province'] = $user->province;
			$data['postcode'] = $user->postal_code;
			$data['company'] = $user->company;
			$data['country'] = $user->country;
			echo json_encode($data);
			
		} else {
			
			$data['success'] = false;
			echo json_encode($data);
		}
	}
  public function register(){
	 
		$first = $this->input->post('first_name');
		$last = $this->input->post('last_name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$phone = $this->input->post('phone');
		$address1 = strtoupper($this->input->post('address1'));
		$address2 = strtoupper($this->input->post('address2'));
		$address = $address1." ".$address2;
		$city = strtoupper($this->input->post('city'));
		$company = strtoupper($this->input->post('company'));
		$province = strtoupper($this->input->post('province'));
		$postcode = strtoupper($this->input->post('postcode'));
		$country = strtoupper($this->input->post('country'));
		$catering = 0;
		$cateringeclub = 0;
		$diner = 0;
		$dinereclub = 0;
		$notes = "";
		$compid = null;
		$method = 2;
		
		if($this->user_model->email_availability($email)){
						
		$data['success'] = true;
		$data['first'] = $first;
		$data['last'] = $last;
		$data['email'] = $email;
		$this->user_model->register($first, $last, $phone, $email, $address, $city, $province, $postcode, $country, $company, $password, $diner, $dinereclub, $catering, $cateringeclub, $method, $compid, $notes);
		$cid = $this->db->insert_id();
		$data['user_id'] = $cid;
		echo json_encode($data);
		
		} else {
			
			$data['success'] = false;
			$data['message'] = "email already exist";
			echo json_encode($data);
		}
		//$data['success'] = true;
		//echo json_encode($data);*/
	}
	public function menu()
	{
		$data['cats'] = $this->menu_model->get_cats();
		$prods = $this->menu_model->get_menu();
		
		foreach($prods as $prod){
			$e = "";
			$b=[];
			$t="";
			if($prod['is_pizza'] == 1)
			{
				$config = $this->pizza_model->get_pizza_config($prod['product_id']);
				$prod['config']=$config;
				$sizes = $this->pizza_model->get_size($prod['product_id']);
				foreach($sizes as $size)
				{
					$e[$size['size_id']]['id']=$size['size_id'];
					$e[$size['size_id']]['size']=$size['size'];
					$e[$size['size_id']]['price']=$size['price'];
					$e[$size['size_id']]['eprice']=$size['eprice'];					
				}
				array_push($prod,$e);
				
				$base = $this->pizza_model->get_pizza_toppings($prod['product_id']);
				foreach($base as $topping){
					array_push($b,$topping['topping_id_topping']);
				}
				array_push($prod,$b);
				
				$toppings = $this->pizza_model->get_toppings();
				foreach($toppings as $topping){
					if(in_array($topping['topping_id'],$b)){
						//$topping['topping_price'] = 0;
					}
					$t[$topping['category_name']][]=$topping;
				}
				array_push($prod,$t);
				//print_r($prod);exit;
			}
			else
			{
				$extras=$this->menu_model->get_product_extra($prod['product_id']);
				foreach($extras as $extra){
					$e[$extra['category']][]=$extra;
				}
				array_push($prod,$e);
			}
			$menu[$prod['category_id']]['category_name']=$prod['category_name'];
			$menu[$prod['category_id']][]=$prod;
		}
		if(!empty($menu)){
			echo json_encode($menu);
		}
	}
	public function change_password(){
		$password = $this->input->post('password');
		$subdomain = $this->input->post('subdomain');
		
		$email=$subdomain.'@trsemail.ca';
		$gte_id=$this->employee_model->get_id_byemail($email);
		$result=$this->employee_model->change_password($gte_id->idemployee,$password); 
		echo  $result;
	}
	public function categories(){
		$cats = $this->menu_model->get_online_cats();
		if(!empty($cats)){
			echo json_encode($cats);
		}
	}
	
	public function category_products($id){
		$prods = $this->menu_model->get_online_category_products($id);
		if(!empty($prods)){
			echo json_encode($prods);
		}
	}
	
	public function product_extras($id){
		$extras = $this->menu_model->get_product_extra($id);
		if(!empty($extras)){
		foreach($extras as $prod){
			$data[$prod['category_id']]['category_name']=$prod['category'];
			$data[$prod['category_id']]['category_id']=$prod['category_id'];
			$data[$prod['category_id']]['is_required']=$prod['is_required'];
			$data[$prod['category_id']]['is_multiple_select']=$prod['is_multiple_select'];
			$data[$prod['category_id']]['extras'][$prod['idextra']]['extra_name']=$prod['name'];
			$data[$prod['category_id']]['extras'][$prod['idextra']]['extra_id']=$prod['idextra'];
			$data[$prod['category_id']]['extras'][$prod['idextra']]['extra_price']=$prod['price'];
		} 
		}
		//if(!empty($data)){
			echo json_encode($data);
		//}
	}
	public function product_extras_ios($id){
		$extras = $this->menu_model->get_product_extra($id);
		if(!empty($extras)){
		foreach($extras as $prod){
			$data['category_name'][]=$prod['category'];
			$data['category_id'][]=$prod['category_id'];
			$data['is_required'][]=$prod['is_required'];
			$data['is_multiple_select'][]=$prod['is_multiple_select'];
			/*$data[$prod['category_id']]['category_name']=$prod['category'];
			$data[$prod['category_id']]['category_id']=$prod['category_id'];
			$data[$prod['category_id']]['is_required']=$prod['is_required'];
			$data[$prod['category_id']]['is_multiple_select']=$prod['is_multiple_select'];
			$data[$prod['category_id']][$prod['idextra']]['extra_name']=$prod['name'];
			$data[$prod['category_id']][$prod['idextra']]['extra_id']=$prod['idextra'];
			$data[$prod['category_id']][$prod['idextra']]['extra_price']=$prod['price'];*/
		} 
		}
		//if(!empty($data)){
			echo json_encode($data);
		//}
	}
	public function product($id){
		$prods = $this->menu_model->get_online_category_product($id);
		    $data['products']['idcategory']=$prods->category_id;
			$data['products']['category_name']=$prods->category_name;
			$data['products']['product_id']=$prods->product_id;
			$data['products']['product_name']=$prods->product_name;
			$data['products']['product_description']=$prods->product_description;
			$data['products']['price']=$prods->price;
			$data['products']['is_taxable']=$prods->is_taxable;
			$data['products']['eprice']=$prods->eprice;
			$data['products']['image']=$prods->image;
			$data['products']['psort']=$prods->psort;
			$data['products']['category_parent']=$prods->category_parent;
			$data['products']['csort']=$prods->csort;
		if(!empty($data)){
			echo json_encode($data);
		}
	}
	public function location(){
		
			$data['locations'] = $this->admin_model->get_locations();
			foreach($data['locations'] as $loc)
			{
				$loc_id=$loc['idlocation'];
			}
			$week_day=date('N')-1;
			
			$hours = $this->admin_model->get_today_hours($loc_id,$week_day);
			foreach($hours as $hour){
				$hd[$hour->type]['open']=date('g:i A', strtotime($hour->open_time));
				$hd[$hour->type]['close']=date('g:i A', strtotime($hour->close_time));
				$hd[$hour->type]['closed']=$hour->closed;
			}
			$data['hours'] = $hd;
		
			if(!empty($data)){
			echo json_encode($data);
		}
		}
	public function type(){
		$orderType = "";
	$order_types = $this->admin_model->get_setting('orderType');
								  if(!empty($order_types)){
									$arr = explode(',', $order_types);
									$orderType = array_map('trim', $arr);
								  }
								  $data['type'] = $orderType;
								  if(!empty($data)){
			echo json_encode($data);
		}
	}
	public function discount($code)
	{
		$coupon=$this->cart_model->get_discount_code($code);
		
		if(!empty($coupon)){
			$today = date('Y-m-d');
			$contractDateBegin = date('Y-m-d', strtotime($coupon->valid_from));
			$contractDateEnd = date('Y-m-d', strtotime($coupon->valid_to));

			if (($today > $contractDateBegin) && ($today < $contractDateEnd))
			{
				if($coupon->used < $coupon->uses_allowed){
					$array['validity']="true";
					$array['type']=$coupon->type;
					$array['discount']=$coupon->discount;
					echo json_encode($array);
				}
			}
			else{
			$array['validity']="false";
			$array['message']="Not a Valid Coupon";
			echo json_encode($array);
		}
		} else{
			$array['validity']="false";
			$array['message']="Not a Valid Coupon";
			echo json_encode($array);
		}
	}
	public function delivery($postcode=null){
		$pc = substr($postcode, 0, 3);
		$dc = $this->admin_model->get_delivery_charge($pc);
		$minimum=get_setting('minolorder');
		if(!empty($dc)){
			$data['delivery']="true";			
			$data['delivery_charge']=number_format($dc->price, 2, '.', '');
			echo json_encode($data);
			
		} else {
			$data['delivery']="false";
			$data['message']="We donot deliver at your location";
			echo json_encode($data);
		}
	}
	//public function payment($id){
		public function payment(){
		//$order=$this->order_model->get_order($id);
		//$total=($order->subtotal-$order->discount)+$order->delivery_charge+$order->tax;
		$id=$this->input->post('id');
		$total=$this->input->post('total');
		$full_name=$this->input->post('full_name');
		$card_number=$this->input->post('cc_number');
		$card_number=preg_replace('/\D/', '', $card_number);
		$exp_date=$this->input->post('expiry');
		$exp_date=preg_replace('/\D/', '', $exp_date);
		$cvv2=$this->input->post('cvc');
		require_once APPPATH.'third_party/Converge/API.php';
		
		$PaymentProcessor = new \markroland\Converge\ConvergeApi(
			$this->admin_model->get_setting('elavon_merchant_id'),
			$this->admin_model->get_setting('elavon_user_id'),
			$this->admin_model->get_setting('elavon_pin'),
			true
		);
		$response = $PaymentProcessor->ccsale(
			array(
				'ssl_amount' => $total,
				'ssl_card_number' => $card_number,
				'ssl_cvv2cvc2' => $cvv2,
				'ssl_exp_date' => $exp_date,
				'ssl_invoice_number' => $id,
				'ssl_description' => 'Mobile App'
			)
		);
		if(isset($response['ssl_result'])){
			if($response['ssl_result'] == 0){
				$this->order_model->update_order_name($id, $full_name);
				//$this->pay_order($response['ssl_txn_id'],$id);
				//$this->load->library("session");
				//$this->session->set_userdata('cart','');
				$json['status']=true;
				$json['id']=$id;
				$json['data']=$response;
				echo json_encode($json);
			} else{
				$json['status']=false;
				$json['id']=$id;
				$json['data']=$response;
				echo json_encode($json);
			}
		} else{
				$json['status']=false;
				$json['id']=$id;
				$json['data']=$response;
				echo json_encode($json);
		}
	}
	public function placeOrder()
	{
		
		
		$json = $this->input->post('json');
		//This Json array shall be retrieved from the app{“items”:[{“pid”:“309”,“extras”:[],“qty”:“1"},{“pid”:“187",“extras”:[51],“qty”:“1”},{“pid”:“289”,“extras”:[203,22,16],“qty”:“1"}],“type”:“Delivery”,“date”:“09\/22\/2017",“time”:“12:15pm”,“note”:“”,“from”:“1",“delivery_charge”:“2.00"}
		
		$json = json_decode($json);
		
		$cart="";
		if(empty($json)){
			redirect('/menu');
		}	
		if(isset($json->coupon))
		{
			$code = $json->coupon;
		}
		else
		{
		$code='';	
		}
			if(isset($json->tip))
		{
		$tip = $json->tip;
		}
		else
		{
			$tip=0;
		}
		$system = "Online";
		
		$j=0;
		$total=0;
		$gprc=0;
		//delivery
		$billingFirstName = $this->input->post('billing_first_name');
		$billingLastName = $this->input->post('billing_last_name');
		$billingCompany = $this->input->post('billing_company');
		$billingAddress = $this->input->post('billing_address_1') . " " . $this->input->post('billing_address_2');
		$billingCity = $this->input->post('billing_city');
		$billingProvince = $this->input->post('billing_state');
		$billingPostCode = $this->input->post('billing_postcode');
		$billingCountry = 'Canada';
		$billingEmail = $this->input->post('billing_email');
		$billingPhone = $this->input->post('billing_phone');
		
		$dev=0;
		$dev_percent=0;
		if($json->type == 'Delivery'){
			if(isset($json->delivery_charge)){
				$dev=$json->delivery_charge;
			}
			$dev_percent= ($this->admin_model->get_setting('hst')/100)*$dev;
		}
		
		$tax=0;
		
	
		if(!empty($json->items)){
			foreach($json->items as $prod){
				$item_total = 0;
				$pinfo = $this->menu_model->get_product($prod->pid);
				
					$taxable=$pinfo->is_taxable;
					
					$item_total += $pinfo->price;
				
					if(!empty($prod->qty)){
						$qty=$prod->qty;
					}
					if(!empty($prod->extras)){
						foreach($prod->extras as $extra){
							$einfo = $this->menu_model->get_extra($extra);
						
							$item_total += $einfo->price;
						}
					}
					//echo $prod->pid." ".$taxable."<br/>";
					if($taxable==1)
					{
						//$item[count-2]
						$tax += ($this->admin_model->get_setting('hst')/100)* (($item_total*$qty));
						
					}
					$total = $total + ($item_total*$qty);
					//echo $tax;
				
				
			}
		}
		
		$tax=$tax+$dev_percent;
		$discount=null;
		$coupon_id=null;
		if($code != ""){
			$coupon = $this->cart_model->get_discount_code($code);
			$coupon_id=$coupon->iddiscount_code;
			if($coupon->type = "percent"){
				$discount = ($coupon->discount/100)*$total;
				$this->cart_model->use_discount_code($coupon_id);
			} else if($coupon->type = "amount"){
				$discount = $coupon->discount;
				$this->cart_model->use_discount_code($coupon_id);
			}
		}
		//echo $tax;
		//$tax = ($this->admin_model->get_setting('hst')/100)*($total+$dev);
		
		$user_id=0;
		if($this->session->userdata("logged_in") == true){
			$user_id = $this->session->userdata("cid");
			$first = $this->input->post('shipping_first_name');
			$last = $this->input->post('shipping_last_name');
			$email = $this->input->post('shipping_email');
			$phone = $this->input->post('shipping_phone');
			$company = $this->input->post('shipping_company');
			$address1 = $this->input->post('shipping_address_1');
			$address2 = $this->input->post('shipping_address_2');
			$city = $this->input->post('shipping_city');
			$state = $this->input->post('shipping_state');
			$postcode = $this->input->post('shipping_postcode');
			
		}
		else {
			$password = $this->randomPassword();
			$this->user_model->register($billingFirstName, $billingLastName, $billingPhone, $billingEmail, $billingAddress, $billingCity, $billingProvince, $billingPostCode, 'Canada', $billingCompany, $password, 1);
			$user_id = $this->db->insert_id();
			
			$email=$this->admin_model->get_email('new_client_frontend');
			$body="<pre>".$email->body."<br>For future orders, please log in with:<br>Email: $billingEmail<br>Password: $password</pre>";
			$recipients=$this->admin_model->get_event_recipients($email->idemail);
			$arr=[];
			if(!empty($recipients)){
				foreach($recipients as $recipient){
					$add=$this->admin_model->get_notification($recipient['notification_emails_idnotification_emails']);
					array_push($arr,$add->email);
				}
			}
			mailer($this->admin_model->get_setting('sendemailfrom'), $email->subject, $body, $billingEmail, null, null, null);
			mailer($this->admin_model->get_setting('sendemailfrom'), 'New Customer', $this->admin_model->get_setting('site_name').' new diner customer signup: '.$billingFirstName.' '.$billingLastName, $arr, null, null, null);
		}
		
		$order_id = $this->order_model->create_order($json->from,$json->type,1,$total,$dev,$tax,$discount,$coupon_id,$json->note,$user_id,"Online",null,null,date("y-m-d H:i:s",strtotime($json->date." ".$json->time)),$tip);
		if($this->session->userdata("logged_in") == true){
			$logged_user = $this->user_model->get_user($user_id);
			$billing_id = $this->order_model->order_billing($logged_user->first_name,$logged_user->last_name,$logged_user->phone,$logged_user->email,$logged_user->address,$logged_user->city,$logged_user->province,$logged_user->postal_code,$logged_user->company,$order_id);
			if(!empty($address1) && !empty($postcode) && !empty($city) && empty($logged_user->address) && empty($logged_user->postal_code)){
				$this->user_model->update($user_id, $logged_user->first_name, $logged_user->last_name, $logged_user->phone, $logged_user->email, $address1 . " " . $address2, $city, $state, $postcode, 'Canada', $company, $logged_user->user_company_iduser_company, $logged_user->is_diner, $logged_user->eclub, $logged_user->is_catering, $logged_user->eclub_catering, $logged_user->preferred_catering_payment, $logged_user->notes);
			}
		} else{
			$billing_id = $this->order_model->order_billing($billingFirstName,$billingLastName,$billingPhone,$billingEmail,$billingAddress,$billingCity,$billingProvince,$billingPostCode,$billingCompany,$order_id);
		}
		
		if(!empty($address1) && !empty($postcode) && !empty($city)){
			$this->order_model->order_shipping($first, $last, $phone, $email, $address1 . " " . $address2, $city, $state, $postcode, $company, $order_id);
		} else if($json->type == 'Delivery'){
			if($this->session->userdata("logged_in") == true){
				$this->order_model->order_shipping($logged_user->first_name,$logged_user->last_name,$logged_user->phone,$logged_user->email,$logged_user->address,$logged_user->city,$logged_user->province,$logged_user->postal_code,$logged_user->company,$order_id);
			} else{
				$this->order_model->order_shipping($billingFirstName,$billingLastName,$billingPhone,$billingEmail,$billingAddress,$billingCity,$billingProvince,$billingPostCode,$billingCompany,$order_id);
			}
			$data['message']="Order Placed";
		echo json_encode($data);
		}
		
		
		
		$this->order_model->order_status('Created', $order_id);
		
		$amount = (($total-$discount)+$dev)+$tax;
		$data['oid']=$order_id;
		$data['amount']=$amount;
		echo json_encode($data);
		//$url = "/checkout/pay/diner/".$order_id;
		
		//redirect($url);
	}
}