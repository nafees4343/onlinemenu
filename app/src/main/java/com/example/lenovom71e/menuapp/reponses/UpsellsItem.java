
package com.example.lenovom71e.menuapp.reponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpsellsItem {

    @SerializedName("upsells")
    @Expose
    private Upsells upsells;

    public Upsells getUpsells() {
        return upsells;
    }

    public void setUpsells(Upsells upsells) {
        this.upsells = upsells;
    }

}
