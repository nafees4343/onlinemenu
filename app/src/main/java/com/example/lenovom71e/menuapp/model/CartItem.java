package com.example.lenovom71e.menuapp.model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by LENOVO on 5/9/2016.
 */
public class CartItem {
    private Product product;
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}


