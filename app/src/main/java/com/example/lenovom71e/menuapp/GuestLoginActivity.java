package com.example.lenovom71e.menuapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.Common;

import org.json.JSONException;
import org.json.JSONObject;

public class GuestLoginActivity extends AppCompatActivity implements View.OnClickListener {


    private InputMethodManager imm;
    private Boolean isCheckout;
    RequestQueue requestQueue;
    private static final String DELIVERY_URL = "https://demo.online-menu.ca/api/delivery/";
    String value;
     EditText guestEmail;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_login);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            value = intent.getExtras().getString("total");
            isCheckout = intent.getExtras().getBoolean("isCheckout");
            Log.d("checkoutvalue","chkvalue: "+value);
        }
        requestQueue = Volley.newRequestQueue(this);
        guestEmail = (EditText) findViewById(R.id.guestEmail);
        final EditText firstName = (EditText) findViewById(R.id.guestFname);
        final EditText lastName = (EditText) findViewById(R.id.guestLname);
        final EditText address = (EditText) findViewById(R.id.guestAddress);
        final EditText address2 = (EditText) findViewById(R.id.guestAddress2);
        final EditText city= (EditText) findViewById(R.id.guestCity);
        final EditText province = (EditText) findViewById(R.id.guestProvince);
        final EditText postCode= (EditText) findViewById(R.id.guestPostCode);
        final EditText phone = (EditText) findViewById(R.id.guestPhone);
        Button guestLogin = (Button) findViewById(R.id.guestLoginBtn);
        final Profile profile = ProfileHelper.getProfilehelper();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        init();

        guestLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isOnline(GuestLoginActivity.this)) {
                    String guestemail, fName, lName, addressText, address2Txt, citytxt, provinceTxt, postCodeTxt, phoneTxt;
                    guestemail = guestEmail.getText().toString();
                    fName = firstName.getText().toString();
                    lName = lastName.getText().toString();
                    addressText = address.getText().toString();
                    address2Txt = address2.getText().toString();
                    citytxt = city.getText().toString();
                    provinceTxt = province.getText().toString();
                    postCodeTxt = postCode.getText().toString();
                    phoneTxt = phone.getText().toString();

                    if (checkRequired(guestemail) && checkRequired(fName) && checkRequired(lName) && checkRequired(addressText)
                            && checkRequired(citytxt) && checkRequired(provinceTxt) && checkRequired(postCodeTxt) && checkRequired(phoneTxt)) {

                        if (!validateEmail()) {
                            return;
                        }

                        if(!validCellPhone(phoneTxt)){
                            return;
                        }

                        SaveSharedPreference.setGuestInfo(getApplicationContext(), guestemail, fName, lName, phoneTxt, addressText,
                                address2Txt, citytxt, provinceTxt, postCodeTxt);

                        JsonObjectRequest obreq = new JsonObjectRequest(DELIVERY_URL + postCodeTxt, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("DeliveryJson", response.toString());
                                try {
                                    Boolean deliveryStatus = response.getBoolean("delivery");

                                    if (!deliveryStatus) {
                                        Toast.makeText(getApplication(), "Please enter different address", Toast.LENGTH_SHORT).show();
                                        //TODO: retrieve Delivery Charge
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GuestLoginActivity.this);
                                        alertDialogBuilder.setMessage("Sorry we do not deliver at your location. Would you like to change the delivery address or choose other ordering options?");
                                        alertDialogBuilder.setPositiveButton("yes",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {

                                                    }
                                                });

                                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        });

                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();

                                    } else {
                                        String delivery_charge = response.getString("delivery_charge");
                                        // update delivery charge label and added to the total
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GuestLoginActivity.this);
                                        alertDialogBuilder.setMessage("Delivery charges are " + delivery_charge + "$. Do you want to continue?");
                                        alertDialogBuilder.setPositiveButton("yes",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        if (isCheckout) {
                                                            Intent intenta = new Intent(GuestLoginActivity.this, PassLoginActivity.class);
                                                            intenta.putExtra("isCheckout", true);
                                                            SaveSharedPreference.setAccountAsGuest(getApplicationContext(), true);
                                                            startActivity(intenta);
                                                            finish();
                                                        } else {
                                                            Intent intenta = new Intent(GuestLoginActivity.this, UserProfile.class);
                                                            SaveSharedPreference.setAccountAsGuest(getApplicationContext(), true);
                                                            startActivity(intenta);
                                                            finish();
                                                        }
                                                    }
                                                });

                                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });

                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Delivery Json", "Not working");
                            }
                        });

                        obreq.setShouldCache(false);
                        requestQueue.add(obreq);
                        //

                    /*}
                    startActivity(intent);*/
                    }
                }
                else {
                    Common.showToast(GuestLoginActivity.this,"Internet is not available...");
                }

            }
        });

    }

    public boolean validCellPhone(String number)
    {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }

    private boolean validateEmail() {
        String email = guestEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            guestEmail.setError("Invalid email");
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }



    public void init(){

        isCheckout = getIntent().getExtras().getBoolean("isCheckout");
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("GUEST SIGN IN");
        bckbtn.setOnClickListener(this);
    }


    public boolean checkRequired(String input){
        boolean done = false;
        if (input.length() == 0) {
            disPlayMessage("Please Enter all required Fields");
        }else {
            done = true;
        }
        return done;
    }

    public void disPlayMessage(String message){
        AlertDialog builder = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNegativeButton("Okay",null)
                .create();
        builder.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                finish();
                Intent inat = new Intent(GuestLoginActivity.this,CategoriesActivity.class);
                startActivity(inat);
                break;
        }
    }
}
