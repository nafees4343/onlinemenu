package com.example.lenovom71e.menuapp;

/**
 * Created by LENOVO on 5/9/2016.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.coboltforge.slidemenu.SlideMenu;
import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.adapter.ExpandableListAdapter;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.adapter.UpsellsExpendableAdapter;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.CartItem;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Extra;
import com.example.lenovom71e.menuapp.model.Product;
import com.example.lenovom71e.menuapp.reponses.UpsellsItem;
import com.example.lenovom71e.menuapp.util.Common;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.in;

public class ShoppingCartActivity extends AppCompatActivity implements SlideMenuInterface.OnSlideMenuItemClickListener, OnClickListener {
    private static final String TAG = "ShoppingCartActivity";

   // public static final IntentFilter INTENT_FILTER = createIntentFilter();

    ListView lvCartItems;
    Button bClear;
    Button bShop;
    TextView tvTotalPrice;
    TextView totalPriceLabel;
    TextView taxValueLabel;
    private SlideMenu slidemenu;
    private ProgressDialog pDialog;

    //Declair Variables BY Ali Bahrani
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<CartItem> listDataHeader = new ArrayList<>();
    HashMap<Integer, List<ExtraProuct>> listDataChild = new HashMap<>();
    ArrayList<String> extraName = new ArrayList<String>();
    List<String> extraNameList = new ArrayList<String>();
    ArrayList<Integer> headers = new ArrayList<>();
    HashMap<Integer, List<ExtraProuct>> extraList = new HashMap<>();
    BigDecimal result = BigDecimal.ZERO;
    ArrayList<Saleable> headerProducts = new ArrayList<>();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    Toolbar toolbar;
    private final Handler handler = new Handler();
    private Cart cart;
    private boolean isExtras = false;
    String url = "https://demo.online-menu.ca/api/upsells";
    String value;
    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */


    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_cart);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        init();
        setSupportActionBar(toolbar);
        setUpSlideMenu();


        cart = CartHelper.getCart();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             value = extras.getString("PREVIOUS");
            //The key argument here must match that used in the other activity
        }else {
            value = "abc";
        }


        //Adding intent to the activity By Ali Bahrani
        final Intent intent = this.getIntent();
        extraName = (ArrayList<String>) getIntent().getSerializableExtra("extra2List");
        headers =  cart.getProductHeaderId(); // header items
        Log.d("Headers List from Detail: ", String.valueOf(headers));
        extraList = cart.getIdProductExtraList(); // child elements
        Log.d("HasMap from Detail: ", String.valueOf(extraList));
        extraNameList = extraName;


        //Define ExpandableList By Ali Bahrani
        //get the listView
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        //preparing list data
//        listDataHeader = getCartItems(cart);
//        listDataHeader = getCartItems(cart);
        final HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantity = cart.getProductExtraListMap();
        final List<Integer> productQuantities = cart.getProductQuantities();
        final HashMap<Saleable, List<ExtraProuct>> productsWithExtraList = cart.getProductExtraList();
        headerProducts = (ArrayList<Saleable>) cart.getProductsHeader();


        Log.d("ListDataHeader : " , String.valueOf(listDataHeader));
        listDataChild = extraList;
//        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        //listAdapter = new ExpandableListAdapter(this, headers, extraList, listDataHeaderWQuantity, productQuantities);

        listAdapter = new ExpandableListAdapter(this, headers, extraList, listDataHeaderWQuantity, productQuantities, headerProducts,value);

        expListView.setAdapter(listAdapter);


        for (int i= 0; i < headers.size(); i ++) {
            result = result.add(cart.getProductTotalPrice(headers,extraList,i,productQuantities.get(i)));
        }


        Log.d("Result of the productItem is : ", String.valueOf(result));

        tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        //tvTotalPrice.setText(String.valueOf(cart.getTotalPrice()) );
        tvTotalPrice.setText("" + result);


        totalPriceLabel = (TextView) findViewById(R.id.tvGrandTotal);

        totalPriceLabel.setText(String.valueOf(cart.getGrandTotalPrice(result,cart.getTaxValue(result))));
        taxValueLabel = (TextView) findViewById(R.id.tvTax);
        taxValueLabel.setText(String.valueOf(cart.getTaxValue(result)));



        //Update View when item deleted from the list

        listAdapter.setOnDataChangeListener(new ExpandableListAdapter.OnDataChangeListener() {
            public void onDataChanged(int size){
                //do whatever here
                BigDecimal result = BigDecimal.ZERO;

//                headers =  cart.getProductsHeader(); // header items
                headers = cart.getProductHeaderId();
//                extraList = cart.getExtraProduct(); // child elements
                extraList = cart.getIdProductExtraList();
                List<Integer> productQuantities = cart.getProductQuantities(); // headers Quantities
                for (int i= 0; i < headers.size(); i ++) {
                    result = result.add(cart.getProductTotalPrice(headers,extraList,i,productQuantities.get(i)));
                }

                tvTotalPrice.setText(String.valueOf(result));
                taxValueLabel.setText(String.valueOf(cart.getTaxValue(result)));
                totalPriceLabel.setText(String.valueOf(cart.getGrandTotalPrice(result,cart.getTaxValue(result))));
                cart.updateItemPrice(result);


                if(cart.getProductHeaderId().size() == 0 ){
                    Toast.makeText(ShoppingCartActivity.this,"Cart is empty",Toast.LENGTH_SHORT).show();
                    Common.clearCart();
                    finish();
                }


            }
        });



        //lvCartItems.addHeaderView(layoutInflater.inflate(R.layout.cart_header, lvCartItems, false));
//        final CartItemAdapter cartItemAdapter = new CartItemAdapter(this);
//        cartItemAdapter.updateCartItems(getCartItems(cart));
//
//
//        Log.d("passed extra: ", String.valueOf(extraName));

        //lvCartItems.setAdapter(cartItemAdapter);

        bClear = (Button) findViewById(R.id.bCheckout);
        bShop = (Button) findViewById(R.id.bShop);

        bClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isOnline(ShoppingCartActivity.this)) {
                    CheckOut checkout = CheckoutHelper.getCheckout();
                    Cart cart = CartHelper.getCart();
                    if (cart.getProductHeaderId().size() > 0) {
                        if (!checkout.getDessert())
                            askForDesserts();
                        else {
                            doCheckout();
                        }
                    } else {
                        Common.showToast(ShoppingCartActivity.this, "Cart is empty...");
                    }

                }
                else {
                    Common.showToast(ShoppingCartActivity.this, "Internet not available");
                }
            }
        });

        bShop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isOnline(ShoppingCartActivity.this)) {
                    Intent intent = new Intent(ShoppingCartActivity.this, CategoriesActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Common.showToast(ShoppingCartActivity.this, "Internet not available");
                }
            }
        });


    }

    public void doCheckout(){

            CheckOut checkout = CheckoutHelper.getCheckout();

            Log.d(TAG, "Clearing the shopping cart");
            HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantities = cart.getProductExtraListMap();
            //TODO: Implement the array of (headers, extraList, listDataHeaderWQuantity, productQuantities, headerProducts)
            int productId, productQnatity, extraId;

            final HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantity = cart.getProductExtraListMap();
            final List<Integer> productQuantities = cart.getProductQuantities();
            final HashMap<Saleable, List<ExtraProuct>> productsWithExtraList = cart.getProductExtraList();
            headerProducts = (ArrayList<Saleable>) cart.getProductsHeader();

            ArrayList<Integer> listHeader = checkout.getProductHeaderId();
            String tax = (String) taxValueLabel.getText();
            checkout.setmTaxValue(BigDecimal.valueOf(Double.valueOf(tax)));
            checkout.setmSubTotal(BigDecimal.valueOf(Double.valueOf(Double.valueOf((String) tvTotalPrice.getText()))));
            checkout.setmCheckOutTotal(BigDecimal.valueOf(Double.valueOf((String) totalPriceLabel.getText())));
            checkout.setmQuantity(headers.size());
            checkout.setProductQuantities((ArrayList<Integer>) productQuantities);
            ArrayList<ExtraProuct> extras = new ArrayList<>();

            int pid = 0;
            int productQuantity;
            BigDecimal pPrice = BigDecimal.ZERO;


            //for Quantity

            for (int i = 0; i < headerProducts.size(); i++) {

                int headerid = headerProducts.get(i).getitemId();
                productId = headers.get(i);
                if (headerid == productId) {
                    Saleable currentProduct = headerProducts.get(i);
                    pid = currentProduct.getproductId();
                    pPrice = currentProduct.getPrice();
                    //additional
                    //productQuantity = currentProduct.getquantity();
                    extras.addAll(extraList.get(productId));
                   // Log.d("Items are : ", "Product id" + pid +"\nProduct Quantity: "+productQuantity + " price: " + pPrice + " Extras :" + extras.get(i).getExtraId());
                }
                Log.d("Items are : ", String.valueOf(pid));
                headerProducts.get(i).getPrice();
                //yaha qty save horaen

                productQnatity = productQuantities.get(i);
                extraList.get(productId);
                Log.d("checkoutvariables", "pid: " + productId + "\nPqty: " + productQnatity + "\nExtragetPid: " + extraList.get(productId));
            }

            //Make Post using Volley Library

            //cart.clear();
            getTimeStamp();
//                cartItemAdapter.updateCartItems(getCartItems(cart));
//                cartItemAdapter.notifyDataSetChanged();
//                tvTotalPrice.setText(String.valueOf(cart.getTotalPrice()));
//                taxValueLabel.setText("0.00");
//                totalPriceLabel.setText("0.00");
            Log.d("shoppingcart.result", "res" + result);

            Intent intent2 = new Intent(ShoppingCartActivity.this, CheckoutActivity.class);
            startActivity(intent2);
            finish();


//                if(!isExtras) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ShoppingCartActivity.this);
//                    builder.setMessage("Would you also like Dessert")
//                            .setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    doCheckout();
//                                }
//                            })
//                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//
//                                }
//                            });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
//
//                }
    }

    public void init(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        title.setText("Cart");
        navigation.setOnClickListener(this);
    }


    private String getTimeStamp(){
        Long tsLong  = currentTimeMillis();
        return sdf.format(tsLong);

    }

    private List<CartItem> getCartItems(Cart cart) {
        List<CartItem> cartItems = new ArrayList<CartItem>();
        Log.d(TAG, "Current shopping cart: " + cart);

        Map<Saleable, Integer> itemMap = (Map<Saleable, Integer>) cart.getItemWithQuantity();

        for (Entry<Saleable, Integer> entry : itemMap.entrySet()) {
            CartItem cartItem = new CartItem();
            cartItem.setProduct((Product) entry.getKey());
            cartItem.setQuantity(entry.getValue());
            cartItems.add(cartItem);
        }

        Log.d(TAG, "Cart item list: " + cartItems);
        return cartItems;
    }

    @Override
    public void onBackPressed() {
        if(value!=null && value.equals("Category")){
            Intent i = new Intent(ShoppingCartActivity.this,CategoriesActivity.class);
            startActivity(i);
            finish();
        }
        else {
            super.onBackPressed();
        }
    }

    public void setUpSlideMenu(){
        slidemenu = (SlideMenu) findViewById(R.id.slideMenu);


        if(SaveSharedPreference.getCheckLogInState(ShoppingCartActivity.this).equals("LOGIN")) {
            slidemenu.init(this, R.menu.slide, this, 333);
        }
        else {
            slidemenu.init(this, R.menu.slideforwithoutlogin, this, 333);
        }
        SlideMenu.SlideMenuItem item = new SlideMenu.SlideMenuItem();
        slidemenu.addMenuItem(item);
    }

    @Override
    public void onSlideMenuItemClick(int itemId) {
        switch (itemId) {
            case R.id.item_one:
                Intent p = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(p);
                finish();
                break;
            case R.id.item_two:
                Intent j = new Intent(getApplicationContext(), CategoriesActivity.class);
                startActivity(j);
                finish();
                break;
            case R.id.item_four:
                Common.clearCart();
                finishAffinity();
                break;
            case R.id.item_fifth:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://demo.online-menu.ca/customer"));
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_navigation:
                slidemenu.show(CartHelper.getCart().getTotalPriceWithExtra().toString());
                break;
        }
    }

    public void askForDesserts(){
       /* AlertDialog.Builder builder = new AlertDialog.Builder(ShoppingCartActivity.this);
        builder.setMessage("Would you also like some ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(ShoppingCartActivity.this, DessertsActivity.class);
                        startActivityForResult(intent, 110);

                    }
                })
                .setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CheckOut checkout = CheckoutHelper.getCheckout();
                        checkout.setDessert(true);
                        doCheckout();
                    }
                })
                .setCancelable(false)
                .create()
                .show();*/
       pDialog.show();
        JsonObjectRequest movieReq = new JsonObjectRequest(url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                           List<String> list = upsellsJsonArray(jsonObject.getJSONObject("upsells"));
                           String [] upsells = new String[list.size()];
                           for(int i=0;i<list.size();i++){
                               upsells[i] = list.get(i);
                           }

                            showUpsellsOptions(upsells);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        hidePDialog();
                        // Parsing json

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);



    }

    private List<String> upsellsJsonArray(JSONObject json) {
        Iterator<String> keys = json.keys();
        List<String> list = new ArrayList<>();
        while (keys.hasNext()) {
            String key = keys.next();
            list.add(key);
        }
        return list;
    }

    private void showUpsellsOptions(final String[] values) {

        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(ShoppingCartActivity.this);
        LayoutInflater inflater = ShoppingCartActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.upsells_dialog_list_layout, null);
        dialogBuilder.setView(dialogView);

        final ListView listView = (ListView) dialogView.findViewById(R.id.listview);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(Common.isOnline(ShoppingCartActivity.this)) {
                    String itemValue = (String) listView.getItemAtPosition(position);
                    Intent intent = new Intent(ShoppingCartActivity.this, DessertsActivity.class);
                    intent.putExtra("upsells", values[position]);
                    startActivityForResult(intent, 110);
                    alertDialog.hide();
                }
                else {
                    Common.showToast(ShoppingCartActivity.this,"Internet not available");
                }
            }

        });

        Button cancel_btn = (Button) dialogView.findViewById(R.id.button_no);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isOnline(ShoppingCartActivity.this)) {
                    CheckOut checkout = CheckoutHelper.getCheckout();
                    checkout.setDessert(true);
                    doCheckout();
                    alertDialog.hide();
                }
                else {
                    Common.showToast(ShoppingCartActivity.this,"Internet not available");
                }
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 110 && resultCode == 200){
            listAdapter.notifyDataSetChanged();
            BigDecimal result = BigDecimal.ZERO;

//                headers =  cart.getProductsHeader(); // header items
            headers = cart.getProductHeaderId();
//                extraList = cart.getExtraProduct(); // child elements
            extraList = cart.getIdProductExtraList();
            List<Integer> productQuantities = cart.getProductQuantities(); // headers Quantities
            for (int i= 0; i < headers.size(); i ++) {
                result = result.add(cart.getProductTotalPrice(headers,extraList,i,productQuantities.get(i)));
            }

            tvTotalPrice.setText(String.valueOf(result));
            taxValueLabel.setText(String.valueOf(cart.getTaxValue(result)));
            totalPriceLabel.setText(String.valueOf(cart.getGrandTotalPrice(result,cart.getTaxValue(result))));
            cart.updateItemPrice(result);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(cart.getProductHeaderId().size() <= 0 ){
            Common.showToast(ShoppingCartActivity.this,"Cart is empty");
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }
}
