
package com.example.lenovom71e.menuapp.reponses;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("extras")
    @Expose
    private List<Integer> extras = null;
    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("qty")
    @Expose
    private String qty;

    public List<Integer> getExtras() {
        return extras;
    }

    public void setExtras(List<Integer> extras) {
        this.extras = extras;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

}
