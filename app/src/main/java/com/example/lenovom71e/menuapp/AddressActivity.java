package com.example.lenovom71e.menuapp;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.model.Address;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.util.Common;
import com.example.lenovom71e.menuapp.util.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener {

     EditText sFirstName;
     EditText sLastName;
     EditText newAddress;
     EditText newAddress1;
     EditText newCity;
     EditText newProvince;
     EditText newPostcode;
     EditText deliveryCompanyEmail;
     EditText shippingFirstname;
     EditText shippingLastname;
     EditText guestPhone;
     String postcode,orderType;
     private static final String DELIVERY_URL = "https://demo.online-menu.ca/api/delivery/";
     RequestQueue requestQueue;
     SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_address);

        prefs = getSharedPreferences(getApplicationContext().getPackageName(), MODE_PRIVATE);


        requestQueue = Volley.newRequestQueue(this);



        newAddress = (EditText) findViewById(R.id.newAddress);
        newAddress1 = (EditText) findViewById(R.id.newAddress1);
        newCity = (EditText) findViewById(R.id.newCity);
        newProvince = (EditText) findViewById(R.id.newProvince);
        newPostcode = (EditText) findViewById(R.id.newPostCode);
        guestPhone = findViewById(R.id.differentPhone);
        deliveryCompanyEmail = (EditText) findViewById(R.id.shippingCompanyEmail);
        shippingFirstname = (EditText) findViewById(R.id.sFirstName);
        shippingLastname = (EditText) findViewById(R.id.sLastname);


        Button submitNewAddress = (Button) findViewById(R.id.bSubmitNewAddress);

        final CheckOut checkout = CheckoutHelper.getCheckout();

        init();

        submitNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isEmpty(shippingFirstname)& !isEmpty(shippingLastname) & !isEmpty(newAddress) & !isEmpty(newCity) & !isEmpty(newProvince) & !isEmpty(newPostcode) & !isEmpty(deliveryCompanyEmail) & !isEmpty(shippingFirstname) & !isEmpty(shippingLastname)){
                checkout.setDeliveryAddress(newAddress.getText().toString());
                checkout.setDeliveryAddress1(newAddress1.getText().toString());
                checkout.setPhoneno(guestPhone.getText().toString());
                checkout.setDeliveryCity(newCity.getText().toString());
                checkout.setDeliveryProvince(newProvince.getText().toString());
                checkout.setDeliveryPostcode(newPostcode.getText().toString());
                checkout.setDeliveryCompanyEmail(deliveryCompanyEmail.getText().toString());
                checkout.setShippingFirstName(shippingFirstname.getText().toString());
                checkout.setShippingLastName(shippingLastname.getText().toString());

                Log.d("no quotes:","name: "+checkout.getShippingFirstName());
                //Adding shipping firstname and lastname by Ali March 2nd,2018
                    if (!validateEmail()) {
                        return;
                    }
                    send_network_call();

                }
                else {
                    Common.showToast(AddressActivity.this, "Some thing is missing");

                }
            }
        });
    }


    public void send_network_call(){

        final CheckOut checkOut= CheckoutHelper.getCheckout();

        orderType = "Delivery";


        postcode = checkOut.getDeliveryPostcode();


        //deliveryLabel.setText(delivery_charge + "CAD");
        Log.d("Post Code: ",""+postcode);

        JsonObjectRequest obreq = new JsonObjectRequest(DELIVERY_URL + postcode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressDialogClass.dismissRoundProgress();

                Log.d("DeliveryJson", response.toString());
                try{
                     checkOut.setDeliveryIsDifferent(response.getBoolean("delivery"));

                    String delivery_charge;
                    if (!checkOut.getDeliveryIsDifferent()){
                        Toast.makeText(getApplication(),"Please enter different address",Toast.LENGTH_SHORT).show();

                        //TODO: retrieve Delivery Charge

                        //updateTextViewTotal(delivery_charge,coupon,tipEditText);

                    }else {
                        delivery_charge = response.getString("delivery_charge");
                        //Toast.makeText(getApplication(),"Your Delivery Charges is "+delivery_charge+"$",Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(AddressActivity.this);
                        builder1.setMessage("Your delivery charges is "+delivery_charge+"$");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        SharedPreferences.Editor editor = prefs.edit();
                                        editor.remove("rbtnDifferent");
                                        editor.apply();

                                        dialog.cancel();

                                        finish();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgressDialogClass.dismissRoundProgress();
                Log.d("Delivery Json", "Not working");
            }
        });

        obreq.setShouldCache(false);
        requestQueue.add(obreq);
        ProgressDialogClass.showRoundProgress(AddressActivity.this,"Loading...");
    }

    private boolean validateEmail() {

        String email = deliveryCompanyEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            deliveryCompanyEmail.setError("Invalid email");
            return false;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public void init(){
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("ADDRESS");
        bckbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                Log.d("nifhsi","finish");

                SharedPreferences.Editor editor = prefs.edit();
                editor.remove("rbtnDifferent");
                editor.apply();

                finish();
                break;
        }
    }

}
