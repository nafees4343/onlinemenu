package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by admin on 2017-08-23.
 */

public class SaveSharedPreference {
    static final String PREF_USER_NAME = "username";
    static final String PREF_PASSWORD = "password";
    static final String PREF_LOGIN_STATE = "LOGIN";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static void setAccountAsGuest(Context ctx, Boolean isGuest){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putBoolean("isGuest", isGuest);
        editor.commit();
    }
    public static boolean getAccountAsGuest(Context ctx){
        return getSharedPreferences(ctx).getBoolean("isGuest", false);
    }

    public static void setGuestInfo(Context ctx,String guestemail, String fname, String lName,String phone, String address,
                                    String address2, String city, String province, String postalCode){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();

        editor.putString("guestemail", guestemail);
        editor.putString("fname", fname);
        editor.putString("lname", lName);
        editor.putString("address", address);
        editor.putString("address2", address2);
        editor.putString("city", city);
        editor.putString("provice", province);
        editor.putString("postCode", postalCode);
        editor.putString("phone", phone);
        editor.commit();
    }

    public static String getGuestemail(Context ctx){
        return getSharedPreferences(ctx).getString("guestemail", "");
    }
    public static String getGuestfname(Context ctx){
        return getSharedPreferences(ctx).getString("fname", "");
    }

    public static String getGuestlname(Context ctx){
        return getSharedPreferences(ctx).getString("lname", "");
    }

    public static String getGuestAddress(Context ctx){
        return getSharedPreferences(ctx).getString("address", "");
    }
    public static String getGuestaddress1(Context ctx){
        return getSharedPreferences(ctx).getString("address2", "");
    }
    public static String getGuestcity(Context ctx){
        return getSharedPreferences(ctx).getString("city", "");
    }
    public static String getGuestProvince(Context ctx){
        return getSharedPreferences(ctx).getString("provice", "");
    }
    public static String getGuestPostcode(Context ctx){
        return getSharedPreferences(ctx).getString("postCode", "");
    }
    public static String getGuestphone(Context ctx){
        return getSharedPreferences(ctx).getString("phone", "");
    }


    public static void setPassword(Context ctx, String password){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_PASSWORD, password);
        editor.commit();
    }

    public static String getPassword(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_PASSWORD, "");
    }

    public static  String getUserName(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void SetCheckLogInState(Context ctx, String LoginState){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_LOGIN_STATE, LoginState);
        editor.commit();
    }

    public static String getCheckLogInState(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_LOGIN_STATE, "");
    }

    public static void clearUserName(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear();
        editor.commit();
    }

    public static void clearshareprefrence(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }


    public static void saveCredentials(Context context ,String name,String lastName,String userId,String phone,String email,String postcode,String billingCompany,String billingEmail,String address,String city,String province) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("name",name+"");
        editor.putString("lastName",lastName+"");
        editor.putString("userId",userId+"");
        editor.putString("phone",phone+"");
        editor.putString("email",email+"");
        editor.putString("postcode",postcode+"");
        editor.putString("billingCompany",billingCompany+"");
        editor.putString("billingEmail",billingEmail+"");
        editor.putString("address",address+"");
        editor.putString("city",city+"");
        editor.putString("province",province+"");

        editor.commit();
    }

    public static String getName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("name", "").length() > 0) {
            return prefs.getString("name", "");
        } else
            return "";
    }

    public static String getlastName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("lastName", "").length() > 0) {
            return prefs.getString("lastName", "");
        } else
            return "";
    }

    public static String getUserId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("userId", "").length() > 0) {
            return prefs.getString("userId", "");
        } else
            return "";
    }

    public static String getPhone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("phone", "").length() > 0) {
            return prefs.getString("phone", "");
        } else
            return "";
    }

    public static String getEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("email", "").length() > 0) {
            return prefs.getString("email", "");
        } else
            return "";
    }



    public static String getPostcode(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("postcode", "").length() > 0) {
            return prefs.getString("postcode", "");
        } else
            return "";
    }
    public static String getbillingCompany(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("billingCompany", "").length() > 0) {
            return prefs.getString("billingCompany", "");
        } else
            return "";
    }

    public static String getbillingEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("billingEmail", "").length() > 0) {
            return prefs.getString("billingEmail", "");
        } else
            return "";
    }

    public static String getAddress(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("address", "").length() > 0) {
            return prefs.getString("address", "");
        } else
            return "";
    }
    public static String getCity(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("city", "").length() > 0) {
            return prefs.getString("city", "");
        } else
            return "";
    }
    public static String getProvince(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("province", "").length() > 0) {
            return prefs.getString("province", "");
        } else
            return "";
    }

}
