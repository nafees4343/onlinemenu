package com.example.lenovom71e.menuapp.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.PassLoginActivity;
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.RegisterReqeustDelivery;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by admin on 2017-08-09.
 */

public class RegisterDeliveryActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_delivery);
        final EditText etname = (EditText) findViewById(R.id.etfName);
        final EditText etLname = (EditText) findViewById(R.id.etlName);
        final EditText etPhoneNumber = (EditText) findViewById(R.id.rPhoneNumber);
        final EditText etEmail = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final EditText etCompanyName = (EditText) findViewById(R.id.etCompanyName);
        final EditText etAddress = (EditText) findViewById(R.id.etAddress);
        final EditText etAddress1 = (EditText) findViewById(R.id.etAddress1);
        final EditText etcity = (EditText) findViewById(R.id.etCity);
        final EditText etProvince = (EditText) findViewById(R.id.etProvince);
        final EditText etPostCode = (EditText) findViewById(R.id.etPostCode);
        final Button bRegisterDelivery = (Button) findViewById(R.id.bDeliveryRegister);

        init();

        bRegisterDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = etname.getText().toString();
                final String lName = etLname.getText().toString();
                final String phoneNumber =etPhoneNumber.getText().toString();
                final String email = etEmail.getText().toString();
                final String password = etPassword.getText().toString();
                final String companyName = etCompanyName.getText().toString();
                final String address = etAddress.getText().toString();
                final String address1 = etAddress1.getText().toString();
                final String city = etcity.getText().toString();
                final String province = etProvince.getText().toString();
                final String postCode = etPostCode.getText().toString();
                final String country = "Canada";



                Log.d("RegisterButton" , "Register Delivery Has been Click");
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            if (success) {
                                Intent intent = new Intent(RegisterDeliveryActivity.this, PassLoginActivity.class);
                                RegisterDeliveryActivity.this.startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterDeliveryActivity.this);
                                builder.setMessage("Register Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                RegisterReqeustDelivery registerReqeustDelivery= new RegisterReqeustDelivery(name,lName,phoneNumber, email, password,
                        companyName, address, address1, city, province, postCode, country, responseListener);
                RequestQueue queue = Volley.newRequestQueue(RegisterDeliveryActivity.this);
                queue.add(registerReqeustDelivery);
            }
        });
    }

    public void init(){
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("REGISTER");
        bckbtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                finish();
                break;
        }
    }



}

