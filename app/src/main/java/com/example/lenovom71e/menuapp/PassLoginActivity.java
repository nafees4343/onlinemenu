package com.example.lenovom71e.menuapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.exception.ProductNotFoundException;
import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.Service.OrderSendClient;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.CartItem;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.example.lenovom71e.menuapp.model.Product;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.reponses.Item;
import com.example.lenovom71e.menuapp.reponses.OrderPlace;
import com.example.lenovom71e.menuapp.util.Common;
import com.example.lenovom71e.menuapp.util.OrderSend;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PassLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private String userId, accountNumber, email,firstName,LastName, orderType, postcode;
    private static final String DELIVERY_URL = "https://demo.online-menu.ca/api/delivery/";
    RequestQueue requestQueue;
    String delivery_charge = "0";
    String orderNumber = "";
    String total = "";
    String coupon = "";
    String dateToString = "";
    String address = "";
    String address1 = "";
    String city = "";
    String postCode = "";
    String province = "";
    String firstname = "";
    String lastName = "";
    String phoneNumber = "";
    String user = "0";
    String deliveryAddreess = "",deliveryAddress2 = "",deliverypostCode = "",deliveryCity = "",deliveryProvince = "",companyName = "",companyEmail = "",tipEditText = "0";
    double tipValue = 0.0;
    ProgressDialog pDialog;
    private InputMethodManager imm;
    String value;
    private Map<Saleable, Integer> cartItemMap = new HashMap<Saleable, Integer>();
    Cart cart;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pass);
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        cart = CartHelper.getCart();

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);

        title.setText("Your Bill");
        bckbtn.setOnClickListener(this);

        // Showing progress dialog before making http request
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        TextView welcomeText = (TextView) findViewById(R.id.welcomeMessage);
        TextView accountNumberText = (TextView) findViewById(R.id.billAccountNumber);
        TextView emailText = (TextView) findViewById(R.id.billEmail);
        TextView phoneText = (TextView) findViewById(R.id.billPhone);
        TextView orderTypeId = (TextView) findViewById(R.id.billOrderId);
        TextView orderDateText = (TextView) findViewById(R.id.billOrderDate);
        final TextView totalPaymentText = (TextView) findViewById(R.id.billTotalPayment);

        TextView billPriceText = (TextView) findViewById(R.id.billPrice);

        Button placeOrderButton = (Button) findViewById(R.id.placeOrder);
        final Button couponDiscountBtn = (Button) findViewById(R.id.discuoutButton);
        final TextView orderTimeText = (TextView) findViewById(R.id.billOrderTime);
        final EditText discountCodeTxt = (EditText) findViewById(R.id.discountCodeTxt);
       // TextView notGuest = (TextView) findViewById(R.id.notUserTxt);
        requestQueue = Volley.newRequestQueue(this);
        final TextView deliveryLabel = (TextView) findViewById(R.id.delivery_Charge);
        final EditText tip =  (EditText) findViewById(R.id.tip);



        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext()) == true){
            firstName = SaveSharedPreference.getGuestfname(getApplicationContext());
            LastName = SaveSharedPreference.getGuestlname(getApplicationContext());
            accountNumber = "Guest";
            email = SaveSharedPreference.getGuestemail(getApplicationContext());
            phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());
          //  notGuest.setText("Not guest ? Please Click here to Sign in / Sign up");
            postcode = SaveSharedPreference.getGuestPostcode(getApplicationContext());
        }else {
            accountNumber = SaveSharedPreference.getUserId(getApplication());
            firstName = SaveSharedPreference.getUserName(getApplication());
            LastName = SaveSharedPreference.getlastName(getApplication());
            email = SaveSharedPreference.getEmail(getApplication());
            phoneNumber = SaveSharedPreference.getPhone(getApplication());
            postcode = SaveSharedPreference.getPostcode(getApplication());
            userId = SaveSharedPreference.getUserId(getApplication());
           Log.d("DETAILS","acc number: "+accountNumber+"\nfirst name: "+firstName+"\nemail: "+email+"\nphone no: "+phoneNumber+"\npost code: "+postcode+"\nUser id: "+userId);
        }





        /*notGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PassLoginActivity.this, LoginActivity.class);
                SaveSharedPreference.clearUserName(getApplicationContext());
                startActivity(intent);
            }
        });*/

        //Log.d("LoginPassActivity", accountNumber);
        welcomeText.setText("Thanks for Logging in");
        accountNumberText.setText(accountNumber);
        emailText.setText(email);
        phoneText.setText(phoneNumber);



        //so lets assume i going to use the checkout to store data from this class
        //all what i have to do is to save to the checkout

        final CheckOut checkOut= CheckoutHelper.getCheckout();


        checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal());
        if (checkOut.isDeliver()){
            orderType = "Delivery";

            if (checkOut.getDeliveryIsDifferent() == true) {
                postcode = checkOut.getDeliveryPostcode();
            }


            deliveryLabel.setText(delivery_charge + "CAD");
            Log.d("Post Code: ",""+postcode);

            JsonObjectRequest obreq = new JsonObjectRequest(DELIVERY_URL + postcode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("DeliveryJson", response.toString());
                    try{
                        Boolean deliveryStatus =  response.getBoolean("delivery");

                        if (deliveryStatus == false && checkOut.getDeliveryIsDifferent() == false){
                            Toast.makeText(getApplication(),"Please enter different address",Toast.LENGTH_SHORT).show();
                            onBackPressed();

                            //TODO: retrieve Delivery Charge
                            updateTextViewTotal(delivery_charge,coupon,tipEditText);


                        }else {
                            delivery_charge = response.getString("delivery_charge");
                            // update delivery charge label and added to the total
                            deliveryLabel.setText(delivery_charge + "CAD");
                            updateTextViewTotal(delivery_charge,coupon,tipEditText);
                        }

                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Delivery Json", "Not working");
                }
            });

            obreq.setShouldCache(false);
            requestQueue.add(obreq);


        }else if(checkOut.isEatin()){
            orderType = "Eatin";
            delivery_charge = "0";

        }else if (checkOut.isPickup()){
            orderType = "Pickup";
            delivery_charge = "0";
        }

        orderTypeId.setText(orderType);
        orderDateText.setText(checkOut.getOrderDate());
        billPriceText.setText(checkOut.getmCheckOutTotal() + "$ CAD");
        updateTextViewTotal(delivery_charge,coupon,tipEditText);
        orderTimeText.setText(checkOut.getOrderTime());

        tip.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    tipEditText = tip.getText().toString();
                    if(tipEditText.isEmpty()) {
                        tipEditText = "0";
                    }
                    updateTextViewTotal(delivery_charge,coupon,tipEditText);
                }
                return false;
            }
        });

        tip.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                tipEditText = tip.getText().toString();
                if(tipEditText.isEmpty()) {
                    tipEditText = "0";
                    Log.d("tipppppp","check mei agya: "+tip);

                }
                Log.d("tipppppp","agya: "+tipEditText);
                updateTextViewTotal(delivery_charge,coupon,tipEditText);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });


        tip.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    tipEditText = tip.getText().toString();
                    if(tipEditText.isEmpty()) {
                        tipEditText = "0";
                    }
                    updateTextViewTotal(delivery_charge,coupon,tipEditText);
                }
                return false;
            }
        });


        //TODO: Get Coupon code and price.


        placeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isOnline(PassLoginActivity.this)) {
//                Intent paymentIntent = new Intent(PassLoginActivity.this, CreditPayment.class);
//                paymentIntent.putExtra("accountNumber", accountNumber);
//                paymentIntent.putExtra("email", email);
//                paymentIntent.putExtra("phone", phoneNumber);
//                paymentIntent.putExtra("type",orderType);
//                paymentIntent.putExtra("discountCode", discountCodeTxt.getText().toString());
                    pDialog.show();
                    HashMap<String, Integer> itemExtra = new HashMap<>();
//                Object[] cartItem = cart.getIdProductExtraList().values().toArray();
                    Object[] extra = checkOut.getIdProductExtraList().values().toArray();
                    Object extra1 = checkOut.getIdProductExtraList().values().iterator().next().iterator().next().getExtraId();
                    Log.d("Extra values are ", extra1.toString());
                    Log.d("Extra values are ", extra.toString());

                    //TODO: Add products list here

                    HashMap<Saleable, List<ExtraProuct>> cartItem = cart.getProductExtraList();
                    //Integer ProductId = cartItem.keySet().iterator().next().getproductId();
                    List<Object[]> productextras = new ArrayList<>();
                    List<Integer> extras = new ArrayList<>();

                    List<Integer> pids = new ArrayList<>();

                    Map<Saleable, Integer> cartItemMap = new HashMap<Saleable, Integer>();


                    for (Saleable key : cartItem.keySet()) {
                        Integer pid = key.getproductId();//product id hai ye
                        String pname = key.getPname();//product name
                        Log.d("productId:", "id: " + pid + "\nPname: " + pname + "\nqty:");
                        //if product id is not to zero then add it to list
                        if (cart.getProductQuantity(pid) != 0) {
                            Log.d("CARTITEMSFORORDER:", "Add id: " + pid + "\nqty: " + cart.getProductQuantity(pid));
                            pids.add(pid);
                        } else {
                            cartItemMap.remove(key);
                            Log.d("CARTITEMSFORORDER:", "remove id: " + pid + "\nqty: " + cart.getProductQuantity(pid));

                        }
                    }

                    for (List<ExtraProuct> extralist : cartItem.values()) {
                        //Log.d("Exxxxxxtrasize","size: "+extralist.size());
                        for (ExtraProuct list : extralist) {
                            Log.d("Exxxxxxtras","Name: "+list.getExtraName()+"\nprice: "+list.getExtraPrice()+"eid: "+list.getExtraId());

                            if (list.getExtraId() != 0) {
                               // Log.d("Exxxxxxtrass","Name: "+list.getExtraName()+"\nprice: "+list.getExtraPrice());
                                extras.add(list.getExtraId());
                            }
                        }

                        Log.d("",""+extras);
                        productextras.add(extras.toArray());
                        extras.clear();
                    }

                    //               get tip value
                    //Submitting Order
                    ArrayList<OrderSend.Item> items = new ArrayList<>();

                    Log.d("piddd", "pid: " + pids.size());
                    //List<Item> items = new ArrayList<>();
                    int s = pids.size() - 1;
                    for (int i = 0; i < pids.size(); i++) {

                        Log.d("OrderCount", "Pid: " + String.valueOf(pids.get(i)) + "\nQuantities: " + String.valueOf(cart.getProductQuantity(pids.get(i))) + "\npextrasid: " + productextras.get(i));
                        items.add(new OrderSend.Item(String.valueOf(pids.get(i)), String.valueOf(cart.getProductQuantity(pids.get(i))), Double.valueOf(tipEditText), productextras.get(i)));
                    }

                    pids.clear();
                    cartItem.clear();

                    String strDate = checkOut.getOrderDate();
                    try {
                        SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
                        SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
                        Date date = formate.parse(strDate);
                        dateToString = output.format(date);
                        Log.d("Date is : ", date.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //TODO : Check weather the user Loged in or guest and save its data


                    //TODO: Get User data here

                    if (accountNumber == "Guest") {
                        Log.d("Come", "1");
                        //Guest Parameters for Delivery
                        firstname = SaveSharedPreference.getGuestfname(getApplicationContext());
                        lastName = SaveSharedPreference.getGuestlname(getApplicationContext());
                        phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());
                        address = SaveSharedPreference.getGuestAddress(getApplicationContext());
                        address1 = SaveSharedPreference.getGuestaddress1(getApplicationContext());
                        postCode = SaveSharedPreference.getGuestPostcode(getApplicationContext());
                        city = SaveSharedPreference.getGuestcity(getApplicationContext());
                        province = SaveSharedPreference.getGuestProvince(getApplicationContext());

                        if (checkOut.getDeliveryIsDifferent() == true) {
                            Log.d("Come", "2");
                            deliveryAddreess = checkOut.getDeliveryAddress();
                            deliveryAddress2 = checkOut.getDeliveryAddress1();
                            deliverypostCode = checkOut.getDeliveryPostcode();
                            deliveryCity = checkOut.getDeliveryCity();
                            deliveryProvince = checkOut.getDeliveryProvince();
                            companyName = checkOut.getDeliveryCompanyName();
                            companyEmail = checkOut.getDeliveryCompanyEmail();
                        }
                    } else {
                        if (checkOut.getDeliveryIsDifferent() == true) {
                            Log.d("Come", "3");
                            // Log.d("flname","f: "+firstName+"\nL: "+lastName);
                            deliveryAddreess = checkOut.getDeliveryAddress();
                            deliveryAddress2 = checkOut.getDeliveryAddress1();
                            deliverypostCode = checkOut.getDeliveryPostcode();
                            phoneNumber = checkOut.getPhoneno();
                            deliveryCity = checkOut.getDeliveryCity();
                            deliveryProvince = checkOut.getDeliveryProvince();
                            companyName = checkOut.getDeliveryCompanyName();
                            companyEmail = checkOut.getDeliveryCompanyEmail();
                            user = accountNumber;

                        } else {
                            firstname = SaveSharedPreference.getName(PassLoginActivity.this);
                            lastName = SaveSharedPreference.getlastName(PassLoginActivity.this);
                            address = SaveSharedPreference.getAddress(PassLoginActivity.this);
                            postCode = SaveSharedPreference.getPostcode(PassLoginActivity.this);
                            city = SaveSharedPreference.getCity(PassLoginActivity.this);
                            province = SaveSharedPreference.getProvince(PassLoginActivity.this);
                            user = accountNumber;
                        }
                    }

//                tipValue = Double.valueOf(tipEditText);
                    final Profile profile = ProfileHelper.getProfilehelper();

                    // Log.d("tipndel","Tip: "+tipEditText+"\ndelivery: "+delivery_charge+"\ndelivery1: "+deliveryAddreess+"\ndelivery2: "+deliveryAddress2);
                    Log.d("sflname", "SFIRSTNAME: " + checkOut.getShippingFirstName() + "\nSLNAME: " + checkOut.getShippingLastName());
                    OrderSend order = new OrderSend(
                            Double.parseDouble(delivery_charge),
                            Double.parseDouble(tipEditText),
                            checkOut.getProviderAddress().getId(),
                            checkOut.getCheckoutInstruction(),
                            checkOut.getOrderTime(),
                            dateToString,
                            orderType,
                            items,
                            coupon,
                            Integer.valueOf(user),
                            firstName,
                            lastName,
                            address,
                            address1,
                            city,
                            province,
                            postCode,
                            phoneNumber,
                            deliveryAddreess,
                            deliveryAddress2,
                            deliverypostCode,
                            deliveryProvince,
                            deliveryCity,
                            companyName,
                            companyEmail,
                            checkOut.getBillingCompany(),
                            checkOut.getBillingEmail(),
                            checkOut.getShippingFirstName(),
                            checkOut.getShippingLastName()
                    );
                    sendNetworkRequest(order);
                    // sendOrderPlace(order);
                }
                else {
                    Common.showToast(PassLoginActivity.this, "Internet not available...");
                }
            }
        });


        couponDiscountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if(Common.isOnline(PassLoginActivity.this)) {

                    if (discountCodeTxt.getText().toString().equals("")) {
                        Toast.makeText(PassLoginActivity.this, "Please enter coupon code", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    //Loader show

                    //TODO: Add discuount Feature and detuct the amount from the total
                    Log.d("Discount code is:", String.valueOf(discountCodeTxt));
                    final String dicountCodetext = discountCodeTxt.getText().toString();
                    // Response received from the server
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean valid = jsonResponse.getBoolean("validity");

                                DecimalFormat df2 = new DecimalFormat(".##");

                                if (valid) {
                                    //TODO: Add Coupon Code
                                    coupon = dicountCodetext;
                                    Log.e("DISCOUNT", coupon);
                                    String type = jsonResponse.getString("type");
                                    String discount = jsonResponse.getString("discount");
                                    BigDecimal discountValue = BigDecimal.valueOf(Double.valueOf(discount));
                                    //Toast.makeText(PassLoginActivity.this,discount + " Type " + type, Toast.LENGTH_LONG).show();
                                    if (type.contentEquals("amount")) {
                                        // coupon = discount;
                                        Common.showToast(PassLoginActivity.this, "You will get " + discount + " $ CAD discount");
                                        checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal().subtract(BigDecimal.valueOf(Double.valueOf(discount))));
                                        // totalPaymentText.setText(checkOut.getmCheckoutTotalAfterDiscount().toString() + "$ CAD");
                                        updateTextViewTotal(delivery_charge, coupon, tipEditText);
                                        couponDiscountBtn.setEnabled(false);
                                    } else {
                                        //coupon = String.valueOf(checkOut.getmCheckOutTotal().doubleValue() - percentage(checkOut.getmCheckOutTotal().doubleValue(),discountValue.doubleValue()));

                                        Common.showToast(PassLoginActivity.this, "You will get " + discount + " percent discount");
                                        double value = percentage(checkOut.getmCheckOutTotal().doubleValue(), discountValue.doubleValue());
                                        checkOut.setmCheckoutTotalAfterDiscount(new BigDecimal(value));
                                        updateTextViewTotal(delivery_charge, coupon, tipEditText);
                                        //totalPaymentText.setText(checkOut.getmCheckoutTotalAfterDiscount().toString() + "$ CAD");
                                        couponDiscountBtn.setEnabled(false);

                                    }
                                    hidePDialog();

                                } else {

                                    hidePDialog();

                                    String message = jsonResponse.getString("message");
                                    checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal());
                                    AlertDialog.Builder builder = new AlertDialog.Builder(PassLoginActivity.this);
                                    builder.setMessage(message)
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }

                            } catch (JSONException e) {
                                //Common.showToast(PassLoginActivity.this,"e : "+e);
                                hidePDialog();
                                e.printStackTrace();
                            }
                        }
                    };

                    discountRequest request = new discountRequest(dicountCodetext, "" + checkOut.getmCheckOutTotal(), responseListener);
                    RequestQueue queue = Volley.newRequestQueue(PassLoginActivity.this);
                    request.setShouldCache(false);
                    queue.add(request);
                    //todo
                    pDialog.show();
                }
                else {
                    Common.showToast(PassLoginActivity.this, "Internet not available...");
                }

            }
        });



    }

/*    private  void sendOrderPlace(OrderSend orderSend){
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if(response != null){
                        Common.showToast(PassLoginActivity.this,"Response Success.");
                    }else{
                        Common.showToast(PassLoginActivity.this,"Response Fail.");
                    }
                    hidePDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                    hidePDialog();
                }
            }
        };

        PlaceOrderRequest placeOrderRequest = new PlaceOrderRequest(orderSend, responseListener);
        RequestQueue queue = Volley.newRequestQueue(PassLoginActivity.this);
        placeOrderRequest.setShouldCache(false);
        queue.add(placeOrderRequest);

        placeOrderRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


    }*/
    protected void showAlertMessage(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please enter different address for delivery")
                .setPositiveButton(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(PassLoginActivity.this, AddressActivity.class);
                        startActivity(intent);
                    }
                }
                ).create()
                .show();
    }

    private void sendNetworkRequest(OrderSend orderSend){
        //create Okhttp client
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okhttpClientBuilder.addInterceptor(logging);
        //create Retrofit Instance
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://demo.online-menu.ca/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okhttpClientBuilder.build());

        Retrofit retrofit = builder.build();

        // get client & call object for request
        OrderSendClient client = retrofit.create(OrderSendClient.class);

        PostProduct postProduct = new PostProduct();
        Log.d("Ok", orderSend.toString());

        Log.d("DeliveryyDeliveryy","sfname: "+orderSend.getShippingFirstname()+"\nsldcharges: "+orderSend.getDeliveryCharge()+"\nTip: "+orderSend.getTip()
                +"\ngetfrom: "+orderSend.getFrom()+"\nGetitems: "+orderSend.getItems()+"\ngetnote: "+orderSend.getNote()+"\ngetTime: "+orderSend.getTime()
        +"\ngetType: "+orderSend.getType()+"\ngetCoupon: "+orderSend.getCoupon()+"\ngetUseriD: "+orderSend.getUserId()+"\ngetbfname: "+orderSend.getBfname()
        +"\nblname: "+orderSend.getBlname()+"\ndaddress: "+orderSend.getdAddress()+"\nbaddress1: "+orderSend.getdAddress1()
                +"\nbcity: "+orderSend.getBcity()+"\nbstate: "+orderSend.getBstate()+"\nbpostcode: "+orderSend.getBpostcode()+"\nbphone: "+orderSend.getBphone()
                +"\nbcompany: "+orderSend.getBcompany()+"\nbemail: "+orderSend.getBemail()+"\nbsfname: "+orderSend.getShippingFirstname()
                +"\nbslname: "+orderSend.getShippingLastname()+"\nblname: "+orderSend.getdAddress()+"\ndaddress1: "+orderSend.getdAddress1()+"\ndcity: "+orderSend.getdCity()
                +"\ndprovince: "+orderSend.getdProvince()+"\ndpostcode: "+orderSend.getdPostcode()+"\ndcompany: "+orderSend.getDcompany()+"\nDemail: "+orderSend.getDemail());
       /* Log.d("DeliveryDelivery ","Date: "+orderSend.getDate()+"\nDelivery charges: "+orderSend.getShippingFirstname()+orderSend.getShippingLastname()
                +"\naddress: "+orderSend.getBaddress()+"\n"+orderSend.getBaddress1()+"\nDhipping phone no: "+orderSend.getBphone()
                +"\nEmail: "+orderSend.getBemail());*/
        Call<PostProduct> call2 = client.sendOrder2(orderSend.getDate(),orderSend.getDeliveryCharge(),orderSend.getTip(),orderSend.getFrom(),
                orderSend.getItems(),orderSend.getNote(),orderSend.getTime(),orderSend.getType(),
                orderSend.getCoupon(),orderSend.getUserId(),orderSend.getBfname(),orderSend.getBlname(),
                orderSend.getBaddress(),orderSend.getBaddress1(),orderSend.getBcity(),orderSend.getBstate(),orderSend.getBpostcode(),orderSend.getBphone(),
                orderSend.getBcompany(),orderSend.getBemail(),orderSend.getShippingFirstname(),orderSend.getShippingLastname(),orderSend.getdAddress()
                ,orderSend.getdAddress1(),orderSend.getdCity(),orderSend.getdProvince(),orderSend.getdPostcode(),orderSend.getDcompany(),orderSend.getDemail());


        //TODO: Adding first name and last name with the shipping order
        Log.d("Success", "it worked"+call2.isExecuted());
        call2.enqueue(new Callback<PostProduct>() {
            @Override
            public void onResponse(Call<PostProduct> call, retrofit2.Response<PostProduct> response) {
                Log.d("Success", "it worked");
                Log.d("Databack", response.body().toString());
                orderNumber = response.body().getOid().toString();
               // total = response.body().getAmount().toString();
                Intent payment = new Intent(PassLoginActivity.this, CreditPayment.class);
                payment.putExtra("total", total);
                payment.putExtra("orderNumber", orderNumber);
                Log.d("TotalNORDER","\nTotal: "+total+"\nOrder: "+orderNumber);
                PassLoginActivity.this.startActivity(payment);
                hidePDialog();
                Common.showToast(PassLoginActivity.this,"Request place order successful");
                Common.clearCart();
                finish();
            }

            @Override
            public void onFailure(Call<PostProduct> call, Throwable t) {
                hidePDialog();
                Common.showToast(PassLoginActivity.this,"Request place order failled");
                Log.e("Failure", "it didn't worked ");
                Log.e("Failure", t.getMessage());
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    private void updateTextViewTotal(String deliveryValue,String coupon,String tipEditText) {
        BigDecimal totalWithCoupon = null;
        double overalltotal;

        TextView totalPaymentText = (TextView) findViewById(R.id.billTotalPayment);
        CheckOut checkOut = CheckoutHelper.getCheckout();
        Log.d("ttttotalAmount","tip: "+tipEditText);
        tipValue = Double.parseDouble(tipEditText);
        Log.d("ttttotalAmount","tipValue: "+tipEditText);
        Log.d("ddd","delivery: "+deliveryValue+"\ncheckOut.getmCheckOutTotal(): "+checkOut.getmCheckOutTotal());
        if(checkOut.getmCheckOutTotal()==null){
            Log.d("bigD","checkOut.getmCheckOutTotal(): "+checkOut.getmCheckOutTotal());
            checkOut.setmCheckOutTotal(BigDecimal.valueOf(Double.valueOf(0)));
        }
        BigDecimal totalAmount = checkOut.getmCheckOutTotal().add(new BigDecimal(deliveryValue));
        //total = String.valueOf(totalAmount);
         BigDecimal totalWithTip = totalAmount.add(BigDecimal.valueOf(tipValue));
         Log.d("ttttotalAmount","totalWithTip: "+totalWithTip);
        if(!coupon.equals("")) {
            total = String.valueOf(removeDecimalPlaces(checkOut.getmCheckoutTotalAfterDiscount().doubleValue())+tipValue);

            Log.d("ttttotalAmount"," if(coupon!=null): "+total);

        }
        else {
            totalWithCoupon = totalWithTip.subtract(BigDecimal.valueOf(Double.valueOf("0")));
            total = String.valueOf(totalWithCoupon );
            Log.d("ttttotalAmount","else total without coupon: "+total);

        }

        //BigDecimal totalAll = checkOut.getmCheckOutTotal().add(totalWithTip);
        totalPaymentText.setText("Total Payment " + total +"$ CAD");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                onBackPressed();
                break;

        }
    }

    private static double percentage(double price,double discount){
        DecimalFormatSymbols locale = new DecimalFormatSymbols(Locale.ENGLISH);
        DecimalFormat df2 = new DecimalFormat(".###", locale);
        double cal = price - (discount * price) / 100.0;
        double value = Double.parseDouble(df2.format(cal));
        return value;
    }

    private static double removeDecimalPlaces(double total){
        DecimalFormatSymbols locale = new DecimalFormatSymbols(Locale.ENGLISH);
        DecimalFormat df2 = new DecimalFormat(".###", locale);
        double value = Double.parseDouble(df2.format(total));
        return value;
    }

}
