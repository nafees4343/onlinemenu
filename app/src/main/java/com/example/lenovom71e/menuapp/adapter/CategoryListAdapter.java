package com.example.lenovom71e.menuapp.adapter;

/**
 * Created by STUDENT on 2/24/2016.
 */
import com.example.lenovom71e.menuapp.R;

import com.example.lenovom71e.menuapp.model.Category;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CategoryListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Category> categoryItems;
 //   private int[] colors = new int[] { 0x302C6700, 0x30FFCC00 };

    public CategoryListAdapter(Activity activity, List<Category> categoryItems) {
        this.activity = activity;
        this.categoryItems = categoryItems;
    }

    @Override
    public int getCount() {
        return categoryItems.size();
    }

    @Override
    public Object getItem(int location) {
        return categoryItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_categories, parent, false);
        TextView category = (TextView) convertView.findViewById(R.id.category_name);
        TextView id = (TextView) convertView.findViewById(R.id.category_id);

        // getting category data for the row
        Category c = categoryItems.get(position);
        id.setText(String.valueOf(c.getId()));
        category.setText(c.getCategory());
//        int colorPos = position % colors.length;
//       convertView.setBackgroundColor(colors[colorPos]);
        return convertView;

    }

}
