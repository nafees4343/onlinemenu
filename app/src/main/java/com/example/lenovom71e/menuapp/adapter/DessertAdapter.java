package com.example.lenovom71e.menuapp.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Saleable;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.lenovom71e.menuapp.DessertsActivity;
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.CartItem;
import com.example.lenovom71e.menuapp.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by subhan on 8/19/18.
 */

public class DessertAdapter extends RecyclerView.Adapter<DessertAdapter.ViewHolder>{

    List<Product> cartItems;
    DessertsActivity mActivity;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public DessertAdapter(DessertsActivity activity, List<Product> products){
        this.mActivity = activity;
        this.cartItems = products;
        //init();
    }

   /* public void init(){
        Product p1 = new Product(0, 168, "Cookies (EACH)","https://demo.online-menu.ca/assets/img/product/168.jpg",new BigDecimal(0.75),"Homemade!!",0,"",false, "https://demo.online-menu.ca/assets/img/product/168.jpg");
        Product p2 = new Product(0, 170, "Squares","https://demo.online-menu.ca/assets/img/product/170.jpg",new BigDecimal(2.50),"Homemade Squares: Baked Daily\\r\\nDollie, Date, Butter Tart,\\r\\nChocolate Carmel & Skor ",0,"",false, "https://demo.online-menu.ca/assets/img/product/170.jpg");
        cartItems = new ArrayList<>();
        cartItems.add(p1);
        cartItems.add(p2);
    }*/

    @Override
    public DessertAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_dessert, parent, false);
        return new DessertAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DessertAdapter.ViewHolder holder, int position) {
        final Product p = cartItems.get(position);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        holder.thumbnail.setImageUrl(p.getThumbnailUrl(), imageLoader);
        holder.dessertprice.setText(p.getPrice().toString());
        holder.dessertname.setText(p.getPname());
        holder.dessertquantity.setText(String.valueOf(p.getQuantity()));
        holder.itemdesert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setSelected(!p.isSelected());
                holder.isSelected.setVisibility(p.isSelected()? View.VISIBLE : View.GONE);
                mActivity.OnSelect(getSelectedList());
            }
        });
        holder.addToQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = p.getQuantity() + 1;
                if(q <= 10) {
                    p.setQuantity(q);
                    holder.dessertquantity.setText(String.valueOf(p.getQuantity()));
                }
            }
        });
        holder.subtactToQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = p.getQuantity() - 1;
                if(q >= 1) {
                    p.setQuantity(q);
                    holder.dessertquantity.setText(String.valueOf(p.getQuantity()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView dessertname;
        TextView dessertprice;
        TextView dessertquantity;
        ImageView isSelected;
        NetworkImageView thumbnail;
        RelativeLayout itemdesert;
        ImageButton addToQuantity, subtactToQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            dessertname =(TextView) itemView.findViewById(R.id.tvCartItemName);
            dessertprice = (TextView) itemView.findViewById(R.id.tvCartItemPrice);
            dessertquantity =(TextView) itemView.findViewById(R.id.tvCartItemQuantity);
            itemdesert = (RelativeLayout) itemView.findViewById(R.id.item_desert);
            isSelected = (ImageView) itemView.findViewById(R.id.tick);
            thumbnail = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            addToQuantity = (ImageButton)itemView.findViewById(R.id.btn_plus);
            subtactToQuantity = (ImageButton)itemView.findViewById(R.id.btn_minus);
        }
    }

    protected ArrayList<Product> getSelectedList(){
        ArrayList<Product> selected = new ArrayList<>();
        for( Product p : cartItems) {
            if (p.isSelected()) {
                selected.add(p);
            }
        }
        return selected;
    }
}
