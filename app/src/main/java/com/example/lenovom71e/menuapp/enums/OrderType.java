package com.example.lenovom71e.menuapp.enums;

public enum OrderType {
    PICKUP("Pickup"), EATIN("Eatin"), DELIVERY("Delivery");

    private String value;

    private OrderType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }}
