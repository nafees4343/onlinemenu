package com.example.lenovom71e.menuapp;

/**
 * Created by STUDENT on 2/17/2016.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.coboltforge.slidemenu.SlideMenu;
import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.adapter.CategoryListAdapter;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Category;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity implements SlideMenuInterface.OnSlideMenuItemClickListener, View.OnClickListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
    // Categories json url
    private ProgressDialog pDialog;
    private List<Category> categoryList = new ArrayList<Category>();
    private CategoryListAdapter adapter;
//    private ProgressDialog pDialog;
    private SlideMenu slidemenu;
    private final static int MYITEMID = 42;
    private Toolbar toolbar;
    Cart cart;
    boolean doubleBackToExitPressedOnce = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);

        cart = CartHelper.getCart();

        init();
        setSupportActionBar(toolbar);
        setUpSlideMenu();

        final String url = this.getResources().getString(R.string.api_url)+"categories";

        ListView listView = (ListView) findViewById(R.id.list);
        adapter = new CategoryListAdapter(this, categoryList);
        listView.setAdapter(adapter);
        //listView.setAdapter(new CategoryListAdapter(this, categoryList));


        // Creating volley request obj
        JsonArrayRequest catReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        // get listview
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Category category = new Category();
                                category.setId(obj.getInt("idcategory"));
                                category.setCategory(obj.getString("name"));
                                // adding categories to categories array
                                categoryList.add(category);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();

                        hidePDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();

                    }
                }
        );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                if(Common.isOnline(CategoriesActivity.this)) {
                    Intent i = new Intent(getApplicationContext(), ProductActivity.class);
                    // send category id to productlist activity to get list of products under that category
                    String category_id = ((TextView) view.findViewById(R.id.category_id)).getText().toString();
                    String category_name = ((TextView) view.findViewById(R.id.category_name)).getText().toString();
                    i.putExtra("category_id", category_id);
                    i.putExtra("category_name", category_name);
                    startActivity(i);
                }else{
                    Common.showToast(CategoriesActivity.this,"Internet is not available...");
                }
            }
        });

        // Adding request to request queue
        catReq.setShouldCache(false);

        AppController.getInstance().addToRequestQueue(catReq);

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void init(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);
        Button profile = (Button) toolbar.findViewById(R.id.btn_toolbrprofile);

        title.setText("CATEGORIES");
        profile.setVisibility(View.VISIBLE);
        profile.setOnClickListener(this);
        navigation.setOnClickListener(this);
    }

    public void setUpSlideMenu(){
        slidemenu = (SlideMenu) findViewById(R.id.slideMenu);
        if(SaveSharedPreference.getCheckLogInState(CategoriesActivity.this).equals("LOGIN")) {
            slidemenu.init(this, R.menu.slide, this, 333);
        }
        else {
            slidemenu.init(this, R.menu.slideforwithoutlogin, this, 333);
        }
        SlideMenu.SlideMenuItem item = new SlideMenu.SlideMenuItem();
        slidemenu.addMenuItem(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onSlideMenuItemClick(int itemId) {
        switch (itemId) {
            case R.id.item_one:
                Intent p = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(p);
                finish();

                break;
            case R.id.item_three:
                if(cart.getProductHeaderId().size() <= 0 ){
                     Common.showToast(CategoriesActivity.this,"Your Cart is empty");
                    return;
                }
                else {
                    Intent k = new Intent(getApplicationContext(), ShoppingCartActivity.class);
                    k.putExtra("PREVIOUS", "Category");
                    startActivity(k);
                    finish();
                }
                break;
            case R.id.item_four:
                Common.clearCart();
                finishAffinity();
                break;
            case R.id.item_fifth:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://demo.online-menu.ca/customer"));
                startActivity(intent);
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_toolbrprofile:
                Profile profile = ProfileHelper.getProfilehelper();
                Intent intent = new Intent(CategoriesActivity.this, UserProfile.class);
                profile.setFromActivity("categories");
                startActivity(intent);
                finish();
                break;

            case R.id.btn_navigation:
                slidemenu.show(CartHelper.getCart().getTotalPriceWithExtra().toString());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}