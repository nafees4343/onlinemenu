package com.example.lenovom71e.menuapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.lenovom71e.menuapp.adapter.LocationAdapter;
import com.example.lenovom71e.menuapp.model.Address;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_SHORT;

public class LocationActivity extends AppCompatActivity implements View.OnClickListener {

    RequestQueue requestQueue;
    String JsonURL = "https://demo.online-menu.ca/api/location" ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        init();


        final ArrayList<Address> addresses = (ArrayList<Address>) getIntent().getSerializableExtra("extraLocations");

        ListView locationListView = (ListView) findViewById(R.id.locationList);
        LocationAdapter adapter = new LocationAdapter(this,addresses);
        locationListView.setAdapter(adapter);

        locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent locationListIntent = new Intent();
                locationListIntent.putExtra("companyName", addresses.get(position).getmName());
                locationListIntent.putExtra("companyAddress", addresses.get(position).getmAddress());
                locationListIntent.putExtra("companyCity", addresses.get(position).getmCity());
                locationListIntent.putExtra("companyPhone", addresses.get(position).getmPhoneNumber());
                CheckOut checkout = CheckoutHelper.getCheckout();
                checkout.setProviderAddress(addresses.get(position));

                setResult(Activity.RESULT_OK, locationListIntent);
                finish();
            }
        });

    }

    public void init(){
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("Select Restaurant");
        bckbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                finish();
                break;
        }
    }


}
