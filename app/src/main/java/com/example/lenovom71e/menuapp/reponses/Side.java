
package com.example.lenovom71e.menuapp.reponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Side {

    @SerializedName("idproduct")
    @Expose
    private String idproduct;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("e_price")
    @Expose
    private String ePrice;
    @SerializedName("sort")
    @Expose
    private String sort;
    @SerializedName("category_idcategory")
    @Expose
    private String categoryIdcategory;
    @SerializedName("is_online")
    @Expose
    private String isOnline;
    @SerializedName("is_pos")
    @Expose
    private String isPos;
    @SerializedName("printer_idprinter")
    @Expose
    private String printerIdprinter;
    @SerializedName("last_updated")
    @Expose
    private String lastUpdated;
    @SerializedName("idvendor")
    @Expose
    private Object idvendor;
    @SerializedName("is_pizza")
    @Expose
    private String isPizza;
    @SerializedName("is_taxable")
    @Expose
    private String isTaxable;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("is_hidden")
    @Expose
    private String isHidden;
    @SerializedName("idproduct_images")
    @Expose
    private String idproductImages;
    @SerializedName("product_idproduct")
    @Expose
    private String productIdproduct;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("category_name")
    @Expose
    private String categoryName;

    public String getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(String idproduct) {
        this.idproduct = idproduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getEPrice() {
        return ePrice;
    }

    public void setEPrice(String ePrice) {
        this.ePrice = ePrice;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCategoryIdcategory() {
        return categoryIdcategory;
    }

    public void setCategoryIdcategory(String categoryIdcategory) {
        this.categoryIdcategory = categoryIdcategory;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getIsPos() {
        return isPos;
    }

    public void setIsPos(String isPos) {
        this.isPos = isPos;
    }

    public String getPrinterIdprinter() {
        return printerIdprinter;
    }

    public void setPrinterIdprinter(String printerIdprinter) {
        this.printerIdprinter = printerIdprinter;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Object getIdvendor() {
        return idvendor;
    }

    public void setIdvendor(Object idvendor) {
        this.idvendor = idvendor;
    }

    public String getIsPizza() {
        return isPizza;
    }

    public void setIsPizza(String isPizza) {
        this.isPizza = isPizza;
    }

    public String getIsTaxable() {
        return isTaxable;
    }

    public void setIsTaxable(String isTaxable) {
        this.isTaxable = isTaxable;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public String getIdproductImages() {
        return idproductImages;
    }

    public void setIdproductImages(String idproductImages) {
        this.idproductImages = idproductImages;
    }

    public String getProductIdproduct() {
        return productIdproduct;
    }

    public void setProductIdproduct(String productIdproduct) {
        this.productIdproduct = productIdproduct;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
