package com.example.lenovom71e.menuapp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.coboltforge.slidemenu.SlideMenu;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.Common;
import com.example.lenovom71e.menuapp.util.RegisterDeliveryActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    boolean isCheckout = false;
    private InputMethodManager imm;
    String value;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final TextView tvRegisterLink = (TextView) findViewById(R.id.tvRegisterLink);
        final TextView tvForgotLink = (TextView) findViewById(R.id.bForgotPass);
        final Button bLogin = (Button) findViewById(R.id.bSignIn);

        final CheckOut checkOut = CheckoutHelper.getCheckout();
        final TextView signInAsGuest = (TextView) findViewById(R.id.userGuestLogin);
        final Profile profile = ProfileHelper.getProfilehelper();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        init();

        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext()) == true & isCheckout){
            Intent intent = new Intent(LoginActivity.this, PassLoginActivity.class);
            intent.putExtra("total",value);
            startActivity(intent);
            finish();
        }

        signInAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: login into guestLogin Activity
                if(Common.isOnline(getApplication())){
                if(isCheckout) {
                    Intent intent = new Intent(LoginActivity.this, GuestLoginActivity.class);
                    intent.putExtra("isCheckout",true);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(LoginActivity.this, GuestLoginActivity.class);
                    intent.putExtra("isCheckout",false);
                    startActivity(intent);
                    finish();
                }
            }
            else {
                    Common.showToast(LoginActivity.this,"Internet is not available...");

                }
            }
        });

        tvRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (checkOut.getOrderTypeId()){
                    case 1:
                        //Delivery Case
                        if(Common.isOnline(LoginActivity.this)) {
                            Intent registerDeliveryIntent = new Intent(LoginActivity.this, RegisterDeliveryActivity.class);
                            LoginActivity.this.startActivity(registerDeliveryIntent);
                        }
                        else {
                            Common.showToast(LoginActivity.this,"Internet is not available...");
                        }
                        break;
                    case 2:
                    case 3:
                        if(Common.isOnline(LoginActivity.this)){
                            Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                            LoginActivity.this.startActivity(registerIntent);
                        }
                        else {
                            Common.showToast(LoginActivity.this,"Internet is not available...");
                        }

                        break;
                    default:
                        if(Common.isOnline(LoginActivity.this)) {
                            Intent register = new Intent(LoginActivity.this, RegisterActivity.class);
                            LoginActivity.this.startActivity(register);
                        }
                        else {
                            Common.showToast(LoginActivity.this,"Internet is not available...");
                        }
                        break;
                }


            }
        });


        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isOnline(LoginActivity.this)) {

                    final String username = etUsername.getText().toString();
                    final String password = etPassword.getText().toString();
                    if (username.isEmpty()) {
                        etUsername.setError("Please enter the name");
                        return;
                    }
                    if (password.isEmpty()) {
                        etPassword.setError("Please enter the password");
                        return;
                    }

                    if (etUsername.getText().toString().isEmpty() || !isValidEmail(etUsername.getText().toString())) {
                        etUsername.setError("Invalid Email");
                        requestFocus(etUsername);
                        return;
                    }


                    Log.d("Loged in USer", SaveSharedPreference.getUserName(getApplicationContext()).toString());

                    // Response received from the server
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("login_response: ", response);
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {

                                    SaveSharedPreference.SetCheckLogInState(LoginActivity.this, "LOGIN");
                                    SaveSharedPreference.setPassword(getApplicationContext(), password);
                                    SaveSharedPreference.setUserName(getApplicationContext(), username);

                                    String userId = jsonResponse.getString("user_id");
                                    String firstName = jsonResponse.getString("first_name");
                                    String lastName = jsonResponse.getString("last_name");
                                    String loginEmail = jsonResponse.getString("email");
                                    String phone = jsonResponse.getString("phone");
                                    String postCode = jsonResponse.getString("postcode");
                                    String billingCompany = jsonResponse.getString("company");
                                    String billingEmail = jsonResponse.getString("email");
                                    String city = jsonResponse.getString("city");
                                    String province = jsonResponse.getString("province");
                                    String address = jsonResponse.getString("address");

                                    SaveSharedPreference.saveCredentials(getApplication(), firstName, lastName, userId, phone, loginEmail, postCode, billingCompany, billingEmail, address, city, province);

                                    //String name = jsonResponse.getString("name");
                                    //int age = jsonResponse.getInt("age");
                                    profile.setFirstName(firstName);
                                    profile.setLastName(lastName);
                                    profile.setLoginEmail(loginEmail);
                                    profile.setPhone(phone);
                                    profile.setBillingCompany(billingCompany);
                                    profile.setBillingEmail(billingEmail);
                                    profile.setAddress(address);
                                    profile.setCiry(city);
                                    profile.setProvince(province);
                                    profile.setUserId(userId);
                                    profile.setPostCode(postCode);
                                    Boolean isUserProfile = getIntent().getBooleanExtra("isUserProfile", false);
                                    if (!isCheckout) {
                                        finish();
                                        Common.hidePDialog();
                                        Intent intent = new Intent(LoginActivity.this, UserProfile.class);
                                        startActivity(intent);
                                    } else {
                                        Common.hidePDialog();
                                        Intent intent = new Intent(LoginActivity.this, PassLoginActivity.class);
                                        checkOut.setBillingCompany(billingCompany);
                                        checkOut.setBillingEmail(billingEmail);
                                        checkOut.setUserId(userId);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    Common.hidePDialog();
                                    Common.showToast(LoginActivity.this, "Login Failed");
                                }

                            } catch (JSONException e) {
                                Log.d("e", "" + e);
                                Common.hidePDialog();
                                e.printStackTrace();
                            }
                        }
                    };


                    if (Common.isOnline(getApplication())) {

                        LoginRequest loginRequest = new LoginRequest(username, password, responseListener);
                        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                        loginRequest.setShouldCache(false);
                        queue.add(loginRequest);
                        Common.showLoader(LoginActivity.this);


                        loginRequest.setRetryPolicy(new RetryPolicy() {
                            @Override
                            public int getCurrentTimeout() {
                                return 5000;
                            }

                            @Override
                            public int getCurrentRetryCount() {
                                return 5000;
                            }

                            @Override
                            public void retry(VolleyError error) throws VolleyError {

                            }
                        });

                    }
                }
                else {
                    Common.showToast(LoginActivity.this,"Internet is not available...");
                }
            }
        });

        tvForgotLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isOnline(LoginActivity.this)) {
                    String urlString = "https://demo.online-menu.ca/user/forgot_password";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.android.chrome");
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        // Chrome browser presumably not installed so allow user to choose instead
                        intent.setPackage(null);
                        startActivity(intent);
                    }
                }
                else {
                    Common.showToast(LoginActivity.this,"Internet is not available...");
                }
            }
        });
    }

    public void init(){
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            value = intent.getExtras().getString("total");
            isCheckout = intent.getExtras().getBoolean("isCheckout");
            Log.d("checkoutvalue","chkvalue: "+value);
        }




        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);

        title.setText("SIGN IN");
        bckbtn.setOnClickListener(this);
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent i = new Intent(LoginActivity.this,CategoriesActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                if(Common.isOnline(LoginActivity.this)) {
                    if (isCheckout) {
                        finish();
                    } else {
                        finish();
                        Intent inat = new Intent(LoginActivity.this, CategoriesActivity.class);
                        startActivity(inat);
                    }
                }
                else {
                    Common.showToast(LoginActivity.this,"Internet is not available...");
                }
                break;

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

