package com.example.lenovom71e.menuapp.util;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2/5/2018.
 */

public class OrderSend {


    @Expose
    private String coupon;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("delivery_charge")
    @Expose
    private Double deliveryCharge;
    @SerializedName("tip")
    @Expose
    private Double tip;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("items")
    @Expose
    private ArrayList<Item> items = null;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("type")
    @Expose
    private String type;
    @Expose
    @SerializedName("userId")
    private Integer userId = 0;
    @Expose
    @SerializedName("billing_first_name")
    private String bfname;
    @Expose
    @SerializedName("billing_last_name")
    private String blname;
    @Expose
    @SerializedName("billing_address_1")
    private String baddress;
    @Expose
    @SerializedName("billing_address_2")
    private String baddress1;
    @Expose
    @SerializedName("billing_city")
    private String bcity;
    @Expose
    @SerializedName("billing_state")
    private String bstate;
    @Expose
    @SerializedName("billing_postcode")
    private String bpostcode;
    @Expose
    @SerializedName("billing_phone")
    private String bphone;
    @Expose
    @SerializedName("shipping_address_1")
    private String dAddress;
    @Expose
    @SerializedName("shipping_address_2")
    private String dAddress1;
    @Expose
    @SerializedName("shipping_postcode")
    private String dPostcode;
    @Expose
    @SerializedName("shipping_city")
    private String dCity;
    @Expose
    @SerializedName("shipping_province")
    private String dProvince;
    @Expose
    @SerializedName("shipping_company")
    private String dcompany;
    @Expose
    @SerializedName("shipping_email")
    private String demail;
    @Expose
    @SerializedName("company_name")
    private String bcompany;
    @Expose
    @SerializedName("company_email")
    private String bemail;

    @Expose
    @SerializedName("shipping_firstname")
    private String shippingFirstname;

    @Expose
    @SerializedName("shipping_lastname")
    private String shippingLastname;



    public OrderSend(Double deliveryCharge, Double tip, Integer from, String note, String time, String date,
                     String type, ArrayList<Item> items,
                     String coupon, Integer userId, String bfname, String blname, String baddress, String baddress1, String bcity, String bstate, String bpostcode, String bphone,
                     String dAddress, String dAddress1, String dPostcode, String dProvince, String dCity, String company, String email, String bcompany,
                     String bemail,String sfirstname,String slastname){
        this.deliveryCharge = deliveryCharge;
        this.tip = tip;
        this.from = from;
        this.note = note;
        this.time = time;
        this.date = date;
        this.items = items;
        this.type = type;
        this.coupon = coupon;
        this.userId = userId;
        this.bfname = bfname;
        this.blname = blname;
        this.baddress = baddress;
        this.baddress1 = baddress1;
        this.bcity = bcity;
        this.bstate = bstate;
        this.bpostcode = bpostcode;
        this.bphone = bphone;
        this.dAddress = dAddress;
        this.dAddress1 = dAddress1;
        this.dPostcode = dPostcode;
        this.dProvince = dProvince;
        this.dCity = dCity;
        this.dcompany = company;
        this.demail = email;
        this.bcompany = bcompany;
        this.bemail = bemail;
        this.shippingFirstname = sfirstname;
        this.shippingLastname = slastname;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }

    public String getShippingFirstname() {
        return shippingFirstname;
    }

    public void setShippingFirstname(String shippingFirstname) {
        this.shippingFirstname = shippingFirstname;
    }

    public String getShippingLastname() {
        return shippingLastname;
    }

    public void setShippingLastname(String shippingLastname) {
        this.shippingLastname = shippingLastname;
    }

    public String getBcompany() {
        return bcompany;
    }

    public void setBcompany(String bcompany) {
        this.bcompany = bcompany;
    }

    public String getBemail() {
        return bemail;
    }

    public void setBemail(String bemail) {
        this.bemail = bemail;
    }

    public String getDcompany() {
        return dcompany;
    }

    public void setDcompany(String dcompany) {
        this.dcompany = dcompany;
    }

    public String getDemail() {
        return demail;
    }

    public void setDemail(String demail) {
        this.demail = demail;
    }

    public String getdAddress() {
        return dAddress;
    }

    public void setdAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public String getdAddress1() {
        return dAddress1;
    }

    public void setdAddress1(String dAddress1) {
        this.dAddress1 = dAddress1;
    }

    public String getdPostcode() {
        return dPostcode;
    }

    public void setdPostcode(String dPostcode) {
        this.dPostcode = dPostcode;
    }

    public String getdCity() {
        return dCity;
    }

    public void setdCity(String dCity) {
        this.dCity = dCity;
    }

    public String getdProvince() {
        return dProvince;
    }

    public void setdProvince(String dProvince) {
        this.dProvince = dProvince;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBfname() {
        return bfname;
    }

    public void setBfname(String bfname) {
        this.bfname = bfname;
    }

    public String getBlname() {
        return blname;
    }

    public void setBlname(String blname) {
        this.blname = blname;
    }

    public String getBaddress() {
        return baddress;
    }

    public void setBaddress(String baddress) {
        this.baddress = baddress;
    }

    public String getBaddress1() {
        return baddress1;
    }

    public void setBaddress1(String baddress1) {
        this.baddress1 = baddress1;
    }

    public String getBcity() {
        return bcity;
    }

    public void setBcity(String bcity) {
        this.bcity = bcity;
    }

    public String getBstate() {
        return bstate;
    }

    public void setBstate(String bstate) {
        this.bstate = bstate;
    }

    public String getBpostcode() {
        return bpostcode;
    }

    public void setBpostcode(String bpostcode) {
        this.bpostcode = bpostcode;
    }

    public String getBphone() {
        return bphone;
    }

    public void setBphone(String bphone) {
        this.bphone = bphone;
    }

    public static class Item {
        @SerializedName("extras")
        @Expose
        private Object[] extras = null;
        @SerializedName("pid")
        @Expose
        private String pid;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("tip")
        @Expose
        private double tip;

        public Item(String pid, String quantity, double tip, Object[] extras) {

            Log.d("chkitems","pid: "+pid+"\nqty: "+quantity);
            this.pid = pid;
            this.qty = quantity;
            this.extras = extras;
            this.tip = tip;
        }

        public double getTip() {
            return tip;
        }

        public void setTip(double tip) {
            this.tip = tip;
        }

        public Object[] getExtras() {
            return extras;
        }

        public void setExtras(Object[] extras) {
            this.extras = extras;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

    }
}
