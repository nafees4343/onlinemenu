package com.example.lenovom71e.menuapp;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017-08-10.
 */

public class RegisterReqeustDelivery extends StringRequest {
    /**
     * Construct a new {@code StringReader} with {@code str} as source. The size
     * of the reader is set to the {@code length()} of the string and the Object
     * to synchronize access through is set to {@code str}.
     *
     * @param str the source string for this reader.
     */
    //TODO:need to change the request url
    private static final String REGISTER_DELIVERY_REQUEST_URL = "https://demo.online-menu.ca/api/register";
    private Map<String, String> params;


    public RegisterReqeustDelivery(String name, String lname, String phoneNumber, String emailAddress, String password,
                                   String companyName, String address, String address2, String city, String province,
                                   String postcode, String country, Response.Listener<String> listener) {

        super(Request.Method.POST, REGISTER_DELIVERY_REQUEST_URL, listener, null);

        Log.d("Reg_req_params","name: "+name+"\nlast: "+lname+"\nnumber: "+phoneNumber+"\npass: "+password);


        params = new HashMap<>();
        params.put("first_name", name);
        params.put("last_name", lname);
        params.put("phone", phoneNumber);
        params.put("email", emailAddress);
        params.put("password", password);
        params.put("company", companyName);
        params.put("address1", address);
        params.put("address2", address2);
        params.put("city", city);
        params.put("province", province);
        params.put("postcode", postcode);
        params.put("country", country);
    }

    @Override
    public Map<String ,String> getParams(){return params;}

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }

}
