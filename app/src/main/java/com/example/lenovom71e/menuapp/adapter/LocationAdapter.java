package com.example.lenovom71e.menuapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.model.Address;

import java.util.ArrayList;

/**
 * Created by admin on 2017-07-21.
 */

public class LocationAdapter extends BaseAdapter implements SpinnerAdapter{

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Address> locations;

    public LocationAdapter(Activity activity, ArrayList<Address> locations){
        this.activity = activity;
        this.locations = locations;

    }



    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        if (convertView == null)
            convertView = inflater.inflate(R.layout.location_item, parent, false);

        TextView nameLabel = (TextView) convertView.findViewById(R.id.companyName);
        TextView addressLabel = (TextView) convertView.findViewById(R.id.locationAddress);
        TextView cityLabel = (TextView) convertView.findViewById(R.id.locationCity);

        Address currentAddress =  locations.get(position);
        nameLabel.setText(currentAddress.getmName());
        addressLabel.setText(currentAddress.getmAddress());
        cityLabel.setText(currentAddress.getmCity());
        return convertView;

    }






}
