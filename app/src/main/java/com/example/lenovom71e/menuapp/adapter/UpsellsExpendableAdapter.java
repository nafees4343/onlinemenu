package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.lenovom71e.menuapp.DessertsActivity;
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpsellsExpendableAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private DessertsActivity mActivity;
    private List<String> _listDataHeader;
    private HashMap<String, List<Product>> _listDataChild;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private List<Product> products;
    private Product p;

    public UpsellsExpendableAdapter(DessertsActivity mActivity, List<String> listDataHeader,
                                 HashMap<String, List<Product>> listChildData) {
        this.mActivity = mActivity;
        this._context = mActivity.getApplicationContext();
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        p = (Product) getChild(groupPosition, childPosition);
        //products =

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_dessert, null);
        }

        TextView dessertname =(TextView) convertView.findViewById(R.id.tvCartItemName);
        TextView dessertprice = (TextView) convertView.findViewById(R.id.tvCartItemPrice);
        final TextView dessertquantity =(TextView) convertView.findViewById(R.id.tvCartItemQuantity);
        final RelativeLayout itemdesert = (RelativeLayout) convertView.findViewById(R.id.item_desert);
        final ImageView isSelected = (ImageView) convertView.findViewById(R.id.tick);
        NetworkImageView thumbnail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
        ImageButton addToQuantity = (ImageButton)convertView.findViewById(R.id.btn_plus);
        ImageButton subtactToQuantity = (ImageButton)convertView.findViewById(R.id.btn_minus);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        thumbnail.setImageUrl(p.getThumbnailUrl(), imageLoader);
        dessertprice.setText(p.getPrice().toString());
        dessertname.setText(p.getPname());
        dessertquantity.setText(String.valueOf(p.getQuantity()));
        itemdesert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                p.setSelected(!p.isSelected());
                isSelected.setVisibility(p.isSelected()? View.VISIBLE : View.GONE);
                mActivity.OnSelect(getSelectedList());
            }
        });
        addToQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = p.getQuantity() + 1;
                if(q <= 10) {
                    p.setQuantity(q);
                    dessertquantity.setText(String.valueOf(p.getQuantity()));
                }
            }
        });
        subtactToQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = p.getQuantity() - 1;
                if(q >= 1) {
                    p.setQuantity(q);
                    dessertquantity.setText(String.valueOf(p.getQuantity()));
                }
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        /*if(isExpanded){
            products = _listDataChild.get(_listDataHeader.get(groupPosition));
        }*/

        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);
        products = _listDataChild.get(_listDataHeader.get(groupPosition));

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    protected List<Product> getSelectedList(){
        List<Product> selected = new ArrayList<>();
        for( Product p : products) {
            if (p.isSelected()) {
                selected.add(p);
            }
        }
        return selected;
    }
}
