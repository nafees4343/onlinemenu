package com.example.lenovom71e.menuapp.model;

/**
 * Created by STUDENT on 2/24/2016.
 */

import java.math.BigDecimal;
import java.util.ArrayList;

public class Extra {


    private int idE;
    private String eName;
    private ArrayList<Choice> Choices;




    public int getIde() {
        return idE;
    }

    public int setIde(int idE) {
        this.idE = idE;
        return idE;
    }
    public String getEname() {
        return eName;
    }

    public String setEname(String eName) {
        this.eName = eName;
        return eName;
    }
    public ArrayList<Choice> getChoices() {
        return Choices;
    }

    public void setChoices(ArrayList<Choice> Choices) {
        this.Choices = Choices;
    }

}
