package com.example.lenovom71e.menuapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/26/2018.
 */

public class Paymentconfirm {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("data")
    @Expose
    private DataSSL data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DataSSL getData() {
        return data;
    }

    public void setData(DataSSL data) {
        this.data = data;
    }
}
