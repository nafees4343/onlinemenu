package com.example.lenovom71e.menuapp;

/**
 * Created by LENOVO on 12/7/2016.
 */

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovom71e.menuapp.model.Profile;

import java.util.HashMap;
import java.util.Map;

public class RegisterRequest extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "https://demo.online-menu.ca/api/register";
    //TODO: we have to change REGISTER_REQUEST_URL
    private Map<String, String> params;

    public RegisterRequest(Profile profile, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("first_name", profile.getFirstName());
        params.put("last_name", profile.getLastName());
        //params.put("age", age + "");
        params.put("email", profile.getLoginEmail());
        params.put("password", profile.getLoginPassword());
        params.put("phone", profile.getPhone());
        params.put("address1", profile.getAddress());
        params.put("address2", profile.getFromActivity());
        params.put("city", profile.getCiry());
        params.put("province", profile.getProvince());
        params.put("postcode", profile.getPostCode());
        params.put("country", profile.getCountry());
        params.put("company", profile.getBillingCompany());


    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }


    @Override
    public Map<String, String> getParams() {
        return params;
    }
}