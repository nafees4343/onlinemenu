package com.example.lenovom71e.menuapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.lenovom71e.menuapp.adapter.DessertAdapter;
import com.example.lenovom71e.menuapp.adapter.UpsellsExpendableAdapter;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.callbacks.OnDessertComplete;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Product;
import com.example.lenovom71e.menuapp.reponses.Beverage;
import com.example.lenovom71e.menuapp.reponses.Breakfast;
import com.example.lenovom71e.menuapp.reponses.Side;
import com.example.lenovom71e.menuapp.reponses.Upsells;
import com.example.lenovom71e.menuapp.reponses.UpsellsItem;
import com.example.lenovom71e.menuapp.reponses.Wrap;
import com.example.lenovom71e.menuapp.util.Common;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by subhan on 8/19/18.
 */

public class DessertsActivity extends AppCompatActivity implements View.OnClickListener, OnDessertComplete{

    Toolbar toolbar;
    RecyclerView recyclerView;
    Button addToCart;
    CheckOut checkout;
    List<Product> dessertListSelected;
    List<Product> upsellsList;
    String url = "https://demo.online-menu.ca/api/upsells";
    private ProgressDialog pDialog;
    private Gson gson;
    private UpsellsItem upsellsItem;
    UpsellsExpendableAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<Product>> listDataChild;
    private static final String TAG = DessertsActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desserts);

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
        gson = new Gson();
        upsellsList = new ArrayList<>();
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Product>>();


        if(Common.isOnline(DessertsActivity.this)){
            getUpsells();
        }else{
            Common.showToast(DessertsActivity.this,"Internet not available...");
        }


        init();
    }

    private void getUpsells(){
        // Creating volley request obj
        JsonObjectRequest movieReq = new JsonObjectRequest(url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            upsellsItem = gson.fromJson(response.toString(),UpsellsItem.class);
                            JSONObject jsonObject = new JSONObject(response.toString());
                            //upsellsJsonArray(jsonObject.getJSONObject("upsells"));
                           // getListFromArray(jsonObject.getJSONObject("upsells"),getIntent().getStringExtra("upsells"));
                           /* for(String key : listDataHeader){
                                listDataChild.put(key, getListFromArray(jsonObject.getJSONObject("upsells"),key) );
                            }*/
                           String key =  getIntent().getStringExtra("upsells");
                            listDataChild.put(key, getListFromArray(jsonObject.getJSONObject("upsells"),key) );
                            listDataHeader.add(key);
                            // prepareListData();
                            listAdapter = new UpsellsExpendableAdapter(DessertsActivity.this, listDataHeader, listDataChild);
                            expListView.setAdapter(listAdapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        hidePDialog();
                        // Parsing json

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });
        movieReq.setShouldCache(true);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);
    }

    private List<Product> getBeverage(List<Beverage> beverages){
        List<Product> products = new ArrayList<>();
        for(int i=0;i<beverages.size();i++){
            products.add(new Product(0,Integer.parseInt(beverages.get(i).getIdproduct()),beverages.get(i).getName()
            ,beverages.get(i).getIdproductImages(),new BigDecimal(beverages.get(i).getPrice()),beverages.get(i).getDescription(),
                    0,"",false,beverages.get(i).getIdproductImages()));
        }
        return products;
    }
    private List<Product> getSide(List<Side> sides){
        List<Product> products = new ArrayList<>();
        for(int i=0;i<sides.size();i++){
            products.add(new Product(0,Integer.parseInt(sides.get(i).getIdproduct()),sides.get(i).getName()
                    ,sides.get(i).getIdproductImages(),new BigDecimal(sides.get(i).getPrice()),sides.get(i).getDescription(),
                    0,"",false,sides.get(i).getIdproductImages()));
        }
        return products;
    }
    private List<Product> getWrap(List<Wrap> wraps){
        List<Product> products = new ArrayList<>();
        for(int i=0;i<wraps.size();i++){
            products.add(new Product(0,Integer.parseInt(wraps.get(i).getIdproduct()),wraps.get(i).getName()
                    ,wraps.get(i).getIdproductImages(),new BigDecimal(wraps.get(i).getPrice()),wraps.get(i).getDescription(),
                    0,"",false,wraps.get(i).getIdproductImages()));
        }
        return products;
    }
    private List<Product> getBreakfast(List<Breakfast> breakfasts){
        List<Product> products = new ArrayList<>();
        for(int i=0;i<breakfasts.size();i++){
            String image = breakfasts.get(i).getIdproductImages()!= null ? breakfasts.get(i).getIdproductImages() : "";
            products.add(new Product(0,Integer.parseInt(breakfasts.get(i).getIdproduct()),breakfasts.get(i).getName()
                    ,image,new BigDecimal(breakfasts.get(i).getPrice()),breakfasts.get(i).getDescription(),
                    0,"",false,image));
        }
        return products;
    }

    public void init(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button back= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation = (Button) toolbar.findViewById(R.id.btn_navigation);

       // recyclerView = (RecyclerView)findViewById(R.id.dessert_list);
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        addToCart = (Button)findViewById(R.id.addToCart);
        expListView.setGroupIndicator(null);
        expListView.setDividerHeight(0);
        addToCart.setOnClickListener(this);
        back.setOnClickListener(this);
        title.setText("");
        navigation.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        checkout = CheckoutHelper.getCheckout();
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition).getPname(), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
        //DessertAdapter dessertAdapter = new DessertAdapter(DessertsActivity.this,upsellsList);
        /*recyclerView.setAdapter(dessertAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setOnClickListener(this);*/
    }

    private void prepareListData() {


        // Adding child data
        /*listDataHeader.add("Breakfast");
        listDataHeader.add("Wrap");
        listDataHeader.add("Side");
        listDataHeader.add("Beverage");*/

        // Adding child data
       /* List<Product> products_breakfast = getBreakfast(upsellsItem.getUpsells().getBreakfast());
        List<Product> products_wrap = getWrap(upsellsItem.getUpsells().getWraps());
        List<Product> products_beverage = getBeverage(upsellsItem.getUpsells().getBeverages());
        List<Product> products_side = getSide(upsellsItem.getUpsells().getSides());*/

       for(String key : listDataHeader){
          // listDataChild.put(key, getListFromArray() );
       }


//        listDataChild.put(listDataHeader.get(0), products_breakfast );
//        listDataChild.put(listDataHeader.get(1), products_wrap);
//        listDataChild.put(listDataHeader.get(2), products_side);
//        listDataChild.put(listDataHeader.get(3), products_beverage);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                if(Common.isOnline(DessertsActivity.this)) {
                    super.onBackPressed();
                }
                else {
                    Common.showToast(DessertsActivity.this,"Internet not available");
                }
                break;

            case R.id.addToCart:
                if(Common.isOnline(DessertsActivity.this)) {
                    onAdd();
                }
                else {
                    Common.showToast(DessertsActivity.this,"Internet not available");
                }
                break;

        }
    }

    public void onAdd(){

        if (dessertListSelected != null && dessertListSelected.size() > 0) {
            ArrayList<ExtraProuct> extraProucts = new ArrayList<ExtraProuct>();
            extraProucts.add(0,new ExtraProuct("", 0.0));
            Cart cart = CartHelper.getCart();
            for (Product p : dessertListSelected) {

                p.set_ItemId();
                cart.setItemid(p.get_ItemId());
                cart.addHeader(p);
                cart.addHeaderid(p);
                cart.setQuantity(p.getQuantity());
                cart.additemWithId(p, extraProucts ,p.getQuantity());
                cart.addQuantity(p.getPid(),p.getQuantity());

            }
            checkout.setDessert(true);
            setResult(200);
            finish();
            Log.d("adadasd","ada");
        }
        Log.d("adadasd","if se out");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }
    @Override
    public void OnSelect(List<Product> desserts) {
        dessertListSelected = desserts;
    }

    private void upsellsJsonArray(JSONObject json) {
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            listDataHeader.add(key);
        }
    }

    private List<Product> getListFromArray(JSONObject jsonObject,String key){
        List<Product> products = new ArrayList<>();
        try {
            JSONArray array = jsonObject.getJSONArray(key);
            for(int i =0;i<array.length();i++){
                JSONObject obj = array.getJSONObject(i);//category_idcategory
                products.add(new Product(Integer.parseInt(obj.getString("category_idcategory")),Integer.parseInt(obj.getString("idproduct")),obj.getString("name")
                        ,obj.getString("idproduct_images"),new BigDecimal(obj.getString("price")),obj.getString("description"),
                        0,"",false,obj.getString("idproduct_images")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return products;
    }


}
