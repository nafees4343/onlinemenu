package com.example.lenovom71e.menuapp.util;

/**
 * Created by admin on 2/6/2018.
 */

public class OrderPostBack {
    private String message;
    private String oid;
    private String amount;

    public String getMessage() {
        return message;
    }

    public String getOid() {
        return oid;
    }

    public String getAmount() {
        return amount;
    }
}
