package com.example.lenovom71e.menuapp.model;

import com.android.tonyvu.sc.model.ExtraProuct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 2017-07-14.
 */

public class CheckOut {


    public interface checkoutData {
         ArrayList<Integer> getProductHeaderId();
         //void setProductHeaderId(ArrayList<Integer> productHeaderId);
         HashMap<Integer, List<ExtraProuct>> getIdProductExtraList();
         //void setIdProductExtraList(HashMap<Integer, List<ExtraProuct>> idProductExtraList);

    }

    HashMap<Integer, List<Integer>> checkoutProductWithList;
    private ArrayList<Integer> productHeaderId = new ArrayList<>();
    private HashMap<Integer, List<ExtraProuct>> idProductExtraList = new HashMap<>();
    int mQuantity;
    BigDecimal mSubTotal, mTaxValue, mCheckOutTotal, mCheckoutTotalAfterDiscount;
    Address providerAddress;
    int btnorderTypeId, orderTypeId ;
    ArrayList<String> timeperiod = new ArrayList<>();
    String[] timeArray = {"7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM",
            "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM"
            , "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM"
            , "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM", "12:00 AM"};
    boolean deliver,pickup,eatin;
    ArrayList<Integer> productQuantities = new ArrayList<>();
    String orderDate= "", orderTime, checkoutInstruction;
    String deliveryAddress, deliveryAddress1, deliveryCity, deliveryProvince, deliveryPostcode, deliveryCompanyName, deliveryCompanyEmail;
    String billingCompany , billingEmail , shippingFirstName, shippingLastName;

    String phoneno;
    Boolean deliveryIsDifferent = false;
    String userId ;
    private Boolean isDessert;




    public CheckOut(){

    }
    public CheckOut(HashMap<Integer, List<Integer>> products,
                    int itemQuantity, BigDecimal subTotal,
                    BigDecimal taxValue,
                    BigDecimal checkOutTotal){
        checkoutProductWithList = products;
        mQuantity = itemQuantity;
        mSubTotal = subTotal;
        mTaxValue = taxValue;
        mCheckOutTotal = checkOutTotal;


    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void clearCheckout(){
        orderDate = null;
        productHeaderId.clear();
        idProductExtraList.clear();
        mQuantity = 0;
        mSubTotal = mSubTotal.ZERO;
        mTaxValue = mTaxValue.ZERO;
        mCheckOutTotal = mCheckOutTotal.ZERO;
        productQuantities.clear();
        orderDate = "";
        orderTime = "";
        checkoutInstruction = "";
        deliveryAddress = ""; deliveryAddress1 = ""; deliveryCity = ""; deliveryProvince = ""; deliveryPostcode = "";
                 deliveryCompanyName = ""; deliveryCompanyEmail = ""; billingCompany = ""; billingEmail = ""; shippingFirstName = ""; shippingLastName = ""; phoneno = "";
                 userId = "";
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getShippingFirstName() {
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        this.shippingFirstName = shippingFirstName;
    }

    public String getShippingLastName() {
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        this.shippingLastName = shippingLastName;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public Boolean getDeliveryIsDifferent() {
        return deliveryIsDifferent;
    }

    public void setDeliveryIsDifferent(Boolean deliveryIsDifferent) {
        this.deliveryIsDifferent = deliveryIsDifferent;
    }

    public String getCheckoutInstruction() {
        return checkoutInstruction;
    }

    public void setCheckoutInstruction(String checkoutInstruction) {
        this.checkoutInstruction = checkoutInstruction;
    }

    public BigDecimal getmCheckoutTotalAfterDiscount() {
        return mCheckoutTotalAfterDiscount;
    }

    public void setmCheckoutTotalAfterDiscount(BigDecimal mCheckoutTotalAfterDiscount) {
        this.mCheckoutTotalAfterDiscount = mCheckoutTotalAfterDiscount;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public ArrayList<Integer> getProductQuantities() {
        return productQuantities;
    }

    public void setProductQuantities(ArrayList<Integer> productQuantities) {
        this.productQuantities = productQuantities;
    }

    public ArrayList<String> getAllTimePeriod(){
        return timeperiod;
    }

    public ArrayList<String> getTimeperiod(String openTime, String closeTime) {
        int indexclose = -1;
        int indexOpen = -1;
        timeperiod = new ArrayList<>();
        for (int i=0;i < timeArray.length; i++){
            if (timeArray[i].equals(closeTime)){
                indexclose = i;
                break;
            }
        }
        for (int i=0;i < timeArray.length; i++){
            if (timeArray[i].equals(openTime)){
                indexOpen = i;
                break;
            }
        }

            timeperiod.add("SELECT TIME");
            for (int i=indexOpen; i < indexclose + 1 ; i++){
            timeperiod.add(timeArray[i]);

        }

        return timeperiod;
    }

    public boolean isDeliver() {
        return deliver;
    }

    public void setDeliver(boolean deliver) {
        this.deliver = deliver;
    }

    public boolean isPickup() {
        return pickup;
    }

    public void setPickup(boolean pickup) {
        this.pickup = pickup;
    }

    public boolean isEatin() {
        return eatin;
    }

    public void setEatin(boolean eatin) {
        this.eatin = eatin;
    }

    public void setTimeperiod(ArrayList<String> timeperiod) {
        this.timeperiod = timeperiod;
    }
    public void addToTimePeriod(String time){
        this.timeperiod.add(time);
    }

    public int getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(int orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public int getBtnorderTypeId() {
        return btnorderTypeId;
    }

    public void setBtnorderTypeId(int btnorderTypeId) {
        this.btnorderTypeId = btnorderTypeId;
    }

    public Address getProviderAddress() {
        return providerAddress;
    }

    public void setProviderAddress(Address providerAddress) {
        this.providerAddress = providerAddress;
    }

    public ArrayList<Integer> getProductHeaderId() {
        return productHeaderId;
    }

    public void setProductHeaderId(ArrayList<Integer> productHeaderId) {
        this.productHeaderId = productHeaderId;
    }

    public HashMap<Integer, List<ExtraProuct>> getIdProductExtraList() {
        return idProductExtraList;
    }

    public void setIdProductExtraList(HashMap<Integer, List<ExtraProuct>> idProductExtraList) {
        this.idProductExtraList = idProductExtraList;
    }

    public HashMap<Integer, List<Integer>> getCheckoutProductWithList() {
        return checkoutProductWithList;
    }

    public void setCheckoutProductWithList(HashMap<Integer, List<Integer>> checkoutProductWithList) {
        this.checkoutProductWithList = checkoutProductWithList;
    }

    public int getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(int mQuantity) {
        this.mQuantity = mQuantity;
    }

    public BigDecimal getmSubTotal() {
        return mSubTotal;
    }

    public void setmSubTotal(BigDecimal mSubTotal) {
        this.mSubTotal = mSubTotal;
    }

    public BigDecimal getmTaxValue() {
        return mTaxValue;
    }

    public void setmTaxValue(BigDecimal mTaxValue) {
        this.mTaxValue = mTaxValue;
    }

    public BigDecimal getmCheckOutTotal() {
        return mCheckOutTotal;
    }

    public void setmCheckOutTotal(BigDecimal mCheckOutTotal) {
        this.mCheckOutTotal = mCheckOutTotal;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryAddress1() {
        return deliveryAddress1;
    }

    public void setDeliveryAddress1(String deliveryAddress1) {
        this.deliveryAddress1 = deliveryAddress1;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getDeliveryProvince() {
        return deliveryProvince;
    }

    public void setDeliveryProvince(String deliveryProvince) {
        this.deliveryProvince = deliveryProvince;
    }

    public String getDeliveryPostcode() {
        return deliveryPostcode;
    }

    public void setDeliveryPostcode(String deliveryPostcode) {
        this.deliveryPostcode = deliveryPostcode;
    }

    public String getDeliveryCompanyName() {
        return deliveryCompanyName;
    }

    public void setDeliveryCompanyName(String deliveryCompanyName) {
        this.deliveryCompanyName = deliveryCompanyName;
    }

    public String getDeliveryCompanyEmail() {
        return deliveryCompanyEmail;
    }

    public void setDeliveryCompanyEmail(String deliveryCompanyEmail) {
        this.deliveryCompanyEmail = deliveryCompanyEmail;
    }

    public Boolean getDessert() {
        return isDessert;
    }

    public void setDessert(Boolean dessert) {
        isDessert = dessert;
    }

}
