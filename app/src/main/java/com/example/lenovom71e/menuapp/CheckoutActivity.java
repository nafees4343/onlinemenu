package com.example.lenovom71e.menuapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.adapter.CustomTimeAdapter;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.enums.OrderType;
import com.example.lenovom71e.menuapp.model.Address;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.util.Common;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by admin on 2017-07-19.
 */

public class CheckoutActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener ,TimePickerDialog.OnTimeSetListener {

    String selecteddate;
    Button dateSelection,timeSelection;
    Spinner time;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    final Calendar c = Calendar.getInstance();
    Spinner spin ;
    //Parsing Data from Url
    String JsonURL = "https://demo.online-menu.ca/api/location" ;
    String URL_NO_OF_TYPES = "https://demo.online-menu.ca/api/type" ;

    String data = "";
    RequestQueue requestQueue;
    ArrayList<Address> addresses = new ArrayList<>();
    String openTime, closeTime,closedDay,orderType,selectedTime;
    Cart cart = CartHelper.getCart();
    CheckOut checkOut = CheckoutHelper.getCheckout();
    int dateIntger;
    private TextView nameLbl, addrresLbl, cityLbl, phoneLbl,hoursWork;
    private LinearLayout forDelivery, forEat;
    RadioGroup rg, delivery_radio;
    private Boolean mIgnoreEvent = false;
    private Boolean deliveryIsChanged = false;
    private Boolean isContinueEnabled = false;
    private int day = 0,month = 0 ,years = 0;
    private InputMethodManager imm;
    ProgressDialog pDialog;
    RadioButton rbtn_different,rbtn_same,rbtn_pickup,rbtn_eatin,rbtn_delivery;

    String[] timeArray = {"7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM",
            "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM"
            , "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM"
            , "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM", "12:00 AM"};

    ArrayList<String> currentTimeArray = new ArrayList<>();
    Boolean chk = true,checkDifFromAddress;
    TextView different_address;
    SharedPreferences prefs;
    String value;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);

        pDialog = new ProgressDialog(CheckoutActivity.this);


        rbtn_pickup = findViewById(R.id.pickup);
        rbtn_eatin = findViewById(R.id.eatin);
        rbtn_delivery = findViewById(R.id.delivery);
        rbtn_same = findViewById(R.id.same);
        rbtn_different = findViewById(R.id.different);
        different_address = findViewById(R.id.different_address);
        final Button locationbtn = (Button) findViewById(R.id.locationButton);
        final View v = findViewById(R.id.locationGroup);
        final View v1 = findViewById(R.id.addressLabel);
        final View dateTimeGroupView = findViewById(R.id.dateTimeGroup);
        nameLbl = (TextView) findViewById(R.id.nameLbl);
        addrresLbl= (TextView) findViewById(R.id.addressLbl);
        cityLbl= (TextView) findViewById(R.id.cityLbl);
        phoneLbl= (TextView) findViewById(R.id.phoneLbl);
        final Button continueBtn = (Button) findViewById(R.id.continueCheckoutBtn);
        hoursWork = (TextView) findViewById(R.id.hoursLabel);
        final Button cancelButton = (Button) findViewById(R.id.cancelCheckoutBtn);
        final EditText checkoutInstruction = (EditText) findViewById(R.id.checkoutInstruction);
        rg = (RadioGroup) findViewById(R.id.orderType);
        forEat = (LinearLayout)findViewById(R.id.foreat);
        forDelivery = (LinearLayout)findViewById(R.id.fordelivery);
        delivery_radio = (RadioGroup)findViewById(R.id.delivereyOption);




        categoriesOfOrder();

        //initiate the date picker and the button and time
        dateSelection = (Button) findViewById(R.id.dateBtn);
        timeSelection = (Button) findViewById(R.id.time_btn);
        time = (Spinner) findViewById(R.id.timeBtn);
        time.setOnItemSelectedListener(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        prefs = getSharedPreferences(getApplicationContext().getPackageName(), MODE_PRIVATE);


        init();

        checkOut.setProviderAddress(new Address());

        checkOut = CheckoutHelper.getCheckout();

        checkOutLocationAndOpening();


        locationbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent locationIntent = new Intent(CheckoutActivity.this, LocationActivity.class);
                locationIntent.putExtra("extraLocations", addresses);
                startActivityForResult(locationIntent, 001);

            }
        });

        rbtn_different.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Common.isOnline(CheckoutActivity.this)) {
                    Intent intent = new Intent(CheckoutActivity.this, AddressActivity.class);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("rbtnDifferent", "different");
                    editor.commit();
                    startActivity(intent);
                }
                else {
                    Common.showToast(CheckoutActivity.this, "Internet not available...");
                }
            }
        });


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.delivery:
                        checkOut.setDeliver(true);
                        checkOut.setEatin(false);
                        checkOut.setPickup(false);
                        isContinueEnabled = false;
                        /*dateSelection.setText("Select Date");
                        timeSelection.setText("Select Time");*/
                        RadioButton same = (RadioButton) findViewById(R.id.same);
                        same.setChecked(true);
                        setDateCheckOut();
                        forDelivery.setVisibility(View.VISIBLE);
                        forEat.setVisibility(View.GONE);

                        checkOut.setBtnorderTypeId(R.id.delivery);
                        //TODO: Set the Delivery id = 1
                        checkOut.setOrderTypeId(1);

                        //make open now method
                        requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                        JsonObjectRequest objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {


                                    JSONObject objectArray = response.getJSONObject("hours");
                                    Log.d("Json Object Menthod", String.valueOf(objectArray));
                                    JSONObject delivery = objectArray.getJSONObject("delivery");
                                    openTime = delivery.getString("open");
                                    closeTime = delivery.getString("close");
                                    closedDay =  delivery.getString("closed");
                                    orderType = OrderType.DELIVERY.getValue();
//                                    checkOut.getTimeperiod(openTime,closeTime);
                                    //Load timeSpinner data
                                   /* String[] stringarrray = new String[currentTimeArray.size()];
                                    stringarrray = checkOut.getTimeperiod(openTime, closeTime).toArray(stringarrray);*/
//                                  //TODO:Setup Time Spinner
                                    ArrayList<String> times = setTime(openTime,closeTime);
                                    CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), times);
                                    customTimeAdapter.notifyDataSetChanged();
                                    //time.setVisibility(View.INVISIBLE);
                                    time.setAdapter(customTimeAdapter);
//

                                    if(delivery.getString("closed").equals("1")) {
                                        Log.d("closeddddd","In");
                                        hoursWork.setText("We are closed now");
                                    }else {
                                        hoursWork.setText("Delivery open from: " + delivery.getString("open") + " To " + delivery.getString("close"));

                                    }
                                    hidePDialog();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    hidePDialog();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Volley", "Error");
                                hidePDialog();
                            }
                        });

                        objectRequest.setShouldCache(false);
                        requestQueue.add(objectRequest);
                        // Showing progress dialog before making http request
                        pDialog.setMessage("Loading...");
                        pDialog.setCancelable(false);
                        pDialog.show();

                        break;

                    case R.id.eatin:
                        checkOut.setDeliver(false);
                        checkOut.setEatin(true);
                        checkOut.setPickup(false);
                        isContinueEnabled = false;
                        /*dateSelection.setText("Select Date");
                        timeSelection.setText("Select Time");*/
                        checkOut.setBtnorderTypeId(R.id.eatin);
                        setDateCheckOut();
                        forDelivery.setVisibility(View.GONE);
                        forEat.setVisibility(View.VISIBLE);

                        //TODO: Set the Eatin id = 1 and address
                        checkOut.setOrderTypeId(2);
                        if (checkOut.getProviderAddress() != null){
                            //make open now method
                            requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                            objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {


                                        JSONObject objectArray = response.getJSONObject("hours");
                                        Log.d("Json Object Menthod", String.valueOf(objectArray));
                                        JSONObject eatin = objectArray.getJSONObject("working");
                                        openTime = eatin.getString("open");
                                        closeTime = eatin.getString("close");
                                        closedDay = eatin.getString("closed");
                                        orderType = OrderType.EATIN.getValue();
                                        //Load timeSpinner data
                                        ArrayList<String> times = setTime(openTime,closeTime);
                                        CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), times);
                                        customTimeAdapter.notifyDataSetChanged();
                                        time.setAdapter(customTimeAdapter);

                                        if(eatin.getString("closed").equals("1")) {
                                            hoursWork.setText("We are closed now");
                                        }else {
                                            hoursWork.setText("We are open from: " + eatin.getString("open") + " To " + eatin.getString("close"));
                                        }

                                        hidePDialog();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        hidePDialog();

                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Volley", "Error");
                                    hidePDialog();
                                }
                            });

                            requestQueue.add(objectRequest);
                            // Showing progress dialog before making http request
                            pDialog.setMessage("Loading...");
                            pDialog.setCancelable(false);
                            pDialog.show();
                        }
                        break;

                    case R.id.pickup:
                        checkOut.setDeliver(false);
                        checkOut.setEatin(false);
                        checkOut.setPickup(true);
                        isContinueEnabled = false;
                        checkOut.setBtnorderTypeId(R.id.eatin);
                       /* dateSelection.setText("Select Date");
                        timeSelection.setText("Select Time");*/
                        forDelivery.setVisibility(View.GONE);
                        forEat.setVisibility(View.VISIBLE);
                        setDateCheckOut();

                        //TODO: Set the Pickup id = 1
                        checkOut.setOrderTypeId(3);
                        if (checkOut.getProviderAddress() != null){
                            //make open now method
                            requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                            objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {
                                        JSONObject objectArray = response.getJSONObject("hours");
                                        Log.d("Json Object Menthod", String.valueOf(objectArray));
                                        JSONObject pickup = objectArray.getJSONObject("working");
                                        openTime = pickup.getString("open");
                                        closeTime = pickup.getString("close");
                                        closedDay = pickup.getString("closed");
                                        orderType = OrderType.PICKUP.getValue();

                                        //Load timeSpinner data
                                        ArrayList<String> times = setTime(openTime,closeTime);
                                        CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), times);
                                        customTimeAdapter.notifyDataSetChanged();
                                        time.setAdapter(customTimeAdapter);

                                        if(pickup.getString("closed").equals("1")) {
                                            hoursWork.setVisibility(View.VISIBLE);
                                            hoursWork.setText("We are closed now");
                                        }else {
                                            hoursWork.setVisibility(View.VISIBLE);
                                            hoursWork.setText("We are open from: " + pickup.getString("open") + " To " + pickup.getString("close"));
                                        }

                                        hidePDialog();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        hidePDialog();

                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Volley", "Error");
                                    hidePDialog();
                                }
                            });
                            objectRequest.setShouldCache(false);
                            requestQueue.add(objectRequest);

                            // Showing progress dialog before making http request
                            pDialog.setMessage("Loading...");
                            pDialog.setCancelable(false);
                            pDialog.show();
                        }
                        break;

                }
            }
        });


        if (checkOut.getBtnorderTypeId()> 0 ) {
            RadioButton rdbtn = (RadioButton) findViewById(checkOut.getBtnorderTypeId());
            rdbtn.setChecked(true);
            checkOut.setBtnorderTypeId(0);
        }

        // Perform click event on button
        Calendar now = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(
                CheckoutActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        timePickerDialog = TimePickerDialog.newInstance(
                CheckoutActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                now.get(Calendar.SECOND),
                true
        );
        List<Calendar> weekDays = getAllDate(now,2);
        Calendar[] disabledDays = weekDays.toArray(new Calendar[weekDays.size()]);
        //datePickerDialog.setDisabledDays(disabledDays);
         datePickerDialog.setSelectableDays(disabledDays);
        dateSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calender class instance a nd current date
                if(Common.isOnline(CheckoutActivity.this)) {
                    datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
                    datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                            dateSelection.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            Log.d("datepickerOnclick", "date: " + dateSelection.getText().toString());
                            //
                            selecteddate = dateSelection.getText().toString();
                            //to do nfs
                            Calendar selectedCal = Calendar.getInstance();
                            selectedCal.set(year, monthOfYear, dayOfMonth);
                            java.util.Date selectedDate = selectedCal.getTime();
                            Log.d("dateStamp", "::" + Common.getLocationDate(selectedDate));
                            checkOffDay(Common.getLocationDate(selectedDate), orderType, year, monthOfYear, dayOfMonth);
                        }
                    });
                }
                else {
                    Common.showToast(CheckoutActivity.this, "Internet not available...");
                }

            }
        });


        timeSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(orderType.equals(OrderType.DELIVERY.getValue())){
                    if(closedDay.equals("1")){
                        Common.showToast(CheckoutActivity.this,"Delivery is closed today");
                    }else{
                        timePickerDialog.show(getFragmentManager(), "Timepickerdialog");
                    }
                }
                else if(orderType.equals(OrderType.EATIN.getValue())){
                    if(closedDay.equals("1")){
                        Common.showToast(CheckoutActivity.this,"Eatin is closed today");
                    }else{
                        timePickerDialog.show(getFragmentManager(),"Timepickerdialog");
                    }
                }
                else if(orderType.equals(OrderType.PICKUP.getValue())){
                    if(closedDay.equals("1")){
                        Common.showToast(CheckoutActivity.this,"Pickup is closed today");
                    }else{
                        timePickerDialog.show(getFragmentManager(), "Timepickerdialog");
                    }
                }
            }
        });


        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isOnline(CheckoutActivity.this)) {
                    if (!closedDay.equals("1")) {
                        if (isContinueEnabled) {
                            //Save checkout Insturciton
                            long currentTime = Calendar.getInstance().getTime().getTime();

                            Log.d("currentTime", "c: " + currentTime);

                            checkOut.setCheckoutInstruction(checkoutInstruction.getText().toString());

                            Log.d("checkoutInstruction", "" + checkoutInstruction.getText().toString());
                            Log.d("checkOut.getOrderDate:", "" + checkOut.getOrderDate());
                            Log.d("AAAAA:", "Address: " + checkOut.getDeliveryAddress());
                            Log.d("getTimeStamp", "" + Common.getTimeStamp(checkOut.getOrderDate() + " " + checkOut.getOrderTime()));
                            Log.d("currentTime: ", "" + currentTime);

                            if (Common.getTimeStamp(checkOut.getOrderDate() + " " + checkOut.getOrderTime()) > currentTime) {
                                if (SaveSharedPreference.getCheckLogInState(CheckoutActivity.this).length() > 0 | SaveSharedPreference.getAccountAsGuest(CheckoutActivity.this)) {
                                    // close this activity
                                    Intent intent = new Intent(CheckoutActivity.this, PassLoginActivity.class);
                                    intent.putExtra("isCheckout", true);
                                    intent.putExtra("total", value);
                                    Log.d("saveuser: ", "asdaada");
                                    startActivity(intent);
                                   /* //todo
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.remove("rbtnDifferent");
                                    editor.apply();*/

                                    finish();
                                } else {
                                    Intent intent = new Intent(CheckoutActivity.this, LoginActivity.class);
                                    intent.putExtra("isCheckout", true);
                                    intent.putExtra("total", value);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                showAlertDialogTime("Please select correct time");
                                hoursWork.setError("");
                            }
                        } else {
                            showAlertDialogTime("Please select time");
                            hoursWork.setError("");
                        }
                    } else {
                        showAlertDialogTime(orderType.toLowerCase() + " is closed today...");
                    }
                }else {
                    Common.showToast(CheckoutActivity.this, "Internet not available...");
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isOnline(CheckoutActivity.this)) {
                    //clear Checkout
                    checkOut.clearCheckout();
                    cart.clear();
                    Intent cancelIntent = new Intent(CheckoutActivity.this, CategoriesActivity.class);
                    startActivity(cancelIntent);
                    finish();
                }
                else {
                    Common.showToast(CheckoutActivity.this, "Internet not available...");
                }
            }
        });


        RadioButton pickup = (RadioButton) findViewById(R.id.pickup);
        pickup.setChecked(true);
        pickup.toggle();
        RadioButton same = (RadioButton) findViewById(R.id.same);
        same.setChecked(true);

    }

    public void categoriesOfOrder(){
        requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(URL_NO_OF_TYPES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("type");
                    Log.d("Json Object Menthod", String.valueOf(jsonArray));

                    for (int i = 0; i < jsonArray.length(); i++) {
                        String value = String.valueOf(jsonArray.getString(i));

                        Log.e("jsonnnnn", i+"="+value);

                        if(value.equals("Pickup")){
                            rbtn_pickup.setVisibility(View.VISIBLE);
                        }
                        if(value.equals("Eatin")){
                            rbtn_eatin.setVisibility(View.VISIBLE);
                        }
                        if(value.equals("Delivery")){
                            rbtn_delivery.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "Error");
            }
        });

        requestQueue.add(objectRequest);
    }

    private void setDateCheckOut(){
        Calendar c = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(c.getTime());
        dateSelection.setText(currentDate);
        checkOut.setOrderDate(currentDate);
        day = c.get(Calendar.DAY_OF_MONTH);
        month = c.get(Calendar.MONTH) + 1;
        years = c.get(Calendar.YEAR);
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Button continueBtn = (Button) findViewById(R.id.continueCheckoutBtn);
        isContinueEnabled = false;
        ArrayList<String> times = setTime(openTime, closeTime);
        selectedTime = times.get(position);
        String[] split1 = selectedTime.split(":");
        //CheckOut checkout = CheckoutHelper.getCheckout();
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.HOUR_OF_DAY, Integer.parseInt(split1[0]));
        /*String[] split2 = spilt1[1].split(" ");*/
        calender.set(Calendar.MINUTE, Integer.parseInt(split1[1]));

        Log.d("selectedTime","selected time: "+selectedTime);
        /*if(split2[0].equals("AM")) {
            calender.set(Calendar.AM_PM, Calendar.AM);
        }else{
            calender.set(Calendar.AM_PM, Calendar.PM);
        }*/
        String time = Common.getTime(calender.getTime());
        String completeDate = day + "/" + month + "/" + years;

        if (checkOut.getOrderDate() == null || checkOut.getOrderDate().equals("")) {
            //Todo pick a date
            showAlertDialogTime("Please Choose date first");
            this.time.setSelection(0);
        } else {
            if (dateValidation(completeDate)) {
                if (timeValidation(time)) {
                    if (timeDateValidation(completeDate + " " + time, completeDate, time)) {
                        /*if(!closedDay.equals("1")) {*/
                            isContinueEnabled = true;
                            checkOut.setOrderTime(time);
                            timeSelection.setText(time);
                        /*}else{
                            showAlertDialogTime(orderType.toLowerCase() + " is closed today...");
                        }*/
                    } else {
                        showAlertDialogTime("Please choose future time");
                    }
                } else {
                    showAlertDialogTime("We are open from: " + openTime + " To " + closeTime);
                }
            } else {
                showAlertDialogTime("Its not a valid date please choose a valid date");
            }
        }

    }

    public void checkOutLocationAndOpening(){

        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                        Log.d("rere",""+response);
                    JSONArray objectArray = response.getJSONArray("locations");
                    int count = 0;
                    for(int i = 0; i <objectArray.length(); i++){
                        JSONObject currentAddress = objectArray.getJSONObject(i);
                        Address address = new Address();
                        address.setmAddress(currentAddress.getString("address") + ", " + currentAddress.getString("postal_code"));
                        address.setmName(currentAddress.getString("name"));
                        address.setmCity(currentAddress.getString("city"));
                        address.setmPhoneNumber(currentAddress.getString("phone"));
                        address.setId(Integer.parseInt(currentAddress.getString("idlocation")));
                        addresses.add(address);
                    }
                    if (addresses.size() > 0){
                        nameLbl.setText(addresses.get(0).getmName());
                        addrresLbl.setText(addresses.get(0).getmAddress());
                        cityLbl.setText(addresses.get(0).getmCity());
                        phoneLbl.setText(addresses.get(0).getmPhoneNumber());
                        checkOut.setProviderAddress(addresses.get(0));
                    }

                    hidePDialog();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hidePDialog();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "Error");
                hidePDialog();

            }
        });
        objectRequest.setShouldCache(false);
        requestQueue.add(objectRequest);

        // Showing progress dialog before making http request
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

    }


    private boolean timeValidation(String time){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
        Date currentTime = null;
        try {
            currentTime = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if(currentTime.compareTo(checkWorkingTime(Common.getTimeCovertor(openTime))) >= 0 && currentTime.compareTo(checkWorkingTime(Common.getTimeCovertor(closeTime))) <= 0)
            return true;
        else
            return false;

    }

    private boolean timeDateValidation(String timeDate,String date,String time){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm",Locale.ENGLISH);
        Date currentTime = null,dateMin = null,dateMax = null;
        try {
            currentTime = sdf.parse(timeDate);
            dateMin = checkWorkingTimeDate(Common.getDate(new Date()) + " " +Common.getTimeCovertor(openTime));
            dateMax = checkWorkingTimeDate(Common.getDate(new Date()) + " " +Common.getTimeCovertor(closeTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(currentTime.after(dateMin) && currentTime.before(dateMax)) {
            if(checkWorkingTime(time).compareTo(checkWorkingTime(Common.getTime(new Date()))) >=0 ){
                return true;
            }else{
                return false;
            }
        } else {
            if(dateValidation(date) && timeValidation(time)){
                return true;
            }else{
                return false;
            }
        }

    }

    private boolean dateValidation(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
        Date minDate = null,maxDate = null,currentDate = null;
        try {
             minDate = sdf.parse(checkMinDateTime());
             currentDate = sdf.parse(date);
             maxDate = sdf.parse(checkMaxDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(minDate.compareTo(currentDate) <= 0 && 0 <= maxDate.compareTo(currentDate))
            return true;
        else
            return false;
    }


    public String  checkMinDateTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public String  checkMaxDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, 8); // Adding 5 days
        String output = sdf.format(c.getTime());
        return output;
    }

    public Date checkWorkingTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Date checkWorkingTimeDate(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public int checkAMPM(String ampm){
        int timePeriod = 0;
        if (ampm.contentEquals("PM")){
            timePeriod = 1;
        }else if (ampm.contentEquals("AM")){
            timePeriod = 0;
        }
        return timePeriod;
    }

    public void showAlertDialogTime(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        rbtn_delivery.setChecked(false);
        finish();
    }

    public void init(){
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("CHECKOUT OPTION");
        bckbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                Intent i = new Intent(CheckoutActivity.this,ShoppingCartActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 001 && resultCode == Activity.RESULT_OK){
            if (data != null ){
                nameLbl.setText(data.getStringExtra("companyName"));
                addrresLbl.setText(data.getStringExtra("companyAddress"));
                cityLbl.setText(data.getStringExtra("companyCity"));
                phoneLbl.setText(data.getStringExtra("companyPhone"));
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.same:
                break;
           /* case R.id.different:
                Log.d("zzzzz","stttttt");
                    Intent intent = new Intent(CheckoutActivity.this, AddressActivity.class);
                    startActivity(intent);
                    rbtn_different.setChecked(false);
                break;*/
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    Log.d("Onstart","Onstart");
    }

    @Override
    protected void onResume() {
        super.onResume();


        if(Common.getCheckRadio(getApplication()).equals("different")){
            rbtn_different.setChecked(true);
            Log.d("1","2");
        }
        else {
            rbtn_same.setChecked(true);
            Log.d("1","3");

        }
         /*  else if(delivery_radio != null){
            delivery_radio.check(0);
            Log.d("1","1");
        }
        else
            {
             delivery_radio.check(0);
             Log.d("1","1");
            }*/
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selectedCal =  Calendar.getInstance();
        selectedCal.set(year,monthOfYear,dayOfMonth);
        java.util.Date selectedDate = selectedCal.getTime();
        //checkOffDay(Common.getLocationDate(selectedDate),orderType,year,monthOfYear,dayOfMonth);
        Log.d("OndateSet","Ondate mei agya");
        //}
    }

    private void datePicker(int year, int monthOfYear, int dayOfMonth){
        java.util.Date current = Calendar.getInstance().getTime();
        Calendar selectedCal =  Calendar.getInstance();
        selectedCal.set(year,monthOfYear,dayOfMonth);

        String completeDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        java.util.Date selectedDate = selectedCal.getTime();
        {
            if (selectedDate.getTime() < current.getTime()) {
                Log.d("checkoutActivity", "this is less time ");
                showAlertDialogTime("Please choose a valid date !!");

            } else {
                if (dateValidation(completeDate)) {
                    if (timeDateValidation(completeDate + " " + selectedTime, completeDate, selectedTime)) {
                        if(!closedDay.equals("1")) {
                            day = dayOfMonth;
                            month = (monthOfYear + 1);
                            years = year;
                            dateSelection.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            Log.d("datee",""+year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
                            //TODO: Save the date into checkout helper method
                            checkOut.setOrderDate(dateSelection.getText().toString());
                            // time.setVisibility(View.VISIBLE);
                            //checkOffDay(Common.getLocationDate(selectedDate),orderType,"","","");


                        }else{
                            showAlertDialogTime(orderType + " is closed today...");
                        }
                        Log.d("compare Date", String.valueOf(dateSelection));
                    } else {
                        showAlertDialogTime("Please choose future time");

                    }
                } else {
                    showAlertDialogTime("Its not a valid date please choose a valid date");
                }
            }
        }
    }

    public  List<Calendar> getAllDate(Calendar aCurrentDateCal, int aMaxMonth){
        //max month next month for Exp: next 2 month from this month
        List<Calendar> disableDayList = new ArrayList<>();
        Calendar cal;
        cal = aCurrentDateCal;
        for (int index = 0; index < aMaxMonth; index++) {
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            Calendar curCal1 = Calendar.getInstance();
            curCal1.set(year, month , 1);
            int daysInMonth = curCal1.getActualMaximum(Calendar.DAY_OF_MONTH);

            for (int day = 1; day <= daysInMonth; day++) {
                Calendar curCal = Calendar.getInstance();
                curCal.set(year, month, day);
                int dayOfWeek = curCal.get(Calendar.DAY_OF_WEEK);
                disableDayList.add(curCal);
            }

            cal.add(Calendar.MONTH, 1);
        }
        return disableDayList;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        isContinueEnabled = false;

        CheckOut checkout = CheckoutHelper.getCheckout();
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.HOUR_OF_DAY,hourOfDay);
        calender.set(Calendar.MINUTE,minute);
        calender.set(Calendar.SECOND, second);
        String time = Common.getTime(calender.getTime());
        String completeDate =  day + "/" + month  +"/" + years ;

        if (checkout.getOrderDate() == null || checkout.getOrderDate().equals("")) {
            //Todo pick a date
            showAlertDialogTime("Please Choose date first");
            this.time.setSelection(0);
        }else {
            if(dateValidation(completeDate)){
                if(timeValidation(time)){
                    if(timeDateValidation(completeDate +" "+ time,completeDate,time)){
                        isContinueEnabled = true;
                        checkout.setOrderTime(time);
                        timeSelection.setText(time);
                    }else{
                        showAlertDialogTime("Please choose future time");
                    }
                }else{
                    showAlertDialogTime("We are open from: " + openTime + " To " + closeTime);
                }
            }else{
                showAlertDialogTime("Its not a valid date please choose a valid date");
            }

        }
    }

    private ArrayList<String> setTime(String timeOpen,String timeClose){
        List<java.sql.Time> intervals = new ArrayList<>();
        ArrayList<String> arrayList = new ArrayList<>();
        int stime = Integer.parseInt(getFormatedTime(timeOpen).split(":")[0]);
        int etime = Integer.parseInt(getFormatedTime(timeClose).split(":")[0]);;
        // These constructors are deprecated and are used only for example
        java.sql.Time startTime = new java.sql.Time(stime, 0, 0);
        java.sql.Time endTime = new java.sql.Time(etime, 0, 0);

        intervals.add(startTime);

        Calendar cal = Calendar.getInstance();
        cal.setTime(startTime);
        while (cal.getTime().before(endTime)) {
            cal.add(Calendar.MINUTE, 30);
            intervals.add(new java.sql.Time(cal.getTimeInMillis()));
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        for (java.sql.Time time : intervals) {
            arrayList.add(sdf.format(time));
        }
        return arrayList;
    }


    public static String getFormatedTime(String date) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm");
        Date time = null;
        try {
            time = sdfDate.parse(Common.getTimeCovertor(date));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String strDate = sdfDate.format(time);
        return strDate;
    }

    private void checkOffDay(String date, final String locationType, final int year, final int monthOfYear, final int dayOfMonth){
        Log.d("ccccc","agya");
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject avaliable;
                    JSONObject objectArray = jsonObject.getJSONObject("hours");
                    Log.d("JSON_LOC", String.valueOf(objectArray));
                    Log.d("JSON_LOC", String.valueOf(dayOfMonth +"-"+month+"-"+year));
                    if(locationType.equals(OrderType.DELIVERY.getValue())) {
                        avaliable = objectArray.getJSONObject("delivery");
                    }else{
                        avaliable = objectArray.getJSONObject("working");
                    }
                    openTime = avaliable.getString("open");
                    closeTime = avaliable.getString("close");
                    closedDay = avaliable.getString("closed");

                    ArrayList<String> times = setTime(openTime,closeTime);
                    CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), times);
                    customTimeAdapter.notifyDataSetChanged();
                    time.setAdapter(customTimeAdapter);
                    datePicker( year,  monthOfYear,  dayOfMonth);
                    if(closedDay.equals("1")) {
                        hoursWork.setText("We are closed now");
                    }else {
                        if(locationType.equals(OrderType.DELIVERY.getValue())) {
                            hoursWork.setText("Delivery open from: " + openTime + " To " + closeTime);
                        }else{
                            hoursWork.setText("We open from: " + openTime + " To " + closeTime);
                        }

                    }
                    hidePDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    hidePDialog();
                }

            }
        };

        LocationRequest registerRequest = new LocationRequest(date, responseListener);
        RequestQueue queue = Volley.newRequestQueue(CheckoutActivity.this);
        registerRequest.setShouldCache(false);
        queue.add(registerRequest);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        registerRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 5000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }
}
