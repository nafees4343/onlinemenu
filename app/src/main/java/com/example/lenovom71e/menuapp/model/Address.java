package com.example.lenovom71e.menuapp.model;

import java.io.Serializable;

/**
 * Created by admin on 2017-07-21.
 */

public class Address implements Serializable{
    String mName;
    String mAddress;
    String mCity;
    String mPhoneNumber;
    Integer id = 0;
    String companyName;
    String companyEmail;


    public  Address(){

    }
    public Address(String name, String address, String city , String phone){
        mName = name;
        mAddress = address;
        mCity = city;
        mPhoneNumber = phone;
    }
    public Address(String name, String address, String city , String phone, Integer number){
        mName = name;
        mAddress = address;
        mCity = city;
        mPhoneNumber = phone;
        this.id = number;
    }
    public Address(String name, String address,String city, String phone, Integer number , String companyName, String companyEmail){
        mName = name;
        mAddress = address;
        mCity = city;
        mPhoneNumber = phone;
        this.id = number;
        this.companyName = companyName;
        this.companyEmail = companyEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public void setmPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }
}
