
package com.example.lenovom71e.menuapp.reponses;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Upsells {

    @SerializedName("Breakfast")
    @Expose
    private List<Breakfast> breakfast = null;
    @SerializedName("Wraps")
    @Expose
    private List<Wrap> wraps = null;
    @SerializedName("Sides")
    @Expose
    private List<Side> sides = null;
    @SerializedName("Beverages")
    @Expose
    private List<Beverage> beverages = null;

    public List<Breakfast> getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(List<Breakfast> breakfast) {
        this.breakfast = breakfast;
    }

    public List<Wrap> getWraps() {
        return wraps;
    }

    public void setWraps(List<Wrap> wraps) {
        this.wraps = wraps;
    }

    public List<Side> getSides() {
        return sides;
    }

    public void setSides(List<Side> sides) {
        this.sides = sides;
    }

    public List<Beverage> getBeverages() {
        return beverages;
    }

    public void setBeverages(List<Beverage> beverages) {
        this.beverages = beverages;
    }

}
