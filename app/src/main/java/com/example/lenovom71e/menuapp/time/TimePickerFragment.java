package com.example.lenovom71e.menuapp.time;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TimePicker;

import com.example.lenovom71e.menuapp.R;

import java.util.Calendar;

/**
 * Created by admin on 2017-07-31.
 */

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        int hour = c.get(Calendar.HOUR_OF_DAY);

        int modulo = c.get(Calendar.MINUTE) % 5;
        if (modulo > 0){
            c.add(Calendar.MINUTE, -modulo);
        }
        c.add(Calendar.MINUTE, 5);
//        final int minute = c.get(Calendar.MINUTE);




        //Create and return a new instance of TimePickerDialog
        return new TimePickerDialog(getActivity(),2 ,this, c.HOUR, c.MINUTE, false);
    }

    //onTimeSet() callback method
    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        //Do something with the user chosen time
        //Get reference of host activity (XML Layout File) TextView widget
        Button tv = (Button) getActivity().findViewById(R.id.timeBtn);
        //Set a message for user
        if (hourOfDay > 11 ){
            hourOfDay = hourOfDay - 12;
            if (hourOfDay == 0){
                tv.setText(12
                        + " : " + String.valueOf(minute) + " PM" );
            }else {
                tv.setText(String.valueOf(hourOfDay)
                        + " : " + String.valueOf(minute) + " PM");
            }
        }else {
            //Display the user changed time on TextView
            tv.setText(String.valueOf(hourOfDay)
                    + " : " + String.valueOf(minute) + " AM" );

        }


    }
}
