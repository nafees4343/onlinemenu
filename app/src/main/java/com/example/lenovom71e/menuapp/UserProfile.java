package com.example.lenovom71e.menuapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.Common;

/**
 * Created by admin on 3/5/2018.
 */

public class UserProfile extends AppCompatActivity {


    String firstName = "", LastName = "",  phoneNumber = "",postcode ="", userId ="", fromActivity = "";
    Boolean isCheckout = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);


        if(getIntent().getExtras() != null) {
            isCheckout = getIntent().getExtras().getBoolean("isCheckout");
        }

        TextView accountNumber = (TextView) findViewById(R.id.accountNumberTxt);
        TextView fullName = (TextView) findViewById(R.id.fullnameEditTxt);
        TextView email = (TextView) findViewById(R.id.emailTxt);
        TextView address = (TextView) findViewById(R.id.addressTxt);
        TextView cityProvince  = (TextView) findViewById(R.id.cityTxt);
        TextView postCode = (TextView) findViewById(R.id.postCodeTxt);
        Button signOut = (Button) findViewById(R.id.signOutBtn);
        Button btnContinue = (Button) findViewById(R.id.continueBtn);
        CheckOut checkout = CheckoutHelper.getCheckout();
        Profile profile = ProfileHelper.getProfilehelper();
        userId = profile.getUserId();

        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        title.setText("PROFILE");

        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);



        fromActivity = profile.getFromActivity();

        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext())){
            firstName = SaveSharedPreference.getGuestfname(getApplicationContext());
            LastName = SaveSharedPreference.getGuestlname(getApplicationContext());
            fullName.setText(firstName + " " + LastName);
            accountNumber.setText("Guest");
            email.setText(SaveSharedPreference.getGuestemail(getApplicationContext()));
            address.setText(SaveSharedPreference.getGuestAddress(getApplicationContext()));
            String city = SaveSharedPreference.getGuestcity(getApplicationContext());
            String province =  SaveSharedPreference.getGuestProvince(getApplicationContext());
            cityProvince.setText(city + ", " + province);

            phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());
            postcode = SaveSharedPreference.getGuestPostcode(getApplicationContext());
            postCode.setText(postcode);
        }else if (SaveSharedPreference.getCheckLogInState(getApplication()).length()>0) {
            accountNumber.setText(SaveSharedPreference.getUserId(getApplication()));
            firstName = SaveSharedPreference.getName(getApplication());
            LastName = SaveSharedPreference.getlastName(getApplication());
            fullName.setText(firstName+" "+LastName);
            email.setText(SaveSharedPreference.getEmail(getApplication()));
            phoneNumber = SaveSharedPreference.getPhone(getApplication());
            postCode.setText(SaveSharedPreference.getPostcode(getApplication()));
            userId = SaveSharedPreference.getUserId(getApplication());
            address.setText(SaveSharedPreference.getAddress(getApplication()));
            String city = SaveSharedPreference.getCity(getApplication());
            String province = SaveSharedPreference.getProvince(getApplication());
            cityProvince.setText(city +", "+province);
            postCode.setText(SaveSharedPreference.getPostcode(getApplication()));

        }else {
            Intent intent = new Intent(UserProfile.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        bckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isOnline(UserProfile.this)) {
                    Intent i = new Intent(UserProfile.this,CategoriesActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    Common.showToast(UserProfile.this,"Internet is not available...");
                }
            }
        });

        //TODO: sign out from user
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isOnline(UserProfile.this)) {

                    Intent intent = new Intent(UserProfile.this, LoginActivity.class);
                    intent.putExtra("isUserProfile", true);
                    SaveSharedPreference.clearUserName(getApplicationContext());
                    SharedPreferences preferences = getSharedPreferences("Mypref", 0);
                    preferences.edit().remove("text").commit();

                    finish();

                    startActivity(intent);
                }
                else {
                    Common.showToast(UserProfile.this,"Internet is not available...");
                }
            }
        });

        //TODO: continue function
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (fromActivity == "categories") {
                if(Common.isOnline(UserProfile.this)) {

                    if (SaveSharedPreference.getAccountAsGuest(getApplication()) & isCheckout) {
                        Intent intent = new Intent(UserProfile.this, PassLoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(UserProfile.this, CategoriesActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else {
                    Common.showToast(UserProfile.this,"Internet is not available...");
                }
            }
        });

    }

    @Override
    public void onBackPressed(){
        if(Common.isOnline(UserProfile.this)) {
            super.onBackPressed();
            Intent i = new Intent(UserProfile.this,CategoriesActivity.class);
            startActivity(i);
            finish();
        }
        else {
            Common.showToast(UserProfile.this,"Internet is not available...");
        }
    }


}
