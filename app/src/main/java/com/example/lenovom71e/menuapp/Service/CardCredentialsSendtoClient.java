package com.example.lenovom71e.menuapp.Service;

import com.example.lenovom71e.menuapp.model.Paymentconfirm;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CardCredentialsSendtoClient {

    @FormUrlEncoded
    @POST("payment/")
    Call<Paymentconfirm> makePayment(@Field("id") Integer id,
                                     @Field("total") Double total,
                                     @Field("full_name")String fullname,
                                     @Field("cc_number")Long cardNumber,
                                     @Field("expiry") String expiryDate,
                                     @Field("cvc") Integer cvc
    );
}
