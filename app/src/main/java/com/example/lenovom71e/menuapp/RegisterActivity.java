package com.example.lenovom71e.menuapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.util.Common;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


     private InputMethodManager imm;
     EditText etName ;
     EditText etLName ;
     EditText etEmail ;
     EditText etPassword;
     EditText etPhoneNumber;
     EditText etAddress2;
     EditText etAddress;
     EditText etCity ;
     EditText etProvince;
     EditText etPostcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //final EditText etAge = (EditText) findViewById(R.id.etAge);
         etName = (EditText) findViewById(R.id.etfName);
         etLName = (EditText) findViewById(R.id.etlName);
         etEmail = (EditText) findViewById(R.id.etEmail);
         etPassword = (EditText) findViewById(R.id.etPassword);
         etPhoneNumber =(EditText) findViewById(R.id.rPhoneNumber);

         etAddress2 =(EditText) findViewById(R.id.guestAddress2);
         etAddress =(EditText) findViewById(R.id.guestAddress);
         etCity =(EditText) findViewById(R.id.guestCity);
         etProvince =(EditText) findViewById(R.id.guestProvince);
         etPostcode =(EditText) findViewById(R.id.guestPostCode);

        final Button bRegister = (Button) findViewById(R.id.bRegister);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        init();

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile profile = new Profile();
                 String name = etName.getText().toString();
                 String lName = etLName.getText().toString();
                 String email = etEmail.getText().toString();
                //final int age = Integer.parseInt(etAge.getText().toString());
                String password = etPassword.getText().toString();
                String phoneNumber = etPhoneNumber.getText().toString();

                String address2 = etAddress2.getText().toString();
                String address = etAddress.getText().toString();
                String city = etCity.getText().toString();
                String province = etProvince.getText().toString();
                String postcode = etPostcode.getText().toString();


                if(!name.isEmpty() && !lName.isEmpty() && !email.isEmpty() && !password.isEmpty() && !phoneNumber.isEmpty() && !address2.isEmpty() && !address.isEmpty() && !city.isEmpty() && !province.isEmpty() && !postcode.isEmpty()) {

                    if (!validateEmail()) {
                        return;
                    }

                    profile.setFirstName(name);
                    profile.setLastName(lName);
                    profile.setLoginEmail(email);
                    profile.setLoginPassword(password);
                    profile.setPhone(phoneNumber);
                    profile.setFromActivity(address2);
//                    profile.setAddress(address);
                    profile.setCiry(city);
                    profile.setProvince(province);
                    profile.setPostCode(postcode);


                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //Common.showToast(RegisterActivity.this,"response: "+response);
                                Log.d("reg_resp",response);

                                JSONObject jsonObject = new JSONObject(response);

                                boolean success = jsonObject.getBoolean("success");

                                //String message = jsonObject.getString("message");
//
                                if (success) {
                                    Common.hidePDialog();
                                    Common.showToast(RegisterActivity.this,"Registration Successful");
                                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                    RegisterActivity.this.startActivity(intent);
                                    finish();

                                } else {

                                    String message = jsonObject.getString("message");


                                    if(message.equals("email already exist"))
                                    {
                                        Common.hidePDialog();
                                        Common.showToast(RegisterActivity.this,"You are already registered");

                                    }
                                    else {

                                        Common.hidePDialog();
                                        Common.showToast(RegisterActivity.this, "Registration failed");
                                    }

                                }
                            } catch (JSONException e) {
                                //Common.showToast(RegisterActivity.this,"e; : "+e);
                                Common.hidePDialog();
                                e.printStackTrace();
                            }
                        }
                    };

                    Log.d("Reg_params","name: "+name+"\nlast: "+lName+"\nnumber: "+phoneNumber+"\npass: "+password);
                    RegisterReqeustDelivery registerRequest = new RegisterReqeustDelivery(name,lName,phoneNumber, email
                            ,password,"",address,address2,city,province,postcode,"", responseListener);
                    RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                    registerRequest.setShouldCache(false);
                    queue.add(registerRequest);
                    Common.showLoader(RegisterActivity.this);

                    registerRequest.setRetryPolicy(new RetryPolicy() {
                        @Override
                        public int getCurrentTimeout() {
                            return 5000;
                        }

                        @Override
                        public int getCurrentRetryCount() {
                            return 5000;
                        }

                        @Override
                        public void retry(VolleyError error) throws VolleyError {

                        }
                    });

                }
                   else {

                    if (name.isEmpty()) {
                        etName.setError("first name is missing");
                    } else if (lName.isEmpty()) {
                        etLName.setError("last name is missing");
                    } else if (email.isEmpty()) {
                        etEmail.setError("Email is missing");
                    } else if (password.isEmpty()) {
                        etPassword.setError("password is missing");
                    } else if (phoneNumber.isEmpty()) {
                        etPhoneNumber.setError("phone number is missing");
                    } else if (address.isEmpty()) {
                        etAddress.setError("address is missing");
                    } else if (address2.isEmpty()) {
                        etAddress2.setError("address is missing");
                    } else if (city.isEmpty()) {
                        etCity.setError("city is missing");
                    } else if (province.isEmpty()) {
                        etProvince.setError("province is missing");
                    } else if (postcode.isEmpty()) {
                        etPostcode.setError("postcode is missing");
                    } else {}
                 }
                }
        });
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private boolean validateEmail() {
        String email = etEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            etEmail.setError("Invalid email");
            return false;
        }

        return true;
    }

    public void init(){
        View toolbar =(View) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);
        title.setText("REGISTER");
        bckbtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_toolbrback:
                finish();
                break;
        }
    }
}

