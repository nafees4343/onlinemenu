package com.example.lenovom71e.menuapp.util;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.example.lenovom71e.menuapp.MainActivity;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Common {

     static CheckOut checkout = CheckoutHelper.getCheckout();
     static ProgressDialog pDialog;


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static String getTime(Date timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("HH:mm");
            /* Date netDate = new Date(timeStamp);*/
            return sdf.format(timeStamp);
        }
        catch(Exception ex){
            return "";
        }
    }

    public static String getDate(Date timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            /* Date netDate = new Date(timeStamp);*/
            return sdf.format(timeStamp);
        }
        catch(Exception ex){
            return "";
        }
    }

    public static String getLocationDate(Date timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(timeStamp);
        }
        catch(Exception ex){
            return "";
        }
    }

    public static String getTimeCovertor(String time){

        try{
            DateFormat sdP = new SimpleDateFormat("hh:mm a");
            Date date = sdP.parse(time);
            DateFormat sdF = new SimpleDateFormat("HH:mm");
            /* Date netDate = new Date(timeStamp);*/
            return sdF.format(date);
        }
        catch(Exception ex){
            return "";
        }
    }

    public static long getTimeStamp(String time){

        try{
            DateFormat sdP = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date date = sdP.parse(time);
            return date.getTime();
        }
        catch(Exception ex){
            return 0;
        }
    }

    public static String getCheckRadio(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("rbtnDifferent", "different").length() > 0) {
            return prefs.getString("rbtnDifferent", "different");
        } else
            return "";
    }

    public static void clearCart(){

        Cart c = CartHelper.getCart();
        c.clear();
        checkout.clearCheckout();
    }


    public static void showLoader(Context context){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

    }

    public static void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    public static boolean isInternetAvailable() throws InterruptedException, IOException {

            final String command = "ping -c 1 google.com";
            Log.d("cccchk","chK: "+(Runtime.getRuntime().exec(command).waitFor() == 0));
            boolean b = Runtime.getRuntime().exec(command).waitFor() == 0;
            if(b){
                return true;
            }

            return false;
    }


    public static void isNetworkAvailable(final Handler handler, final int timeout) {
        // ask fo message '0' (not connected) or '1' (connected) on 'handler'
        // the answer must be send before before within the 'timeout' (in milliseconds)

        new Thread() {
            private boolean responded = false;
            @Override
            public void run() {
                // set 'responded' to TRUE if is able to connect with google mobile (responds fast)
                new Thread() {
                    @Override
                    public void run() {
                        HttpGet requestForTest = new HttpGet("http://m.google.com");
                        try {
                            new DefaultHttpClient().execute(requestForTest); // can last...
                            responded = true;
                        }
                        catch (Exception e) {
                        }
                    }
                }.start();

                try {
                    int waited = 0;
                    while(!responded && (waited < timeout)) {
                        sleep(100);
                        if(!responded ) {
                            waited += 100;
                        }
                    }
                }
                catch(InterruptedException e) {} // do nothing
                finally {
                    if (!responded) { handler.sendEmptyMessage(0); }
                    else { handler.sendEmptyMessage(1); }
                }
            }
        }.start();
    }


    // ICMP
    public static boolean isOnlinee() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }


    }
