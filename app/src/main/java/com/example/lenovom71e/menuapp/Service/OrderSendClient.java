package com.example.lenovom71e.menuapp.Service;

import com.example.lenovom71e.menuapp.model.Paymentconfirm;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.example.lenovom71e.menuapp.util.OrderSend;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by admin on 2/5/2018.
 */

public interface OrderSendClient {


    //@Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")

    @Multipart
    @POST("placeOrder/")
    Call<PostProduct> sendOrder2(@Part("date") String date,
                                 @Part("delivery_charge") Double delivery_charge,
                                 @Part("tip") Double tip,
                                 @Part("from") Integer from,
                                 @Part("items[]") ArrayList<OrderSend.Item> items,
                                 @Part("note") String note,
                                 @Part("time") String time,
                                 @Part("type") String type,
                                 @Part("coupon") String coupon,
                                 @Part("userid") Integer userid,
                                 @Part("bfname") String bfname,
                                 @Part("blname") String blname,
                                 @Part("baddress") String baddress,
                                 @Part("baddress1") String baddress1,
                                 @Part("bcity") String bcity,
                                 @Part("bstate") String bstate,
                                 @Part("bpostcode") String bpostcode,
                                 @Part("bphone") String bphone,
                                 @Part("bcompany") String bcompany,
                                 @Part("bemail") String email,
                                 @Part("shipping_first_name") String sFirstname,
                                 @Part("shipping_last_name") String sLastname,
                                 @Part("shipping_address_1") String shippingAddress,
                                 @Part("shipping_address_2") String shippingAddress2,
                                 @Part("shipping_city") String dCity,
                                 @Part("shipping_state") String dState,
                                 @Part("shipping_postcode") String dPostcode,
                                 @Part("shipping_company") String dCompany,
                                 @Part("shipping_email") String demail
    );


   /* @FormUrlEncoded
    @POST("payment/")
    Call<Paymentconfirm> makePayment(@Field("id") Integer id,
                                     @Field("total") Double total,
                                     @Field("full_name")String fullname,
                                     @Field("cc_number")Long cardNumber,
                                     @Field("expiry") String expiryDate,
                                     @Field("cvc") Integer cvc
    );*/

   /* @FormUrlEncoded
    @POST("location/")
    Call<Paymentconfirm> locationapi(@Field("date") String date);*/




//    ("id")("total")("full_name")("cc_number")("expiry") ("cvc")
}
