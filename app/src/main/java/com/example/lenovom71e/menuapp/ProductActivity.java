package com.example.lenovom71e.menuapp;

/**
 * Created by Lenovo M71E on 2/24/2016.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.coboltforge.slidemenu.SlideMenu;
import com.coboltforge.slidemenu.SlideMenu.SlideMenuItem;
import com.coboltforge.slidemenu.SlideMenuInterface.OnSlideMenuItemClickListener;
import com.example.lenovom71e.menuapp.adapter.ProductListAdapter;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Product;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity implements OnSlideMenuItemClickListener, View.OnClickListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
    String category_id;
    String category_name;

    // Products json url

    private ProgressDialog pDialog;
    private List<Product> productList = new ArrayList<Product>();
    private ProductListAdapter adapter;
    private SlideMenu slidemenu;
    private final static int MYITEMID = 42;
    private Toolbar toolbar;
    private ListView listView;
    final String ImageURL = "https://demo.online-menu.ca/assets/img/product/";
    List<String> listDataHeader = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Intent i = getIntent();
        category_id = i.getStringExtra("category_id");
        category_name = i.getStringExtra("category_name");
        final String url = "https://demo.online-menu.ca/api/category_products/";

        init();
        setSupportActionBar(toolbar);
        setUpSlideMenu();


        listView = (ListView) findViewById(R.id.list);
        adapter = new ProductListAdapter(this, productList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                //Object o = listView.getItemAtPosition(position);
                //Product details = (Product) o;
                if(Common.isOnline(ProductActivity.this)) {
                    Intent j = new Intent(getApplicationContext(), DetailActivity.class);
                    // send category id to productlist activity to get list of products under that category
                    String product_id = ((TextView) view.findViewById(R.id.product_id)).getText().toString();
                    String category_id = ((TextView) view.findViewById(R.id.category_id)).getText().toString();
                    String pname = ((TextView) view.findViewById(R.id.pname)).getText().toString();
                    listDataHeader.add(pname);
                    //j.putExtra("product_id", String.valueOf(details));
                    //j.putExtra("category_id", String.valueOf(details));
                    j.putExtra("product_id", product_id);
                    j.putExtra("category_id", category_id);
                    j.putExtra("pname", pname);
                    startActivity(j);
                    finish();
                }
            }
        });
        // Get album id

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();


        // Creating volley request obj
        JsonArrayRequest movieReq = new JsonArrayRequest(url+category_id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        // Parsing json
                        if (response.length() > 0) {
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    Product product = new Product();
                                    product.setId(obj.getInt("category_id"));
                                    product.setPid(obj.getInt("product_id"));
                                    product.setPname(obj.getString("product_name"));
                                    product.setPdescription(obj.getString("product_description"));
                                    BigDecimal price = new BigDecimal(obj
                                            .getString("price"));
                                    product.setPrice(price);
                                    String imageName = obj.getString("image");
                                    Log.v("Product Activity ", imageName);
                                    product.setThumbnailUrl("https://demo.online-menu.ca/assets/img/product/" + imageName);
                                    productList.add(product);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            // notifying list adapter about data changes
                            // so that it renders the list view with updated data
                            adapter.notifyDataSetChanged();

                            hidePDialog();

                        }
                        else

                            {

                            hidePDialog();

                            Common.showToast(ProductActivity.this,"Currently menu is not available please try later.");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        // Adding request to request queue
        movieReq.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(movieReq);
    }

    public void init(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        title.setText(category_name);
        navigation.setOnClickListener(this);
    }

    public void setUpSlideMenu(){
        slidemenu = (SlideMenu) findViewById(R.id.slideMenu);
        if(SaveSharedPreference.getCheckLogInState(ProductActivity.this).equals("LOGIN")) {
            slidemenu.init(this, R.menu.slide, this, 333);
        }
        else {
            slidemenu.init(this, R.menu.slideforwithoutlogin, this, 333);
        }
        SlideMenu.SlideMenuItem item = new SlideMenu.SlideMenuItem();
        slidemenu.addMenuItem(item);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }
    @Override
    public void onSlideMenuItemClick(int itemId) {

        switch(itemId) {
            case R.id.item_one:
                Intent p = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(p);
                finish();
                break;
            case R.id.item_two:
                Intent j = new Intent(getApplicationContext(), CategoriesActivity.class);
                startActivity(j);
                finish();

                break;
            case R.id.item_three:
                Intent k = new Intent(getApplicationContext(), ShoppingCartActivity.class);
                startActivity(k);
                finish();

                break;
            case R.id.item_four:
                Common.clearCart();
                finishAffinity();
                break;
            case R.id.item_fifth:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://demo.online-menu.ca/customer"));
                startActivity(intent);
                break;

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_navigation:
                slidemenu.show(CartHelper.getCart().getTotalPriceWithExtra().toString());
                break;
        }
    }



}
