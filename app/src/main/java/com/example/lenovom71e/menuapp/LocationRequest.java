package com.example.lenovom71e.menuapp;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class LocationRequest extends StringRequest {

    private static final String LOCATION_REQUEST_URL = "https://demo.online-menu.ca/api/location";
    private Map<String, String> params;

    public LocationRequest(String date, Response.Listener<String> listener) {
        super(Request.Method.POST,LOCATION_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("date",date);
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }


    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
