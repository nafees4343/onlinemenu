package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovom71e.menuapp.R;

import java.util.ArrayList;

/**
 * Created by admin on 2017-08-04.
 */

public class CustomTimeAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> time;
    LayoutInflater inflater;
    String firstElement;
    boolean isFirstTime;

    public CustomTimeAdapter(Context applicationContext, ArrayList<String> passingTimeArray){
        this.context = applicationContext;
        this.time = passingTimeArray;
        inflater = (LayoutInflater.from(applicationContext));

    }



    @Override
    public int getCount(){
        return time.size();

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.custom_spinner_items, null);
        TextView timeTitle =(TextView) convertView.findViewById(R.id.spinnerTimeTitle);
        timeTitle.setText(time.get(position));
        return convertView;




    }
}
