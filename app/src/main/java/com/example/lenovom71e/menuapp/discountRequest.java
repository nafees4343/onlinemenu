package com.example.lenovom71e.menuapp;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017-08-22.
 */

public class discountRequest extends StringRequest{


        private static String DISCOUNTURL = "https://demo.online-menu.ca/api/discount/";
        private Map<String, String> params;
        public discountRequest(String discountCode, String value , Response.Listener<String> listener){
            super(Method.GET, DISCOUNTURL + discountCode + "/" +value, listener, null);
            Log.d("VVValue", "discountcode: "+discountCode+"\nvalue: "+value);
            params = new HashMap<>();
            params.put("code", discountCode);
            params.put("total", value);


        }

        @Override
    public Map<String,String> getParams(){
            return params;
        }


}
