package com.example.lenovom71e.menuapp.callbacks;

import com.example.lenovom71e.menuapp.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subhan on 8/20/18.
 */

public interface OnDessertComplete {
    public void OnSelect(List<Product> desserts);
}
