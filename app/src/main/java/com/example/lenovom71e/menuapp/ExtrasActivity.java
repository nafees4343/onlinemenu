package com.example.lenovom71e.menuapp;

/**
 * Created by STUDENT on 2/17/2016.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.lenovom71e.menuapp.adapter.ExpandListAdapter;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Choice;
import com.example.lenovom71e.menuapp.model.Extra;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

public class ExtrasActivity extends Activity implements OnClickListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
    // Categories json url
    private static final String url = "https://demo.online-menu.ca/api/product_extras/";
    private ProgressDialog pDialog;
    private ExpandableListView ExpandList;
    private ExpandListAdapter ExpAdapter;
    String product_id;
    String pname;
    private Toolbar toolbar;
    Button ok_btn , cancel_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra);
        //toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        //setSupportActionBar(toolbar);
        //toolbar.setLogo(R.mipmap.logo);
        ExpandList = (ExpandableListView) findViewById(R.id.list);
        ExpandList.setOnGroupExpandListener(onGroupExpandListenser);
        ok_btn = (Button) findViewById(R.id.ok_btn_id);
        cancel_btn = (Button) findViewById(R.id.cancel_btn_id);
        ok_btn.setOnClickListener(this);
        cancel_btn.setOnClickListener(this);
        Intent i = getIntent();
        product_id = i.getStringExtra("product_id");
        pname = i.getStringExtra("pname");
        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        //setTitle(pname);
        makejsonobjreq();


    }
    private void makejsonobjreq() {
        pDialog.show();
        // Creating volley request obj
        JsonObjectRequest extraReq = new JsonObjectRequest(Request.Method.GET,url+"extras/"+product_id,
                (String)null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<Extra> list = new ArrayList<Extra>();
                        ArrayList<Choice> ch_list;
                        try {
                            Iterator<String> key = response.keys();
                            while (key.hasNext()) {
                                String k = key.next();
                                Extra ext = new Extra();
                                ext.setEname(k);
                                ch_list = new ArrayList<Choice>();
                                JSONArray ja = response.getJSONArray(k);
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject obj = ja.getJSONObject(i);
                                    Choice ch = new Choice();
                                    ch.setCmultiple(obj.getInt("is_multiple_select"));
                                    ch.setCname(obj.getString("name"));
                                    BigDecimal price1 = new BigDecimal(obj.getString("price"));
                                    ch.setCprice(price1);
                                    // adding categories to categories array
                                    ch_list.add(ch);
                                }
                            ext.setChoices(ch_list);
                            list.add(ext);
                            } // while loop end
                            ExpAdapter = new ExpandListAdapter(
                            ExtrasActivity.this, list);
                            ExpandList.setAdapter(ExpAdapter);

                            pDialog.dismiss();

                        } catch (JSONException e) {
                           e.printStackTrace();
                            Log.e("TAG", "There is no Extras for this product" + e.toString());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       pDialog.dismiss();
                    }
                }
        );

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(extraReq, "jreq");
    }




    ExpandableListView.OnGroupExpandListener onGroupExpandListenser = new ExpandableListView.OnGroupExpandListener() {
        int previousGroup =-1;
        @Override
        public void onGroupExpand(int groupPosition) {
            if(groupPosition!= previousGroup)
                ExpandList.collapseGroup(previousGroup);
            previousGroup = groupPosition;

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok_btn_id:
                showToastMessage("Ok Button Clicked");
                this.finish();
                break;
            case R.id.cancel_btn_id:
                showToastMessage("Cancel Button Clicked");
                this.finish();
                break;
        }

    }




    void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
                .show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}