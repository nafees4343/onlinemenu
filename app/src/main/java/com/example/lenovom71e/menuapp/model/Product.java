package com.example.lenovom71e.menuapp.model;

/**
 * Created by STUDENT on 2/24/2016.
 */

import com.android.tonyvu.sc.model.Saleable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

public class Product implements Saleable, Serializable {
    private static final long serialVersionUID = -4073256626483275668L;
    private String pname, thumbnailUrl;
    private BigDecimal price;
    private int id;
    private int pid;
    private String pdescription;
    private int idE;
    private String eName;
    private int quantity= 1;
    private static final AtomicInteger itemId = new AtomicInteger(0);
    private int  _ItemId;
    private String image;
    private boolean isSelected = false;



    public Product() {
    }

    public Product(int id,int pid, String pname, String thumbnailUrl, BigDecimal price, String pdescription, int idE, String eName, boolean isSelected, String image)

    {
        this.id = id;
        this.pid = pid;
        this.pname = pname;
        this.thumbnailUrl = thumbnailUrl;
        this.price = price;
        this.pdescription = pdescription;
        this.idE = idE;
        this.eName = eName;
        this.isSelected = isSelected;
        this.image = image;
    }
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Product)) return false;

        return (this.pid == ((Product) o).getPid());
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 1;
        hash = hash * prime + pid;
        hash = hash * prime + (pname == null ? 0 : pname.hashCode());
        hash = hash * prime + (price == null ? 0 : price.hashCode());
        hash = hash * prime + (pdescription == null ? 0 : pdescription.hashCode());

        return hash;
    }
    public int getId() {
        return id;
    }

    public int getproductId() {
        return  pid;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }
    public int getPid() {
        return pid;
    }

    public int setPid(int pid) {
        this.pid = pid;
        return pid;
    }

    public String getPname() {
        return pname;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public int getitemId() {
        return _ItemId;
    }


    public int get_ItemId() {
        return _ItemId;
    }

    public int set_ItemId() {

        return this._ItemId = itemId.incrementAndGet();
    }

    public String setPname(String name) {
        this.pname = name;
        return name;
    }



    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal setPrice(BigDecimal price) {
        this.price = price;
        return price;
    }

    public String getPdescription() {
        return pdescription;
    }

    public String setPdescription(String pdescription) {
        this.pdescription = pdescription;
        return pdescription;
    }
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return thumbnailUrl;
    }
    public int getIde() {
        return idE;
    }

    public int setIde(int idE) {
        this.idE = idE;
        return idE;
    }
    public String getEname() {
        return eName;
    }

    public String setEname(String eName) {
        this.eName = eName;
        return eName;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

