package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.model.Address;

import java.util.ArrayList;

/**
 * Created by admin on 2017-07-25.
 */

public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<Address> locations;
    LayoutInflater inflater;

    public CustomAdapter(Context applicationContext, ArrayList<Address> locations) {
        this.context = applicationContext;
        this.locations = locations;
        inflater = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.location_spinner_items, null);
        TextView textName= (TextView) convertView.findViewById(R.id.nameLabel);
        textName.setText(locations.get(position).getmName());
        return convertView;

    }
}
