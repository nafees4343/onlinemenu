package com.example.lenovom71e.menuapp.model;

/**
 * Created by aliba on 6/20/2017.
 */

public class ExtraCart {
    String extraName;
    double extraPrice;

    /**
     *
     * @param name Name of the Extra item
     * @param price Price of the Extra Item
     */

    public ExtraCart(String name, double price){
        extraName = name;
        extraPrice = price;

    }

    public String getExtraName() {
        return extraName;
    }

    public double getExtraPrice() {
        return extraPrice;
    }
}
