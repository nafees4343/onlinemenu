package com.example.lenovom71e.menuapp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.lenovom71e.menuapp.CategoriesActivity;
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.ShoppingCartActivity;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.math.BigDecimal.ROUND_CEILING;

/**
 * Created by aliba on 6/20/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<Saleable> _listDataHeader ;
    private List<Integer> _listDataHeaderId ;// header titles
    //child data in format of headertitle, child title;
    private ArrayList<Saleable> _listDataChild;
    private HashMap<Integer, List<ExtraProuct>> _listDataChildId;
//    private HashMap<Integer,HashMap<Saleable,List<ExtraProuct>>> _ListDataChildWithId;
//    private  List<String> _listDataChild;
    private HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> _listHeaderWQuantity;
    private List<Integer> _productQuantities;
    private double _extraPrice;
    final CheckOut checkOut= CheckoutHelper.getCheckout();
    private BigDecimal currentPrice, totalPrice;
    Cart cart;
    String s;


    //    public ExpandableListAdapter(Context context,
//                                 List<Saleable> listDataHeader,
//                                 HashMap<Saleable, List<ExtraProuct>> listChildData,
//                                 HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> listHeaderWQuantity,
//                                 List<Integer> productQuantities){
//        this._context = context;
//        this._listDataHeader = listDataHeader;
//        this._listDataChild = listChildData;
//        this._listHeaderWQuantity = listHeaderWQuantity;
//        this._productQuantities = productQuantities;
//
//    }

    public ExpandableListAdapter(Context context,
                                 List<Integer> listDataHeader,
                                 HashMap<Integer, List<ExtraProuct>> listChildData,
                                 HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> listHeaderWQuantity,
                                 List<Integer> productQuantities,
                                 ArrayList<Saleable> prodcutsWithExtraList,
                                    String s){
        this._context = context;
        this._listDataHeaderId = listDataHeader;
        this._listDataChildId = listChildData;
        this._listHeaderWQuantity = listHeaderWQuantity;
        this._productQuantities = productQuantities;
        this._listDataChild = prodcutsWithExtraList;
        this.s = s;
        checkOut.setIdProductExtraList(listChildData);
        checkOut.setProductHeaderId((ArrayList<Integer>) listDataHeader);
    }

    public interface OnDataChangeListener{
        public void onDataChanged(int size);
    }

    OnDataChangeListener mOnDataChangeListener;
    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
        mOnDataChangeListener = onDataChangeListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPositition) {
//        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPositition);
        return this._listDataChildId.get(this._listDataHeaderId.get(groupPosition)).get(childPositition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


//    public double calculateExtraPrice(int groupPosition){
//        double result = 0;
//        int count = getChildrenCount(groupPosition);
//        for (int i=0; i< count ; i++){
//            Extra current = (Extra) getChild(groupPosition, i);
//            result += current.extraPrice;
//
//        }
//        return result;
//    }

    public BigDecimal updateprice(){
        BigDecimal s;
        s = totalPrice ;
        return s;
    }

    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent){

        Log.d("positpot","childviewposition: "+groupPosition);


        ExtraProuct currentString = (ExtraProuct) getChild(groupPosition,childPosition);


//        Log.d("CurrentExtraList : ", String.valueOf(currentExtraList));
        //Log.d("Current String", currentString);
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_extra_item, null);
        }

       // Log.d("GetchildView", expandedListText);



        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView lblChildPrice = (TextView) convertView.findViewById(R.id.lbllistItemPrice);

        if(currentString.getExtraId() == -1){
            txtListChild.setVisibility(View.GONE);
            lblChildPrice.setVisibility(View.GONE);
        }
        txtListChild.setText(currentString.getExtraName());
        lblChildPrice.setText("$ " +( double)(_productQuantities.get(groupPosition))*(currentString.getExtraPrice()));




        if(childPosition == getChildrenCount(groupPosition) -1) {
            RelativeLayout relativeLayout = (RelativeLayout)convertView.findViewById(R.id.total_container);
            relativeLayout.setVisibility(View.VISIBLE);
            TextView lblTotalItemPrice = (TextView) convertView. findViewById(R.id.itemTotal);

            //todo
            //totalPrice = updateprice();
            lblTotalItemPrice.setText("$ " + CartHelper.getCart().getExtrasTotal(groupPosition));
            Log.d("tttt","childviewTotal: "+totalPrice);
            View view = (View)convertView.findViewById(R.id.divider);
            view.setVisibility(View.VISIBLE);
        }
        else
        {
            RelativeLayout relativeLayout = (RelativeLayout)convertView.findViewById(R.id.total_container);
            relativeLayout.setVisibility(View.GONE);
            View view = (View)convertView.findViewById(R.id.divider);
            view.setVisibility(View.GONE);

        }

        return convertView;
    }

    @Override
    public int getChildrenCount (int groupPosition){
//        Saleable currentGroup = _listDataHeader.get(groupPosition);
//        return this._listDataChild.get(currentGroup).size();
        Integer currentGroup = _listDataHeaderId.get(groupPosition);
        return this._listDataChildId.get(currentGroup).size();
    }


    @Override
    public Object getGroup(int groupPosition) {
//        return this._listDataHeader.get(groupPosition);
        return this._listDataHeaderId.get(groupPosition);
    }

    @Override
    public int getGroupCount(){
//        return this._listDataHeader.size();
        return this._listDataHeaderId.size();
    }
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        Log.d("positpot","position: "+groupPosition);


        final Saleable product = _listDataChild.get(groupPosition);
        String currnetName = "";
        BigDecimal price = BigDecimal.ZERO;
        _extraPrice = 0;
        Integer headerId = _listDataHeaderId.get(groupPosition);
        for (Saleable id:_listDataChild){

            if (headerId == id.getitemId()){
                Log.d("groupname", id.getPname());
                price = id.getPrice();
                break;
            }
        }
        List<ExtraProuct> currentExtraList = _listDataChildId.get(_listDataHeaderId.get(groupPosition));
        Integer currentQuantity = _productQuantities.get(groupPosition);
        //itemQuantity = currentQuantity;
        for (int i = 0 ; i < currentExtraList.size(); i++){

            ExtraProuct currentCHild = currentExtraList.get(i);
            _extraPrice += currentCHild.getExtraPrice();
        }

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_cart_item, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.tvCartItemName);
        lblListHeader.setText(product.getPname());

        final TextView lblProductQuantity = (TextView) convertView.findViewById(R.id.tvCartItemQuantity);
        lblProductQuantity.setText("" + currentQuantity);

//        BigDecimal productPrice = _listDataHeader.get(groupPosition).getProduct().getPrice().multiply(BigDecimal.valueOf(quantityTitle));
//        BigDecimal productPrice = _listDataHeader.get(groupPosition).getPrice();
        currentPrice = price;

        TextView lblProductPrice = (TextView) convertView.findViewById(R.id.tvCartItemPrice);

        price = price.multiply(BigDecimal.valueOf(currentQuantity));

        lblProductPrice.setText("$ " + price.setScale(2,ROUND_CEILING));


        ImageButton btn_minus = (ImageButton)convertView.findViewById(R.id.btn_minus);
        ImageButton btn_plus = (ImageButton)convertView.findViewById(R.id.btn_plus);


        //double extraPrice = (calculateExtraPrice(groupPosition) * quantityTitle);
        totalPrice = currentPrice.add(BigDecimal.valueOf(_extraPrice)).multiply(BigDecimal.valueOf(currentQuantity));

        CartHelper.getCart().addTotal(groupPosition,totalPrice);
//        Log.d("Prodcut Price ", String.valueOf(productPrice));
//        Log.d("Extra Price ", String.valueOf(extraPrice));
        //totalPrice = updateprice();
              Log.d("tttt ", "parentviewTotal: "+String.valueOf(totalPrice));
              //todo
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
        thumbNail.setImageUrl(product.getImage(), AppController.getInstance().getImageLoader());


        final ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        //Implement Delete Button in group list
        Button delete = (Button) convertView.findViewById(R.id.del);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(_context)
                        .setTitle("Discard Item")
                        .setMessage("Do You want to discard item ?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                              deleteCartItem(groupPosition);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer currentQuantity = _productQuantities.get(groupPosition);
                if (currentQuantity >1)
                    currentQuantity = --currentQuantity;
                lblProductQuantity.setText("" + currentQuantity);
                CartHelper.getCart().update(product, currentQuantity);
                totalPrice = currentPrice.add(BigDecimal.valueOf(_extraPrice)).multiply(BigDecimal.valueOf(currentQuantity));
                CartHelper.getCart().addTotal(groupPosition,totalPrice);
                _productQuantities.set(groupPosition, currentQuantity);

                if(mOnDataChangeListener != null){
//                    mOnDataChangeListener.onDataChanged(_listDataChild.size());
                    mOnDataChangeListener.onDataChanged(_listDataChildId.size());

                }

                notifyDataSetChanged();
            }
        });


        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer currentQuantity = _productQuantities.get(groupPosition);
                if(currentQuantity < 10) {
                    currentQuantity = ++currentQuantity;
                    lblProductQuantity.setText("" + currentQuantity);
                    CartHelper.getCart().update(product, currentQuantity);
                    totalPrice = currentPrice.add(BigDecimal.valueOf(_extraPrice)).multiply(BigDecimal.valueOf(currentQuantity));
                    CartHelper.getCart().addTotal(groupPosition,totalPrice);
                    _productQuantities.set(groupPosition, currentQuantity);

                    if (mOnDataChangeListener != null) {
//                    mOnDataChangeListener.onDataChanged(_listDataChild.size());
                        mOnDataChangeListener.onDataChanged(_listDataChildId.size());
                    }

                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }

    private void deleteCartItem(int groupPosition){
        final Cart cart = CartHelper.getCart();

        //TODO: we need to add remove function for the cart and run the function here
         //cart.remove(groupPosition,productsWithExtraListtt,_listHeaderWQuantity); // remove from the cart items
                /*int number = checkOut.getProductHeaderId().get(groupPosition);
                List<ExtraProuct> numberList = checkOut.getIdProductExtraList().get(number);
*/
//                checkOut.getProductHeaderId().remove(groupPosition);

        final Saleable product = _listDataChild.get(groupPosition);

        cart.removeExtrasTotal(groupPosition);

        Log.d("remproduct","product: "+product);
        cart.remove(product);

        cart.removeProductQuantity(product.getproductId());

        _listDataChildId.remove(_listDataHeaderId.get(groupPosition));

        _listHeaderWQuantity.remove(_listDataHeaderId.get(groupPosition));

        _listDataHeaderId.remove(_listDataHeaderId.get(groupPosition));

//                _listDataChild.remove(_listDataHeaderId.get(groupPosition));
        _productQuantities.remove(groupPosition);

        cart.getProductsHeader().remove(groupPosition);

        cart.remove(totalPrice);

        Log.d("totalPricepp","totalPricepp: "+totalPrice);
//                checkOut.getProductHeaderId().remove(groupPosition);


//                List<ExtraProuct> currentList = checkOut.getIdProductExtraList().get(groupPosition);
//                checkOut.getIdProductExtraList().get(checkOut.getProductHeaderId().get(groupPosition));
//                checkOut.getProductHeaderId().remove(groupPosition);




        ExpandableListAdapter.this.notifyDataSetChanged();
        if(mOnDataChangeListener != null){
//                    mOnDataChangeListener.onDataChanged(_listDataChild.size());
            mOnDataChangeListener.onDataChanged(_listDataChildId.size());
        }

        notifyDataSetChanged();

        if(s!=null && s.equals("Category")){
            Intent i = new Intent(_context,CategoriesActivity.class);
            _context.startActivity(i);
            ((Activity)_context).finish();
        }
    }
    @Override
    public boolean hasStableIds(){
        return true;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }
}