package com.example.lenovom71e.menuapp.model;

/**
 * Created by STUDENT on 2/24/2016.
 */
import java.util.ArrayList;

public class Category {
    private int id;
    private String name;
    public Category() {
    }
    public Category(int id, String name)

    {
        this.name = name;
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return name;
    }

    public void setCategory(String name) {
        this.name = name;
    }

}

