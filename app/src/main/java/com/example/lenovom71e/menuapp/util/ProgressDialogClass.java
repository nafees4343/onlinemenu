package com.example.lenovom71e.menuapp.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogClass {

    public static ProgressDialog progressDialog_round;


    public static void showRoundProgress(Context context,String message){
        if(context != null || message.equals("") ) {
            progressDialog_round = new ProgressDialog(context);
            progressDialog_round.setMessage(message);
            progressDialog_round.show();
        }
    }

    public static void dismissRoundProgress()
    {
        if(progressDialog_round !=null && progressDialog_round.isShowing()){
            progressDialog_round.dismiss();
        }
    }

}
