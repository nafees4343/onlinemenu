package com.example.lenovom71e.menuapp;

/**
 * Created by Lenovo M71E on 2/10/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.util.LoadingTask;
import com.example.lenovom71e.menuapp.util.LoadingTask.LoadingTaskFinishedListener;


public class Splash extends AppCompatActivity implements LoadingTaskFinishedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        // Find the progress bar
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        // Start your loading
        new LoadingTask(progressBar, this).execute("www.google.co.uk");
    }
    @Override
    public void onTaskFinished() {
        completeSplash();
    }

    private void completeSplash(){

        startApp();
        // Don't forget to finish this Splash Activity so the user can't return to it!
    }

    private void startApp() {
        Intent intent = new Intent(Splash.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}