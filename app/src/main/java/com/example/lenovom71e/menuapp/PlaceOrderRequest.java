package com.example.lenovom71e.menuapp;

import android.util.Log;

import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.util.OrderSend;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017-10-02.
 */

public class PlaceOrderRequest extends StringRequest {

    //TODO: need to change the request URL
    private static final String PLACE_ORDER_REQUEST_URL =  "https://demo.online-menu.ca/api/placeOrder";
    private Map<String, String> params;
    JSONArray items = new JSONArray();
    CheckOut checkoutItem = CheckoutHelper.getCheckout();


    public PlaceOrderRequest(OrderSend orderSend, Response.Listener<String> listener) {

        super(Method.POST, PLACE_ORDER_REQUEST_URL, listener, null);
        try {
            for(OrderSend.Item i : orderSend.getItems()){
                JSONObject object = new JSONObject();
                JSONArray array = new JSONArray();
                for(int j=0; j<i.getExtras().length; j++){
                    array.put(i.getExtras()[j]);
                }
                object.put("extras",array);
                object.put("pid",i.getPid());
                object.put("qty",i.getQty());
                object.put("tip",i.getTip());

                items.put(object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        params = new HashMap<>();
        params.put("date", orderSend.getDate());
        params.put("delivery_charge", ""+orderSend.getDeliveryCharge());
        params.put("from", String.valueOf(orderSend.getFrom()));
        params.put("items", items.toString());
        params.put("note", orderSend.getNote());
        params.put("time", orderSend.getTime());
        params.put("type", orderSend.getType());
        params.put("coupon", orderSend.getCoupon());
        params.put("userid", String.valueOf(orderSend.getUserId()));
        params.put("bfname", orderSend.getBfname());
        params.put("blname", orderSend.getBlname());
        params.put("baddress", orderSend.getBaddress());
        params.put("baddress1", orderSend.getBaddress1());
        params.put("bcity", orderSend.getBcity());
        params.put("bstate", orderSend.getBstate());
        params.put("bpostcode", orderSend.getBpostcode());
        params.put("bphone", orderSend.getBphone());
        params.put("bcompany", orderSend.getBcompany());
        params.put("bemail", orderSend.getBemail());
        params.put("shipping_first_name", orderSend.getShippingFirstname());
        params.put("shipping_last_name", orderSend.getShippingLastname());
        params.put("shipping_address_1", orderSend.getdAddress());
        params.put("shipping_address_2", orderSend.getdAddress1());
        params.put("shipping_city", orderSend.getdCity());
        params.put("shipping_state", orderSend.getdProvince());
        params.put("shipping_postcode", orderSend.getdPostcode());
        params.put("shipping_company", orderSend.getDcompany());
        params.put("shipping_email", orderSend.getDemail());


    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=UTF-8";
    }


    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
