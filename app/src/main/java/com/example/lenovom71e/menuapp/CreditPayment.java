package com.example.lenovom71e.menuapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.coboltforge.slidemenu.SlideMenu;
import com.example.lenovom71e.menuapp.Service.CardCredentialsSendtoClient;
import com.example.lenovom71e.menuapp.Service.OrderSendClient;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Paymentconfirm;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.example.lenovom71e.menuapp.util.Common;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

public class CreditPayment extends AppCompatActivity {

    final CheckOut checkout = CheckoutHelper.getCheckout();
    String accountNumber, email, phone, totalAfterDiscount;
    static final String PAYMENTPOSTURL = "https://demo.online-menu.ca/api/payment";
//    RequestQueue queue = Volley.newRequestQueue(this);
    String orderNumberString = "";
    String total = "";
    String fullname="", card_number="",cvc="";
    ProgressDialog pDialog;
    private InputMethodManager imm;
    Toolbar toolbar;
    String expirydate;

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        Intent i = new Intent(CreditPayment.this,CategoriesActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView title = (TextView) toolbar.findViewById(R.id.txt_toolbrTitle);
        title.setText("PAYMENT");
        Button bckbtn= (Button) toolbar.findViewById(R.id.btn_toolbrback);
        Button navigation= (Button) toolbar.findViewById(R.id.btn_navigation);

        navigation.setVisibility(View.GONE);
        bckbtn.setVisibility(View.VISIBLE);

        final RequestQueue queue = Volley.newRequestQueue(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // Showing progress dialog before making http request
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        final EditText fullName = (EditText) findViewById(R.id.fullName_cp);
        final EditText cardNumber = (EditText) findViewById(R.id.creditCardNumber_cp);
        final EditText expiry = (EditText) findViewById(R.id.expiryDate_cp);
        final TextView cvcNumber = (TextView) findViewById(R.id.cvcNumber_cp);

        final Button placeOrderBtn = (Button) findViewById(R.id.placeOrderBtn);
        //final TextView orderNumberTxt = (TextView) findViewById(R.id.paymentOrderNumberTxt);
        ImageView imageLogo = (ImageView) findViewById(R.id.logoImage);
//        accountNumber = getIntent().getStringExtra("accountNumber");
//        email = getIntent().getStringExtra("email");
//        phone = getIntent().getStringExtra("phone");
        orderNumberString = getIntent().getStringExtra("orderNumber");
        total = getIntent().getStringExtra("total");
        /*totalAfterDiscount = checkout.getmCheckoutTotalAfterDiscount().toString();
        double totalAmount = Double.parseDouble(total) - Double.parseDouble(totalAfterDiscount);
*/
        //orderNumberTxt.setText(orderNumberString);




        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.myanimation);
        imageLogo.startAnimation(animation);


        final TextView priceText = (TextView) findViewById(R.id.paymentBalanceTxt);
        priceText.setText("Total Payment $"+String.valueOf(total));

        bckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isOnline(CreditPayment.this)) {
                    finishAffinity();
                    Intent i = new Intent(CreditPayment.this,CategoriesActivity.class);
                    startActivity(i);
                }
                else {
                    Common.showToast(CreditPayment.this,"Internet is not available...");
                }
            }
        });




        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isOnline(CreditPayment.this)) {
                    fullname = fullName.getText().toString();
                    card_number = cardNumber.getText().toString();
                    expirydate = expiry.getText().toString();
                    cvc = cvcNumber.getText().toString();


                    if (Common.isOnline(CreditPayment.this)) {
                        if (!card_number.isEmpty() && !expirydate.isEmpty() && !cvc.isEmpty()) {
                            pDialog.show();
                            sendNetworkRequest(orderNumberString, total, card_number, expirydate, cvc);
                        } else {
                            Toast.makeText(CreditPayment.this, "please fill all credentials", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Common.showToast(CreditPayment.this, "No internet connectivity...");
                    }
                }
                else {
                    Common.showToast(CreditPayment.this,"Internet is not available...");
                }
            }
        });
    }

    private void serializeOrderData(){
        List<Integer> extras = new ArrayList<Integer>();
        extras.add(5);
        extras.add(21);
        extras.add(18);

    }



    private void sendNetworkRequest(String orderNumberString,String total,String card_number,String expirydate,String cvc){
        //create Okhttp client
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okhttpClientBuilder.addInterceptor(logging);
        //create Retrofit Instance
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://demo.online-menu.ca/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okhttpClientBuilder.build());

        Retrofit retrofit = builder.build();

        CardCredentialsSendtoClient client = retrofit.create(CardCredentialsSendtoClient.class);

        StringBuilder message = new StringBuilder();
        if(card_number.isEmpty() && expirydate.isEmpty()&& cvc.isEmpty()){

            if (card_number.isEmpty() && card_number.length() != 16){
                message.append("Please Enter Card Number or the correct credit card number \n");
            }
            if(expirydate.isEmpty() && String.valueOf(expirydate).length() != 4 ){
                message.append("Please enter the 4 digits expiry date \n");
            }
            if(cvc.isEmpty() && cvc.length() != 3){
                message.append("Please enter 3 digits of the CVC that is on the back of your card \n");
            }
            viewNegativeMessage(message.toString());


        }else {
           // Integer orderId = Integer.parseInt(orderNumberString);
          //  Double totalToDouble = Double.valueOf(total);
          //  Long cardNumber = Long.parseLong(card_number,L);
          //  Integer cvcNumber = Integer.parseInt(cvc);

           // Call<Paymentconfirm> call2 = client.makePayment(orderNumberString,total, fullname,card_number,expirydate,cvc);


            Log.d("params:","Parameters: "+"Orderno: "+Integer.parseInt(orderNumberString)+"\nTotal: "+Double.valueOf(total)+"\nfullname: "+fullname+"\nCardNumber: "+Long.parseLong(card_number)+"\nCVC: "+cvc);
            Call<Paymentconfirm> call2 = client.makePayment(Integer.parseInt(orderNumberString),Double.valueOf(total), fullname,Long.parseLong(card_number),expirydate,Integer.parseInt(cvc));
            //Call<Paymentconfirm> call2 = client.makePayment(41374,240d, "nfs",5472063333333330L,"1220",123);

            call2.enqueue(new Callback<Paymentconfirm>() {
                @Override
                public void onResponse(Call<Paymentconfirm> call, Response<Paymentconfirm> response) {
                    hidePDialog();
                    checkout.clearCheckout();
                    Log.d("Success", "it worked");
                    Log.d("Databack", response.body().toString());
                    //Paymentconfirm confirm = response.body();
                    if(response.isSuccessful()) {
                        viewMessageStatus(response.body().getData().getSslAmount());
                        Log.d("Success", "it w");

                    }else {
                        viewNegativeMessage(response.body().getData().getSslResult());
                        Log.d("Success", "it !");

                    }

                    hidePDialog();
                }

                @Override
                public void onFailure(Call<Paymentconfirm> call, Throwable t) {
                    Log.d("Failure", "it didn't worked: "+t);
                    hidePDialog();
                    viewNegativeMessage("Transaction Fail...");
                }
            });

        }

    }

    private void backToCategory(){
        if(Common.isOnline(CreditPayment.this)){
            Intent i = new Intent(CreditPayment.this, CategoriesActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        }else{
            Common.showToast(CreditPayment.this,"Internet not available...");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    public void viewMessageStatus(String status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Payment of "+status+" Successful");
        builder.setPositiveButton("Thank you", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Cart c = CartHelper.getCart();
                c.clear();
                checkout.clearCheckout();
                Intent intent = new Intent(CreditPayment.this, CategoriesActivity.class);
                finishAffinity();
                startActivity(intent);

            }
        }).show();



    }

    public void viewNegativeMessage(String status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(status);
        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }



}
