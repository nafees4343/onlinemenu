package com.example.lenovom71e.menuapp.model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by admin on 2017-07-13.
 */

public class ProductCartItem {
    private Product mproduct;
    private static  final AtomicInteger cartId = new AtomicInteger(0);
    private int mquantity;
    private int id;


    public ProductCartItem(Product product){
        mproduct = product;
        id = cartId.incrementAndGet();
    }
    public ProductCartItem(Product product, int quantity){
        mproduct = product;
        id = cartId.incrementAndGet();
        mquantity = quantity;
    }

    public Product getMproduct() {
        return mproduct;
    }

    public void setMproduct(Product mproduct) {
        this.mproduct = mproduct;
    }

    public static AtomicInteger getCartId() {
        return cartId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
